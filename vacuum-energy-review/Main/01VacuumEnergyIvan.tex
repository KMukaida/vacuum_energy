%!TEX root = ../VacuumEnergy.tex

\chapter{Vacuum energy and Casimir Energy}
\label{cha:DefAndCasimir}

\section{``Bare'',``classical'' and ``quantum'' cosmological constant (CC)}
\label{sec:BCQCosmConst}
In this section we will use the following notation:
\begin{itemize}
 \item metric signature: $(-,+,+,+)$
 \item $\displaystyle \kappa = \frac{8\pi G}{c^4} = \frac{8\pi}{m_{pl}^2} = \frac{1}{M_{pl}^2} $
\end{itemize}
We start with the Einstein-Hilbert action
\begin{equation}
 \label{no1_1}
 S = \frac{1}{2\kappa} \int d^4 x \sqrt{-g} \lp R - 2 \Lambda_B \rp + S_{\textrm{matter}}[g_{\mu\nu},\Psi]
\end{equation}
In this equation $\Lambda_B$ is merely a  parameter of the  action and is not fixed by the structure of the theory, so one cannot calculate its value from more fundamental considerations. Thus it is called ``bare'' cosmological contant. Its dimension is $[\Lambda_B] = \frac{1}{L^2}$. 
From the action~\eqref{no1_1} one can derive the equations of motion
\begin{equation}
 \label{no1_2}
 R_{\mu\nu} - \frac{1}{2} R g_{\mu \nu} + \Lambda_B g_{\mu \nu} = \kappa T_{\mu \nu} 
\end{equation}
where $T_{\mu \nu}$ is the stress-energy tensor 
\[
 T_{\mu \nu} = -\frac{2}{\sqrt{-g}} \frac{\delta S_{\textrm{matter}}}{\delta g_{\mu \nu}}
\]
In Quantum Field Theory (QFT) the stress-energy tensor of a field in the vacuum configuration is given by
\begin{equation}
 \label{no1_3}
 \langle   T_{\mu \nu}   \rangle \equiv \langle 0 \vert T_{\mu \nu} \vert 0 \rangle = -\rho_{\textrm{vac}} g_{\mu \nu} \ .
\end{equation} 
To see this, consider a scalar field with the following action
\begin{equation}
 \label{no1_3a}
 S_{\phi} = -\int d^4 x \sqrt{-g} \lp \frac{1}{2}g^{\mu \nu} \partial_{\mu} \Phi \partial_{\nu} \Phi + V(\Phi) \rp
\end{equation}
whose stress-energy tensor   reads
\begin{equation}
 \label{no1_3b}
 T_{\mu \nu} = \partial_{\mu} \Phi \partial_{\nu} \Phi - g_{\mu \nu} \lp \frac{1}{2} g^{\alpha \beta} \partial_{\alpha} \Phi \partial_{\beta} \Phi  + V(\Phi) \rp \ .
 \end{equation}
 The vacuum state is the minimum energy state where all field derivatives should vanish. This implies $T_{\mu \nu} = -V(\Phi_{\textrm{min}}) g_{\mu \nu}$. This expression implies that the vacumm equation of state coincides with the equation of state of a perfect fluid
\begin{equation}
 \label{no1_3c}
 \langle \rho \rangle = -\langle p \rangle \ ,
 \end{equation}
which is a direct consequence of  Lorentz symmetry. We will use this fact later on.
 
 \noindent
Note that in this case there are two sources of vacuum energy~\eqref{no1_3}:
 \begin{itemize}
  \item The ``classical'' contribution $V(\Phi_{\textrm{min}})$.
  \item The ``quantum'' contribution, i.e. zero-point fluctuations of the ground state.
 \end{itemize}
 Now we apply the equivalence principle to the zero-point fluctuations (both ``classical'' and ``quantum'' contribution)\footnote{More discussions on this step can be found in~\cite{Martin:2012bt}.}
 and arrive to the following equation
 \begin{equation}
  \label{no1_4}
  R_{\mu \nu} - \frac{1}{2} R g_{\mu \nu} + \Lambda_B g_{\mu \nu} = \kappa T_{\mu \nu}^{\textrm{matter}} + \kappa \langle T_{\mu \nu} \rangle.
 \end{equation}
 or, equivalently,
 \begin{equation}
  \label{no1_4a}
  R_{\mu \nu} - \frac{1}{2} R g_{\mu \nu} + \Lambda_{\textrm{eff}} g_{\mu \nu} = \kappa T_{\mu \nu}^{\textrm{matter}},
 \end{equation}
 where
 \begin{equation}
  \label{no1_4b}
  \Lambda_{\textrm{eff}} = \Lambda_{B} + \kappa \rho_{\textrm{vac}}
 \end{equation}

 \subsection{Classical cosmological constant problem.}
 \label{subsec:classicalCC}
 \subsubsection{Electroweak contributions.}
 \label{subsubsec:EW}
Higgs potential in the Standard Model has the form
\begin{equation}
 \label{}
 V(H) = \frac{\mu^2}{2} H^{\dagger}H + \frac{\lambda}{4} (H^{\dagger}H)^2 \ .
\end{equation}
Before the Electroweak Phase Transition (EWPT) $\mu^2 > 0$ and the vacuum expectation value of the Higgs field is $\langle H_{\textrm min} \rangle = v = 0$ and correspondingly $V_{\textrm min} = 0$. After the
EWPT $\mu^2 < 0$, so the spontaneous symmetry breaking occurs and $\langle H_{\textrm min} \rangle = v \neq 0$. The value of the potential at the minimum is equal to
\[
 V_{\textrm min} = -\frac{m_h^2 v^2}{8} \ , 
\]
with $m_{h} = 125~{\textrm GeV}$ and $v = 246~{\textrm GeV}$ this gives $V_{\textrm min} = -1.2 \cdot 10^{8}~{\textrm GeV}^4$. As we will see later, this value exceeds the observed one for
the vacuum energy density by 55 orders of magnitude. In principle one can always shift the potential by $\frac{m_h^2 v^2}{8}$, so that after the phase transition $V_{\textrm min} = 0$,
implying that $V_{\textrm min}$ was huge before the transition. Therefore, it is clear that it is not possible to put $V_{\textrm min}$ to zero both before and
after EWPT. Anyhow, there are other sources of $\rho_{\textrm vac}$ that we will discuss in the next sections.
\subsubsection{QCD contributions.}
\label{subsubsec:QCD}
It is known that the vacuum state in quantum chromodynamics (QCD) is not empty. The non-perturbative sector of QCD gives rise to gluon and quark ``condensates'' in the vacuum at low energies 
$\langle \bar{q} q \rangle = \rho_{\textrm{vac}}^{\textrm{QCD}}\neq 0$. From dimensional arguments one can write $\rho_{\textrm{vac}}^{\textrm{QCD}} \sim \Lambda_{\textrm{QCD}}^4$, where $\Lambda_{\textrm{QCD}} \simeq 200 - 300~\textrm{MeV}$. 
The proportionality coefficient is  model dependent but different estimates give the following range for $\rho_{\textrm{vac}}^{\textrm{QCD}}$:
\[
 \rho_{\textrm{vac}}^{\textrm{QCD}} \simeq 10^{-3} - 10^{-2}~\textrm{GeV}^4 \ .
\]
\subsection{Quantum contribution to the CC.}
\label{subsec:quantumCC}
In this section we will examine quantum contributions to the cosmological constant. The calculation of these contributions will be performed with a increasing level of complexity and will allow us to rigorously formulate the  problem.
We will start with a toy model: a non-interacting scalar field in (3+1)-dimensions. This relatively simple example will lead us to illustrate the issue of choice of proper regularization scheme when calculating the cosmological constant. 
We will also identify the diagrams which contribute to it. They appear to be divergent even in the case of (1+0)-dimensions (i.e. in Quantum Mechanics) but always cancel out in all the processes in the absence of gravity. We will also
demonstrate how these results can be generalized to the case of a interacting scalar field (picking the $\phi^4$ theory as an illustration) and to the case of fermionic field. We will largely follow this beautiful review~\cite{Martin:2012bt}.
\subsubsection{A toy model: free scalar theory}
\label{sec:toy}
In this subsection we will illustrate the main issues connected to the calculation of ``quantum'' contributions to the Cosmological Constant. We will consider the free scalar field with  potential
\[
 V(\phi) = \frac{m^2 \phi^2}{2} \ .
\]
In flat space-time the equation of motion   is the Klein-Gordon equation
\begin{equation}
 \label{no221_1}
 -\ddot{\phi} + \vec{\partial}_i^{\ 2} \phi - m^2 \phi = 0 \ .
\end{equation}
Its solution can be written as 
\begin{equation}
 \label{no221_2}
 \phi(t,\vec{x}) = \frac{1}{(2\pi)^{\frac{3}{2}}} \int \frac{d^3 \vec{k}}{\sqrt{2\omega(\vec{k})}} \lp c_{k} e^{-i \omega t + i \vec{k} \vec{x}} + c^{\dagger}_{k} e^{i \omega t - i \vec{k} \vec{x}} \rp,
 \quad \omega(\vec{k}) = \sqrt{\vec{k}^2 + m^2}.
\end{equation}
The field can be quantized in the usual way by considering $c_{k}$ and $c^{\dagger}_{k}$ as operators obeying the  commutation relation
\begin{equation}
  \label{no221_2a}
  [c_{k},c^{\dagger}_{k'}] = \delta^{(3)} (\vec{k}-\vec{k'})\ .
\end{equation}
The Hamiltonian of the field is $\mathcal{H} = T_{00} = \frac{1}{2} \dot{\phi}^2 + \frac{1}{2} \partial_{i} \phi \partial_{i} \phi + \frac{m^2 \phi^2}{2}$. It's vacuum expectation value can be easily computed:
\begin{equation}
 \label{no221_3}
\langle \rho\rangle  = \langle T_{00} \rangle = \frac{1}{2} \langle 0 \vert \dot{\phi}^2 \vert 0 \rangle  + \frac{1}{2} \langle 0 \vert  \partial_{i} \phi \partial_{i} \phi \vert 0 \rangle + \frac{m^2}{2} \langle 0 \vert \phi^2 \vert 0 \rangle = 
\frac{1}{2} \frac{1}{(2\pi)^3} \int d^3 \vec{k}~\omega(k)
\end{equation}
The same can be done for the pressure $\langle p \rangle = \langle 0 \vert \frac{1}{3} \bot^{\mu \nu} T_{\mu \nu} \vert 0 \rangle $, where $\bot^{\mu \nu} = g^{\mu \nu} + u^{\mu} u^{\nu}$ with $u^{\mu} = (1,\vec{0})$ is a projector. The same 
calculation as above yields
\begin{equation}
 \label{no221_4}
 \langle p \rangle = \left \langle 0 \left \vert \frac{1}{2} \dot{\phi}^2 - \frac{1}{6} \partial_{i} \phi \partial_{i} \phi - \frac{m^2 \phi^2}{2} \right \vert 0 \right \rangle = \frac{1}{6} \frac{1}{(2\pi)^3} \int d^3 \vec{k} \frac{k^2}{\omega(k)}
\end{equation}
and $\langle 0 \vert T_{0i} \vert 0 \rangle = 0$.
From~\eqref{no1_4b} and~\eqref{no221_3} it follows that in our toy model
\begin{equation}
 \label{no221_5}
 \Lambda_{\textrm{eff}} = \Lambda_{B} + \frac{\kappa}{(2\pi)^3} \int d^3 \vec{k} \frac{\omega(k)}{2}
\end{equation}
is a divergent quantity.

 As it is well known, without gravity only energy differences  can be measured, but the situtation changes drastically in the presence of gravity. Here the vacuum energy ``weighs'' and in principle can be measured. So a careful treatment of
of the divergencies in Eq.~\eqref{no221_5} is needed. Different regularization techniques will be discussed in the two following paragraphs. More details and discussion can be found in~\cite{Martin:2012bt,Akhmedov:2002ts,Koksma:2011cq}.
\paragraph{Cut-off regularization.}
The main idea of this regularization method is to introduce some cut-off scale $M$ for the integral in Eq.~\eqref{no221_3}:
\begin{eqnarray}
 \label{no2211_1}
 & \displaystyle \langle \rho \rangle = \frac{1}{4\pi^2} \int_{0}^{+\infty} dk~k^2~\sqrt{k^2+m^2} \to \frac{1}{4\pi^2} \int_{0}^{M} dk~k^2~\sqrt{k^2+m^2} = \\
 \nonumber
 & \displaystyle =\frac{M^4}{16\pi^2} \left[\sqrt{1+\frac{m^2}{M^2}} \lp 1+ \frac{m^2}{2M^2} \rp - \frac{m^4}{2M^4} \log \lp \frac{M}{m} + \frac{M}{m} \sqrt{1+\frac{m^2}{M^2}} \rp\right] .
\end{eqnarray}
The same approach, when applied to pressure, gives
\begin{equation}
 \label{no2211_2}
\displaystyle \langle p \rangle = \frac{1}{3}\frac{M^4}{16\pi^2} \left[\sqrt{1+\frac{m^2}{M^2}} \lp 1 -\frac{3m^2}{2M^2} \rp + \frac{3m^4}{2M^4} \log \lp \frac{M}{m} + \frac{M}{m} \sqrt{1+\frac{m^2}{M^2}} \rp\right] \ .
\end{equation}
In the limit $M \to +\infty$ these equations read
\begin{eqnarray}
 \label{no2211_2a}
 & \displaystyle \langle \rho \rangle \to \frac{M^4}{16\pi^2} \lp 1+ \frac{m^2}{M^2} \rp \\
 \nonumber
 & \displaystyle \langle p \rangle \to \frac{1}{3}\frac{M^4}{16\pi^2} \lp 1- \frac{m^2}{M^2} \rp
\end{eqnarray}
and therefore the perfect fluid condition is violated
\[
 \langle p \rangle \neq \langle \rho \rangle.
\]
and so,  the stress-energy tensor is not of the form $T_{\mu \nu} \sim g_{\mu \nu}$.
However, if we keep on expanding eqs.~\eqref{no2211_1} and~\eqref{no2211_2}, we  see that next-to-leading-order terms do satisfy equation of state of the vacuum:
\begin{eqnarray}
 \label{no2211_2b}
 & \displaystyle \langle \rho \rangle^{\textrm{log}} = -\frac{m^4}{32\pi^2} \log \lp \frac{M}{m} + \frac{M}{m} \sqrt{1+\frac{m^2}{M^2}} \rp \\ 
 \nonumber
 & \displaystyle \langle p \rangle^{\textrm{log}} = \frac{m^4}{32\pi^2} \log \lp \frac{M}{m} + \frac{M}{m} \sqrt{1+\frac{m^2}{M^2}} \rp \\ 
\end{eqnarray}
Note that these terms scale as $m^4~\log \lp \frac{m}{M} \rp$, instead of the $M^4$ dependence  of the leading order terms, which is not a coincidence. The reason why we failed to reproduce the 
perfect fluid equation of state is that cut-off regularization violates Lorentz symmetry. From here we conclude that this is not a proper way of regularizing the divergent integrals in the case of cosmological constant.

\paragraph{Dimensional regularization.}
Dimensional regularization requires the reformulation of the theory in $d$ dimensions. However, for the free scalar field the equation of motion remains the same as in $d=4$ and its solution can be written in the following form:
\begin{equation}
 \label{no2212_a}
 \phi(t,\vec{x}) = \frac{1}{(2\pi)^{\frac{d-1}{2}}} \int \frac{d^{d-1} \vec{k}}{\sqrt{2\omega(k)}}~\lp c_{k} e^{-i \omega t + i \vec{k} \vec{x}} + c_{k}^{\dagger} e^{i \omega t - i \vec{k} \vec{x}} \rp .
\end{equation}
The expressions for the energy density then takes the form
\begin{equation}
 \label{no2212_b}
\langle \rho \rangle = \frac{1}{2}\frac{\mu^{4-d}}{(2\pi)^{d-1}} \int d^{d-1} \vec{k}~\omega(k) = \frac{\mu^4}{2 (4\pi)^{\frac{d-1}{2}}} \frac{\Gamma\lp -\frac{d}{2} \rp}{\Gamma\lp -\frac{1}{2} \rp} \lp \frac{m}{\mu} \rp^d .
\end{equation}
where a new  scale $\mu$ is introduced for the equation to be dimensionally correct. 
The same calculation can be done for the pressure and it can be verified that the vacuum equation of state $\langle \rho \rangle = - \langle p \rangle$ is indeed reproduced. This was  expected since dimensional regularization respects the Lorentz symmetry.

 We further proceed with the computation of $\langle \rho \rangle$ by taking $d=4-\epsilon$ and expanding for small $\epsilon$ 
\begin{equation}
 \label{no2212_1} 
 \langle \rho \rangle = -\frac{m^4}{64\pi^2} \lp \frac{1}{\epsilon} + \frac{3}{2} - \gamma_{E} - \log \frac{m^2}{4\pi\mu^2} + \mathcal{O}(\epsilon) \rp. 
\end{equation}
In the so-called  $\overline{\text{MS}}$ scheme\footnote{$\overline{\text{MS}}$ stands for modified minimal subtraction. It consists of substracting the divergent term as well as the constant terms inside the brackets. For further details, see ref.~\cite{Martin:2012bt}. This substraction does not affect the following discussion.} the renormalized, finite quantity reads
\begin{equation}
  \label{no2212_2} 
 \langle \rho \rangle^{\textrm{ren}} = \frac{m^4}{64\pi^2} \log \frac{m^2}{\mu^2}. 
\end{equation}
As we see now the vacuum energy density behaves like $\sim m^4 \log \frac{m}{\mu}$ rather than $M^4$. We already encountered this behaviour for the next-to-leading terms in $\frac{m}{M}$ expansion of the vacuum energy in the naive cutoff scheme and 
noticed back then that it is not a coincidence. Because of the logarithmic dependence on the mass of the field the vacuum energy can be either positive or negative, again as opposed to the case of naive cutoff scheme. Interestingly, the 
massless fields (e.g. photons, gluons) do not contribute to the vacuum energy density.

\subsubsection{The vacuum and Feynman diagrams}
\label{subsubsec:Feyn}
In this section we will try to understand better the structure of the divergences we encountered in the previous sections and identify a special class of diagrams which contribute to them. We start with the trace of the stress-energy
tensor in $d$ dimensions:
\begin{equation}
 \label{no222_a}
 \langle T^{\mu}_{\mu} \rangle = \langle T \rangle = - \langle \rho \rangle  + (d-1) \langle p \rangle .
\end{equation}
The expressions for $\langle \rho \rangle$ and $\langle p \rangle$ were derived in the previous section:
\begin{eqnarray}
 \label{no222_b}
 & \displaystyle \langle \rho \rangle = \frac{1}{2}\frac{\mu^{4-d}}{(2\pi)^{d-1}} \int d^{d-1} \vec{k}~\omega(k) \\
 \nonumber
 & \displaystyle \langle p \rangle = \frac{1}{2(d-1)}\frac{\mu^{4-d}}{(2\pi)^{d-1}} \int d^{d-1} \vec{k}~\frac{k^2}{\omega(k)}.
\end{eqnarray}
From~\eqref{no222_a} and~\eqref{no222_b} we get
\begin{equation}
 \label{no222_c}
 \langle T \rangle = -\mu^{4-d} \int \frac{d^{d-1} \vec{k}}{(2\pi)^{d-1}} \frac{m^2}{2\omega(k)}.
\end{equation}
Now, let us recall the expression for Feynman propagator in $d=4$ dimensions:
\begin{equation}
 \label{no222_d}
 D_{F}(x-y) = \frac{i}{(2\pi)^4} \int \frac{d^4 k}{k^2 + m^2 - i \epsilon} e^{i k (x-y)}
\end{equation}
The value of this propagator at coincident points reads
\begin{equation}
 \label{no222_e}
 D_{F}(0) = \frac{i}{(2\pi)^4} \int \frac{d^4 k}{k^2+m^2} = -\int \frac{d^3 \vec{k}}{(2\pi)^3} \frac{1}{2\omega} \ ,
\end{equation}
so by comparing \eqref{no222_c} and~\eqref{no222_e} we find that
\begin{equation}
 \label{no222_1}
 \langle T \rangle = m^2 D_{F}(0) . 
\end{equation}
Moreover, since in $d=4$ dimensions $\langle T \rangle = -\langle \rho \rangle + 3 \langle p \rangle = -4\langle \rho \rangle$, we find that
\begin{equation}
 \label{no222_1a}
 \langle \rho \rangle = -\frac{m^2}{4}D_{F}(0) . 
\end{equation}
The Feynman propagator $D_{F}(x_1-x_2)$ can be represented graphically as a line connecting two points $x_1$ and $x_2$:
\begin{center}
% \begin{fmffile}{Figures/ruleone}
% \parbox{20mm}{
% \begin{fmfgraph*}(40,30)
% %\fmfpen{thick}
% \fmfleft{i}
% \fmfright{o}
% \fmf{plain}{i,o}
% \fmfdot{i}
% \fmfdot{o}
% \fmflabel{$x_1$}{i}
% %\fmflabel{$\tau$}{v}
% \fmflabel{$x_2$}{o}
% \end{fmfgraph*}} 
% \end{fmffile} 
% $\equiv D_{F}(x_1-x_2)$
\begin{figure}[h!]
\centering
\includegraphics[]{Figures/ruleone.pdf}   
 %\caption{Dependence of the first term in ~\eqref{no231_1a} on the renormalization scale $\mu$.}
 %\label{Fig1}
\end{figure}
\end{center}
so the Feynman propagator at the coincident points can be represented as a ``bubble diagram''
\begin{center}
% \begin{fmffile}{Figures/lambdabubble}
% \quad \, \, 
% \parbox{20mm}{
% \begin{fmfgraph*}(40,30)
% %\fmfpen{thick}
% \fmfleft{i}
% \fmfright{o}
% \fmfdot{i}
% %\fmf{phantom}{i1,v,o1}
% \fmfv{decor.shape=circle,decor.filled=0,decor.size=0.5w}{i}
% %\fmfv{decor.shape=circle,decor.filled=0,decor.size=0.5w}{o1}
% \end{fmfgraph*}}
% \end{fmffile}
% $\equiv D_{F}(0)$
\begin{figure}[h!]
\centering
\includegraphics[]{Figures/lambdabubble.pdf}   
 %\caption{Dependence of the first term in ~\eqref{no231_1a} on the renormalization scale $\mu$.}
 %\label{Fig1}
\end{figure}
\end{center}


and we can use this to write 

\begin{center}
$\Lambda_{\textrm{eff}} = \Lambda_{B} - \kappa \dfrac{ m^2}{4} \  $ \begin{fmffile}{lambdabubble}
\parbox{20mm}{
\begin{fmfgraph*}(40,30)
%\fmfpen{thick}
\fmfleft{i}
\fmfright{o}
\fmfdot{i}
%\fmf{phantom}{i1,v,o1}
\fmfv{decor.shape=circle,decor.filled=0,decor.size=0.5w}{i}
%\fmfv{decor.shape=circle,decor.filled=0,decor.size=0.5w}{o1}
\end{fmfgraph*}}
\end{fmffile}
\end{center} 


\subsubsection{Interacting theories}
So far, we have been working in the framework of free theories. Now we are going to see how the result obtained in the previous section changes in the presence of interactions. As an example we consider $\phi^4$ theory
by adding a term in the potential
\begin{equation}
 \label{no2222_a}
 V_{\textrm{int}} = \frac{\lambda \phi^4}{4!} \ .
\end{equation}
At first glance, this new term in the lagrangian generates new contribution to the vacuum energy density: ???
\begin{equation}
 \label{no2222_b}
 \Delta \rho = \frac{\lambda}{4!} \langle \phi^4 \rangle = \frac{3\lambda}{4!} \langle \phi^2 \rangle^2 = \frac{\lambda}{8} D_{F}(0)^2 \ .
\end{equation}
However, we have to bare in mind that the mass of the particle has to be renormalized in the presence of interactions. The one-loop expression for the renormalized mass is:
\begin{equation}
 \label{no2222_c}
\underbrace{m_{\textrm{ren}}^2}_{\scriptstyle \text{renormalized mass}}  = \underbrace{m^2}_{\scriptstyle \text{bare mass}}- \frac{\lambda}{2}D_{F}(0) \ .
\end{equation}
Thus the expression for vacuum energy density is
\begin{equation}
\label{no2222_d}
\langle \rho \rangle = -\frac{m^2}{4} D_{F}(0) + \frac{\lambda}{8} D_{F}(0)^2 = -\frac{m_{\textrm{ren}}^2}{4} D_{F}(0) - \frac{1}{4}\left[\frac{\lambda}{2}D_{F}(0)\right] D_{F}(0) + \frac{\lambda}{8}D_{F}(0)^2 = -\frac{m_{\textrm{ren}}^2}{4}D_{F}(0) \ .
\end{equation}
Thereby, at the one-loop level the expression for the vacuum energy density coincides with the corresponding expression in the free theory, provided that the final result is expressed in terms of the renormalized mass rather than the bare mass.
Several comments here are in order:
\begin{itemize}
 \item The fact that ``naive cut-off scheme'' led us to quartic divergences $\langle \rho \rangle \sim M^4$ and non-physical behavior of $\langle \rho \rangle$ (i.e. $\langle \rho \rangle \neq \langle p \rangle$) 
does not mean {\textit per se} that there is no relativistically invariant regularization that leads to $\mathcal{O}(M^4)$ terms in $\langle \rho \rangle$. However, eq.~\eqref{no222_1a} rules out such a possibility.
 \item ``Bubble diagrams'' can be used in quantum mechanics for e.g. calculation of shifts of energy levels in case of interaction. Consider, for example, the quantum harmonic oscillator with a quartic interaction
 \begin{eqnarray}
  \begin{aligned}
   & H = \frac{m \dot{x}^2}{2} + \frac{m \omega^2 x^2}{2} + \frac{\lambda m^2 x^4}{4!} = H_{0}+H_{\textrm{int}}, \\
   & H_{0} = \frac{\omega}{2} \lp cc^{\dagger} + c^{\dagger} c \rp \quad {\textrm with} \quad [c,c^{\dagger}] = 1, \\
   & H_{\textrm{int}} = \frac{\lambda m^2 x^4}{4!}.
  \end{aligned}
 \end{eqnarray}
 One can define the ``propagator'' $D(t_1,t_2) = \langle 0 \vert T[\phi(t_1) \phi(t_2)] \vert 0 \rangle$, where $\phi = \sqrt{m}x$. The explicit expression for it reads:
 \begin{equation}
  D(t_1,t_2) = \frac{1}{2\omega} e^{-i \omega \vert t_2 - t_1 \vert}
 \end{equation}
 One can calculate the energy shift of the ground level caused by the interaction in two ways. First, using the standard non-relativistic perturbation theory:
 \begin{equation}
 \Delta E_{n} = E_{n} - E_{n}^{0} = \langle n \vert H_{\textrm{int}} \vert n \rangle
 \end{equation}
 The explicit calculation yields
 \begin{equation}
  \Delta E_{0} = \frac{\lambda}{32 \omega^2}
 \end{equation}
 The alternative way is to recall that quantum mechanics can be thought of as $(1+0)$-dimensional QFT and use the expression~\eqref{no2222_b}:
 \begin{equation}
  \Delta E_{0} = \frac{\lambda}{4!} \langle \phi^4 \rangle = \frac{\lambda}{8} D(0)^2 = \frac{\lambda}{32 \omega^2}
 \end{equation}
 Furthermore one can define ``renormalized'' frequency $\omega_{\textrm{ren}}$ in a similar fashion as we defined renormalized mass above:
 \begin{equation}
  \omega_{\textrm{ren}}^2  = \omega^2 + \frac{\lambda}{2}D_{F}(0)
 \end{equation}
 In quantum mechanics the propagator is finite and hence ``bare'' quantities are finite as well:
 \begin{equation}
 \omega_{\textrm{ren}}  = \omega + \frac{\lambda}{8 \omega^2}
 \end{equation}
 It is interesting that in the presence of interaction the energy difference between the first exited state and the ground state is precisely equal to the ``renormalized'' frequency $E_{1} - E_{0} = \omega_{\textrm{ren}}$.  See Sec.~V. D. of~\cite{Martin:2012bt} for an extensive discussion. 
 \item In non-gravitational physics bubble diagrams always cancel out in the equations which describe observable quantities. However, when (classical) gravity is switched on, the equivalence principle tells us that all froms of energy 
 gravitate and the vacuum energy is no exception. One can think of it as of an infinite row of bubble diagrams with $1,2,...,n$ gravitons attached as  external legs.
 \item Up to now we have been considering flat space-time. We also made use of a semi-classical picture where quantum matter is coupled to classical gravity (see eq.~\eqref{no1_4}).  This may seem inconsistent since we have argued that the problem occurs only in curved space-time. However, in   curved space-time the curvature only 
 influences the modes with   wavelength $k^{-1} \geq a$, where $a$ is the local curvature radius. Since the ultraviolet (UV) divergences correspond to   wavelengths $k^{-1} \ll a$, their nature does not depend on   
 whether the space-time is flat or not. Hence, we can expect the same expression for the vacuum energy even in the curved space-time case. For a rigorous analysis of this fact see Sec.~VIII of~\cite{Martin:2012bt}.
 \item By now we only worked with spin-0 fields. The general expression for the contribution to the vacuum energy from a field with spin $s_n$ is~\cite{Martin:2012bt}
 \begin{equation}
  \label{no2222_e}
  \langle \rho_n \rangle_{\textrm{ren}} = (-1)^{2s_{n}} (2s_n+1) \frac{m_n^4}{64\pi^2} \log \frac{m_n^2}{\mu^2} \ .
 \end{equation}
 Note that fermions make negative contributions.
 \end{itemize}

\subsection{The cosmological constant problem: its two sides}
\label{subsec:twoside}
\subsubsection{The value of the cosmological constant}
\label{subsubsec:value}
We start by putting together all contributions to $\rho_{\textrm{vac}}$ mentioned in the previous sections~\footnote{Interestingly, massless gauge bosons do not contribute to~\eqref{no231_1} contrary to what
a naive cut-off would predict.}
\begin{equation}
 \label{no231_1}
 \rho_{\textrm{vac}} = \underbrace{\sum_{i} n_{i} \frac{m_i^4}{64\pi^2} \log \frac{m_i^2}{\mu^2}}_{\text{zero-point fluctuations}} 
 + \underbrace{\rho_{B}}_{\text{``bare'' CC}} + \rho_{\textrm{vac}}^{\textrm{EW}} + \rho_{\textrm{vac}}^{\textrm{QCD}} + \underbrace{\cdots}_{\text{other possible phase transitions}}
\end{equation}
where $n_i$ is given by the   coeficient in (\ref{no2222_e}) times the number of degrees of freedom of the field.  Let us now compute the first term more precisely for the Standard Model. In this case one has the Higgs boson with 
$n_H = 1$ and $m_H = 125.09~{\textrm GeV}$, six quarks with $n_{q} = -12$ each and masses $m_{t} = 173.1~{\textrm GeV},\,m_{b} = 4.18~{\textrm GeV},\,m_{c} = 1.28~{\textrm GeV},\,m_{s} = 0.096~{\textrm GeV},\,m_{u} = 2.2 \cdot 10^{-3}~{\textrm GeV},
\,m_{d} = 4.7 \cdot 10^{-3}~{\textrm GeV}$, three leptons with $n_{l} = -4$ each and masses $m_{\tau} = 1.776~{\textrm GeV},\,m_{\mu} = 0.105~{\textrm GeV},\,m_{e} = 5.1 \cdot 10^{-4}~{\textrm GeV}$ and two massive gauge bosons with
$n_{Z} = 3, \, m_{Z} = 91.19~{\textrm GeV}$ and $n_{W^{\pm}} = 6, \, m_{W^{\pm}} = 80.385~{\textrm GeV}$~\cite{Patrignani:2016xqp}. 

Now we have to determine the renormalization scale, which enters~\eqref{no231_1}. It was argued in
~\cite{Koksma:2011cq} that since we use photons from the supernovae to determine the cosmological constant and since these photons couple to the metric, the relevant renormalization scale can be taken as 
$\mu \sim \sqrt{s} \sim \sqrt{E_{\gamma} E_{\textrm grav}}$ with $E_{\textrm grav} \simeq H_{0} = 3.7 \cdot 10^{-41}~{\textrm GeV}$.
These photons have a typical wavelength of $\lambda \simeq 500~{\textrm nm}$ which corresponds to $E_{\gamma} \simeq 2.48~{\textrm eV}$. This gives $\mu = 3 \cdot 10^{-25}~{\textrm GeV}$ and
\begin{equation}
  \label{no231_1a}
\sum_{i} n_{i} \frac{m_i^4}{64\pi^2} \log \frac{m_i^2}{\mu^2} \simeq -2 \cdot 10^{9}~{\textrm GeV}^4
\end{equation}
Because of the logarithmic behaviour of~\eqref{no231_1a}, its value remains in the ballpark of $10^{6} - 10^{9}~{\textrm GeV^4}$ in the wide range of renormalization scales $\mu$ as one can see on Fig.~\ref{Fig1}.
\begin{figure}[ht]
\centering
\includegraphics[]{Figures/rho.pdf}   
 \caption{Dependence of the first term in ~\eqref{no231_1a} on the renormalization scale $\mu$.}
 \label{Fig1}
\end{figure}
Cosmological observations imply that $\rho_{\textrm vac} \leq 10^{-47}~{\textrm GeV}^4$ so regardless of the precise value of $\mu$ there is a discrepancy of $53 - 56$ orders of magnitude
between the prediction and the data. The same applies to each of the terms in~\eqref{no231_1}. This is a cosmological constant problem. Another, more technical, formulation of 
this problem will be discussed in the next section.
\subsubsection{Radiative instability.}
\label{subsubsec:radiativeinst}
Let us come back to the $\phi^4$ theory discussed in the previous sections~\footnote{In this paragraph we will largely reproduce the reasoning of~\cite{Padilla:2015aaa}. We refer the reader to these lectures for further details.}. 
We have calculated contribution to the vacuum energy at the one-loop level:
\begin{equation}
 \label{no232_1}
 \rho_{\textrm{vac}}^{1L} \sim -\frac{m^4}{64 \pi^2} \lp \frac{1}{\epsilon} + \log \frac{\mu^2}{m^2} + \textrm{finite} \rp
\end{equation}
The counterterm is needed to make it finite:
\begin{equation}
 \label{no232_2}
 \delta\rho_{\textrm{vac}}^{1L} \sim \frac{m^4}{64\pi^2} \lp \frac{1}{\epsilon} + \log \frac{\mu^2}{m^2} \rp
\end{equation}
The renormalized quantity is then finite:
\begin{equation}
 \label{no232_3}
 \rho_{\textrm{vac}}^{1L,ren} \sim \frac{m^4}{64\pi^2} \lp \log \frac{m^2}{M^2} + \textrm{finite} \rp
\end{equation}
In principle we can infer the value for $\rho_{\textrm{vac}}^{1L,ren}$ from the measurement and thus fix the scale $M$. As we have seen above this fixing has to be made with tremendous accuracy of
one part in $10^{54}$. Next, we can calculate two loop corrections to the vacuum energy. From dimensional arguments it is clear that the result will be proportional to $\lambda~m^4$. In the Standard Model, for example, 
$\lambda \sim \mathcal{O}(0.1)$ which means that the cancellation we imposed at one loop is completely spoilt and we have to retune the value of $M$ again with the level of accuracy equal to $ \sim 10^{-54}$. 
The same will happen in every theory without specially fine-tuned couplings. We can expect that we will have to repeat this procedure at every order or perturbation theory. This simple reasoning tells us that 
the value of vacuum energy is super-sensitive to the UV details of the theory which we do not know. This is called ``radiative instability''.

\section{Casimir energy}
\label{sec:Casimir}
Now that we have seen that zero-point fluctuations manifest themselves as   vacuum energy and that calculations  lead to a   discrepancy between theory and experiment,
a fair question to ask is whether these zero-point fluctuations really exist or are   just an artifact of quantum field theory. The experiments which are considered
as a proof of existance of vacuum fluctuations are the measurements of the Lamb shift and Casimir force. We will discuss the latter in this section.\footnote{In this section we will make use of the following notation for the metric $\lp +1,-1 \rp$ }

For the sake of simplicity we will consider a toy model of a massless scalar field $\phi$ in $(1+1)$-dimensions, confined between two ``plates'' (i.e. points). The distance between the plates is $L$. 
In this way the equation of motion for the field together with the boundary conditions read:
\begin{equation}
\label{no3_1}
 \begin{cases}
  \partial^{\mu} \partial_{\mu} \phi = 0 \\
  \phi(0,t) = \phi(L,t) = 0
 \end{cases} .
\end{equation}
We seek solutions to~\eqref{no3_1} in the form $\phi(x,t) = f(x)\psi(t)$, where $f(0) = f(L) = 0$. This leads us to the fundamental set of solutions $f_{n}(x) = \sin \lp \frac{\pi n}{L} x \rp$
so the general solution to~\eqref{no3_1} can be written as
\begin{equation}
 \label{no3_1a}
 \phi(x,t) = \sum_{n=1}^{+\infty} \frac{\sin \lp \frac{\pi n}{L} x \rp}{\sqrt{L k_n}} \lp a_{n}^{+} e^{i k_{n} t} + a_{n}^{-} e^{-i k_{n} t} \rp \quad , \quad k_n=\dfrac{\pi n}{L} \ .
\end{equation}
The Hamiltonian of the system reads
\begin{equation}
 \label{no3_1b}
 \mathcal{H} = \int_{0}^{L} dx~ \lp \frac{\dot{\phi}^2}{2} + \frac{(\partial_x \phi)^2}{2} \rp = \frac{1}{2} \sum_{n=1}^{+\infty} k_n~\lp a_n^{+}a_n^{-} + a_n^{-}a_n^{+} \rp
\end{equation}
where $a_{n}^{\pm}$ are operators which obey the following commutation relations:
\begin{eqnarray}
\begin{aligned}
 & [a_n^+, a_m^+] = 0 \\
 \nonumber
 & [a_n^-,a_m^-] = 0\\
 \nonumber
 & [a_m^-, a_n^+] = \delta_{mn}
\end{aligned}
\end{eqnarray}
and conditions
\begin{equation}
 a_{n}^{-} \vert 0 \rangle = 0, \quad \langle 0 \vert a_{n}^{+} = 0 \ .
\end{equation}
The vacuum energy density in the presence of plates is
\begin{equation}
\label{no3_2}
\rho_{1} =  \langle 0 \vert \mathcal{H} \vert 0 \rangle = \left \langle 0 \left \vert \frac{\dot{\phi}^2}{2} + \frac{(\partial_x \phi)^2}{2} \right \vert 0 \right \rangle
= \frac{1}{2L} \sum_{n=1}^{+\infty} k_{n} = \frac{\pi}{2L^2} \sum_{n=1}^{+\infty} n \ .
\end{equation}
In the absence of plates the system is a free massless scalar field in $(1+1)$ dimensions whose vacuum energy density we calculated in Sec.~\ref{sec:toy}:
\begin{equation}
\label{no3_3}
\rho_2 = \int_{0}^{+\infty} \frac{dk_0}{2\pi}~k_0  \ .
\end{equation}
In the presence of plates there is a change in the vacuum energy density 
\begin{equation}
 \label{no3_4}
 \Delta \rho = \rho_1 - \rho_2 = \frac{\pi}{2L^2} \lp \sum_{n=1}^{+\infty} n - \int_{0}^{+\infty} dx ~ x \rp \ .
\end{equation}
To calculate the difference between the infinite sum and infinite integral which appear in~\eqref{no3_4} we need to utilize the regulator $f \lp \frac{x}{L\Lambda} \rp$.\footnote{The following calculations are largely based on the ones in Chapter 15 of~\cite{Schwartz:2013pla}.} By doing this we find that
\begin{eqnarray}
 \label{no3_5}
 \begin{aligned}
 & \sum_{n=1}^{+\infty} n f\lp \frac{n}{L \Lambda} \rp - \int_{0}^{+\infty} dx~x f \lp \frac{x}{L \Lambda} \rp = 
 \lim_{M \to +\infty} \left[ \sum_{n=1}^M n f\lp \frac{n}{L \Lambda} \rp - \int_{0}^M dx~x f \lp \frac{x}{L \Lambda} \rp \right] = \\
 \nonumber
 & = \lim_{M \to +\infty} \frac{F(0)+F(M)}{2} + \frac{F'(M)-F'(0)}{12} + \ldots B_j \frac{F^{(j-1)}(M)-F^{(j-1)}(0)}{j!},
 \end{aligned}
\end{eqnarray}
where $F(x) = x f \lp \frac{x}{L \Lambda} \rp$ and $B_j$ are Bernoulli numbers. Higher derivatives of $F(x)$ can be calculated explicitly
\begin{equation}
\label{no3_5a}
F^{(j)} = \lp \frac{1}{L \Lambda} \rp^{j-1} \left[ \frac{x}{L \Lambda} f^{(j)}(x) + j f^{(j-1)}(x)\right] \ .
\end{equation}
We will assume that regulating function has the following properties
\begin{equation}
\label{no3_5b}
\lim_{x \to +\infty} x f^{(j)}(x) = 0, \quad f(0) = 1 .
\end{equation}
The physical meaning of the first condition is that ultraviolet modes are not confined between the plates but rather go right through them. The second one means that the
regulator does not affect the spectrum in the IR. The regulators that fulfill these conditions are e.g. $e^{-x}, e^{-x^2},x^{-s} (s>0)$. 
From~\eqref{no3_5a} and~\eqref{no3_5b} one gets
\begin{eqnarray}
 \begin{aligned}
  & F^{(j)}(M) \to 0~{\textrm when}~M \to +\infty, \\
  & F(0) = 0, \\
  & F^{(j)}(0) = \lp \frac{1}{L\Lambda} \rp^{j-1} j f^{(j-1)}(0)
 \end{aligned}
\end{eqnarray}
Hence we get 
\[
 \sum_{n=1}^{+\infty} n f\lp \frac{n}{L\Lambda} \rp - \int_{0}^{+\infty} dx~x f \lp \frac{x}{L\Lambda} \rp = -\frac{f(0)}{12} + \mathcal{O} \lp \frac{1}{L\Lambda} \rp
 = -\frac{1}{12} + \mathcal{O} \lp \frac{1}{L\Lambda} \rp
\]
and 
\[
 \Delta \rho = -\frac{\pi}{24 L^2}. 
\]
Thus, the shift in the vacuum energy when the plates are present compared to the case when they are absent is
\begin{equation}
 \Delta E = -\frac{\pi}{24 L}
\end{equation}
This energy shift is negative which indicates that the ``plates'' are attracted to each other. This attractive force was observed in the experiment conducted in 1996~\cite{Lamoreaux:1996wh}.
More examples of the calculation of the Casimir energy can be found in the monograph~\cite{Milton:2001yy}. In particular, some relavant results generalizing the previous analysis are:
\begin{itemize}
 \item The case of $d$-dimensional plates (i.e. a massless scalar field in $d+1$ spatial dimensions) gives a Casimir energy of
 \[
  \Delta E = -\frac{1}{2^{d+2} \pi^{\frac{d}{2}+1}} \frac{1}{L^{d+1}} \Gamma \lp 1+\frac{d}{2} \rp \xi(2+d) 
 \]
 \item For a electromagnetic field between conducting plates in $(3+1)$ dimensions we have that
 \[
  \Delta E = - \frac{\pi^2}{720 L^3} 
 \]
 \item And for fermions, the Casimir energy is
 \[
  \Delta E = -\frac{7\pi^2}{5760 L^3}
 \]
\end{itemize}


\section{Outlook}
\label{sec:conclusion}
In this chapter we briefly introduced the problem of the vacuum energy and cosmological constant problem. Vacuum fluctuations exist and as all other sources of energy they gravitate. However, the number predicted by the Standard Model differs from 
the experimental one by many orders of magnitude. We have seen that the very formulation of the problems requires some attention and a proper  regularization of the theory. It can be said that the vacuum energy comes from the so-called
``bubble diagrams'' which always cancel out in the quantum field theory calculations in   flat space-time. However, they become visible in the presence of gravity and require special attention. To argue that vacuum fluctuations indeed
exist and are not just the artefacts of the calculations we shortly reviewed the Casimir effect. In this case the formalism of   quantum field theory works flawlessly and the results are in perfect agreement with the
experiment. It should be pointed out that the Casimir effect is not the only  effect revealing the existence of vacuum fluctuations. We refer the interested reader to~\cite{Martin:2012bt} for the description of The Collela, Overhausser and Werner
(COW) Experiment, The Quantum Galileo Experiment and more. There have been many attempts to attack the cosmological constant problem and we will discuss many of the ideas in the following sections.


