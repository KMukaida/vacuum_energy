%!TEX root = ../VacuumEnergy.tex

\chapter{The Cosmological Constant Problem}
\label{cha:nogo}


\emph{``Physics thrives on crisis. [\ldots] Unfortunately, we have run
  short on crisis lately.''}\\
\phantom{.} \hfill (Steven Weinberg, 1989 \cite{Weinberg:1988cp})

\section{Introduction: the Cosmological Constant}
Besides the fact, that we are still running short on severe crises in
physics also about 30 years after Weinberg's statement, there is still
yet no solution to what is called the ``Cosmological Constant
problem''. Neither is there any clue what the Cosmological Constant (CC)
is made of and if the problem is indeed an outstanding problem.

Weinberg defines and proves in his review on the Cosmological
Constant~\cite{Weinberg:1988cp} a \emph{``no-go'' theorem}. This no-go
theorem is actually \emph{not} a theorem on the smallness of the CC in
the sense of a Vacuum Energy,\footnote{Weinberg does not argue the
  Vacuum Energy to be small, which is rather what we would expect today;
  instead, he focuses on flat and static solutions for the Einstein
  equations with a spatially constant set of matter fields corresponding
  to an isotropically and homogeneously filled universe.}  it is rather
a theorem prohibiting any kind of \emph{adjustment mechanisms} that lead
effectively to a universe with a flat and static (i.\,e.~Minkowski)
space-time metric in the presence of a CC. In other words: with a CC
there is no Minkowski universe possible.

\paragraph{An old crisis}
When Einstein first formulated his field equations for General
Relativity, he was neither aware of a possible Cosmological Constant nor
of an expanding universe solution to them. Originally, the proposed
equations are
\begin{equation}
R_{\mu\nu} - \frac{1}{2} g_{\mu\nu} \; R = -8\pi G \; T_{\mu\nu},
\end{equation}
using Weinberg's \((-+++)\)-metric. The matter content is given by the
energy-stress tensor \(T_{\mu\nu}\), where the geometry of space-time is
encoded in the Ricci tensor \(R_{\mu\nu}\) and the scalar curvature \(R
= g^{\mu\nu} R_{\mu\nu}\) of the metric \(g_{\mu\nu}\). The
gravitational coupling strength is given by Newton's constant
\(G\). Solutions to this set of equations published in 1915 were found
to have continuously expanding space-times. Applying this result to the
whole universe worried Einstein, who believed (for observational
reasons\footnote{``The most important fact that we draw from experience
  is that the relative velocities of the stars are very small as
  compared with the velocity of light.'' \cite{Weinberg:1988cp}}) in a
static solution.

A static solution, however, was easily obtained by a slight modification
of the initial set of equations with a new constant parameter
\(\Lambda > 0\), known as the Cosmological Constant:\footnote{Weinberg,
  however, shows in his review~\cite{Weinberg:1988cp} that such a
  solution does not exist (under certain assumptions).}
\begin{equation}
R_{\mu\nu} - \frac{1}{2} g_{\mu\nu} \; R - \Lambda\; g_{\mu\nu}
= -8\pi G \; T_{\mu\nu}\;.
\end{equation}
For a static universe filled with pressureless dust, the mass density can be
found to be
\begin{equation}
\rho = \frac{\Lambda}{8\pi G}\;.
\end{equation}
Even the mass and size of the universe can be then determined from the
fundamental parameters of the theory. The radius of the \(S_3\) sphere
universe is given by
\[
r = \frac{1}{\sqrt{8\pi \rho G}} \qquad\qquad
\text{and the mass} \qquad\qquad
M = 2\pi^2 r^3 \rho = \frac{\pi}{4} \frac{1}{\sqrt{\Lambda}G}\;.
\]

This solution was based on a simple and well-motivated assumption: a
homogeneous and isotropic universe. However, besides the discovery of
the expansion of the universe by Hubble in 1929, which lead Einstein
call his Cosmological Constant his biggest folly (``\emph{gr\"o\ss{}te
  Eselei}''), de Sitter proposed in 1917 another static solution with
no matter at all! (One may ask if this is a valid approximation for the
universe, although we know that it is mainly empty.) This solution can
explain redshift which increases with distance; and although the metric
is time-independent, testbodies in it are not at rest. The line element
is given by the expression
\begin{equation}
d\tau^2 = \frac{1}{\cosh^2Hr} \left[
  dt^2 - dr^2 - H^{-2} \tanh^2Hr\left(d\theta^2 + \sin^2\theta
    d\varphi^2\right) \right],
\end{equation}
with the constant \(H = \sqrt{\Lambda/3}\) and \(\rho = p = 0\).

Also de Sitter's solution needs a CC term for the static solution; an
expanding universe, however, can live without and this is described by
the Friedmann--Lema\^itre--Robertson--Walker metric
\begin{equation}
d\tau^2 = dt^2 - a^2(t) \left[
  \frac{dr^2}{1 - kr^2} + r^2 \left( d\theta^2 + \sin^2\theta d\varphi^2
  \right) \right],
\end{equation}
defining \emph{comoving} coordinates with a cosmic scale factor
\(a(t)\). Its time-evolution is given by
\begin{equation}
\left[ \frac{da}{dt} \right]^2 = -k + \frac{1}{3} a^2 \left( 8\pi G \rho
  + \Lambda \right),
\end{equation}
which is an energy-conservation equation. The de Sitter model can be
found with \(k = 0\) and \(\rho = 0\); there are also expanding
solutions with \(\Lambda = 0\) and \(\rho > 0\).

Weinberg~\cite{Weinberg:1988cp} cites Pais (1982) quoting Einstein in a
letter to Weyl 1923 after the discovery of the universe expansion:
``\emph{If there is no quasi-static world, then away with the
  cosmological term!}''. Weinberg himself provides now a no-go theorem
that even states the contrary: the presence of such a cosmological term
forbids a quasi-static world!

\section{The Problem}
A modern formulation of the CC problem is quite different from just
being another fine-tuning problem. It is a problem of radiative
stability, as outlined in the lecture notes by
Padilla~\cite{Padilla:2015aaa}. Especially quantum corrections might
generate a vacuum energy that is not present at the classical level and
every energy density of the vacuum acts as cosmological
constant. Because of Lorentz invariance, the equation
\[
\langle T_{\mu\nu} \rangle = - \langle \rho \rangle g_{\mu\nu}
\]
holds and redefines the cosmological constant as \(\Lambda_\text{eff} =
\Lambda + 8 \pi G \langle \rho \rangle\). Correspondingly, the total
vacuum energy is given by \(\rho_V = \langle \rho \rangle +
\frac{\Lambda}{8\pi G} \equiv \frac{\Lambda_\text{eff}}{8\pi G}\).

Now, we can estimate \(\Lambda_\text{eff}\) from the redshift of an
expanding universe,
\begin{equation}
  \left[ \frac{1}{a} \frac{da}{dt} \right]_\text{today} \equiv H_0
  \simeq 50 \ldots 100 \, \mathrm{km/s \, Mpc}
  \simeq \left(\frac{1}{2} \ldots 1 \right) \times 10^{-10} / \mathrm{yr}.
\end{equation}
The universe is known to be rather flat, so \(|k| / a^2_\text{today}
\lesssim H_0^2\), and thus, in comparison with the critical density,
\begin{equation}
|\rho - \langle \rho \rangle | \lesssim 3 H_0^2 / 8\pi G,
\end{equation}
the effective CC can be estimated to be \(|\Lambda_\text{eff}| \lesssim
H_0^2\), and the total vacuum energy
\begin{equation} \label{eq:rhoV}
|\rho_V| \lesssim 10^{-29} \mathrm{g/cm^3} \approx 10^{-47}\,
\mathrm{GeV}^4 \;.
\end{equation}
This potentially crude estimate results in a rather small number, taking
possible contributions from High Energy physics into account. Actually,
any quantum field theory supplies in general a much larger vacuum
energy. For any field with mass \(m\), the summation of the zero-point
energies for all normal modes (i.\,e.~the one-loop vacuum bubbles) up to a
given cut-off \(M \gg m\) results in
\begin{equation}
\langle \rho \rangle = \int_0^M \frac{4\pi k^2 dk}{(2\pi)^3}
\frac{1}{2} \sqrt{k^2 + m^2} \simeq \frac{M^4}{16\pi^2} \;.
\end{equation}
That means, with a cut-off at the Planck-scale, \(M \simeq
1/\sqrt{8\pi G}\), we estimate
\begin{equation} \label{eq:cutoff}
\langle \rho \rangle \approx 2^{-10} \pi^{-4} G^{-2} = 2 \times
10^{71}\,\mathrm{GeV}^4.
\end{equation}
Apparently, the term of Eq.~\eqref{eq:cutoff} has to cancel with 
\(\Lambda\) to more than 118 digits! It does not help to consider the
QCD-scale as probable cut-off, such that \(\langle \rho \rangle \sim
\Lambda^4_\text{QCD}/16\pi^2 \approx 10^{-6} \,\mathrm{GeV}^4\), which
still needs a cancellation up to 41 digits. Other estimates have been
performed e.\,g.~by Zeldovich, who ``for no clear
reasons''~\cite{Weinberg:1988cp} took \(M =
1\,\mathrm{GeV}\). This leads, of course, to a much smaller vacuum
energy if the one-loop terms are canceled by \(\Lambda/ 8\pi G\) and the
other only contribute as higher-order effect. Thus, for \(M^3\)
particles of energy \(\Lambda\) per unit volume gives with \(M =
1\,\mathrm{GeV}\)
\[
\langle \rho \rangle \approx \left(\frac{G M^2}{M^{-1}}
\right) M^3 = G M^6 \approx 10^{-38}\,\mathrm{GeV}.
\]

The ``\emph{real [\ldots] serious worry}''~\cite{Weinberg:1988cp},
however, begins when one tries to take spontaneous symmetry breaking in
the electroweak sector into account. The scalar field potential
\begin{equation}
V = V_0 - \mu^2 \Phi^\dag \Phi + g \left(\Phi^\dag \Phi\right)^2
\end{equation}
with \(\mu^2 > 0\) and \(g>0\) takes a value at its minimum which
corresponds to a vacuum energy
\begin{equation}
\langle \rho \rangle = V_\text{min} = V_0 - \frac{\mu^4}{4g}\;.
\end{equation}
If we assume the potential to vanish at the origin, \(V(\Phi = 0) = V_0
= 0\), the energy density is found to be
\begin{equation}
\langle \rho \rangle \simeq -g (300\,\mathrm{GeV})^4 \simeq 10^6 \,
\mathrm{GeV}^4,
\end{equation}
for \(g \approx \alpha^2\), which is still too large by a factor of
\(10^{53}\). However, neither \(V_0\) nor \(\Lambda\) must vanish, so a
cancellation is still possible.

Things may get more complicated when the thermal history of the universe
is taken into account. At early times, temperature effects drive the
minimum to be in the symmetric phase, so \(\Phi = 0\), because of a
positive temperature coefficient \(\sim \Phi^\dag \Phi\). Now, compared
with the value of the potential at the minimum today, if this has to be
zero because of zero CC today, formerly \(V(\Phi = 0) = V_0\) and thus
an enormous CC before the electroweak phase transition. This large early
CC can drive inflation and is not necessarily seen to be bad. The issue
is nevertheless, why is the CC small \emph{today}.

In summary, we can be assured that there is a CC around because any
reasonable theory produces it---and during the cosmological evolution it
changed its value. So even if there is some magic cancellation happening
today or even if it is just a small number, the cosmological term is
there in Einstein's equations. Weinberg argues briefly, before preparing
the ground for the proof of this no-go theorem, why there are no
constant flat-space solutions to the equations in the presence of such a
term.\footnote{The crucial point is, that there are no static solutions
  without! Einstein introduced the CC term in order to balance the
  expansion; the solution, however, is a tuned one.}

``\emph{That is, the original symmetry of general covariance, which is
  always broken by the appearance of any given metric \(g_{\mu\nu}\),
  cannot, without fine-tuning, be broken in such a way as to preserve
  the subgroup of space-time translations.}''~\cite{Weinberg:1988cp}

In mathematics, that means we are looking for solutions of General
Relativity with all fields constant over the full space in order to have
translational invariance. The field equations are
\begin{equation} \label{eq:eom}
\frac{\partial \mathcal{L}}{\partial \psi_i} = 0\;, \qquad
\frac{\partial \mathcal{L}}{\partial g_{\mu\nu}} = 0\;.
\end{equation}

There are \(N\) generic fields \(\psi\) around and the metric is
symmetric, so there are \(N+6\) equations for \(N+6\) unknowns. The
first set of equations can be easily satisfied; a
\(\operatorname{GL}(4)\) symmetry survives. That means under the
transformations
\begin{equation}
g_{\mu\nu} \to {A^\rho}_\mu \, {A^\sigma}_\nu \, g_{\rho\sigma} \;
\qquad \text{and} \qquad
\psi_i \to \mathcal{D}_{ij}(A)\, \psi_j \;,
\end{equation}
the Lagrangian density transforms as
\begin{equation}
\mathcal{L} \to \det(A)\, \mathcal{L} \;.
\end{equation}
With the \(\psi\) fields constant, Eqs.~\eqref{eq:eom} have a unique
solution
\begin{equation}
\mathcal{L} = c \sqrt{\det(g)},
\end{equation}
with a constant \(c\) independent of \(g_{\mu\nu}\). The second part of
Eqs.~\eqref{eq:eom} can only be satisfied for a vanishing \(c\).

\section{A No-go Theorem}
``\emph{No-go theorems are a way of relying on apparently technical
  assumptions that later turn out to have exceptions of great physical
  interest.}''~\cite{Weinberg:1988cp}\footnote{One of the most famous
  no-go theorems and its exception is the Coleman--Mandula theorem and
  Supersymmetry as proposed by Haag, \L{}opusza\'nski and Sohnius.}

\paragraph{Adjustment mechanism}
We relax the translational invariance condition posed in
Eqs.~\eqref{eq:eom} and do not impose the two sets of equations to hold
independently but
\begin{equation} \label{eq:adjust}
  g_{\lambda\nu} \, \frac{\partial\mathcal{L}(g, \psi)}{\partial
    g_{\lambda\nu}} = \sum_n^N \frac{\partial\mathcal{L}(g,
    \psi)}{\partial \psi_n} \, f_n(\psi)\;,
\end{equation}
with some coefficient functions \(f_n(\psi)\) and constant fields
\(g_{\mu\nu}\) and \(\psi_i\). This describes an equilibrium solution in
which \(g_{\mu\nu}\) and all the fields \(\psi_i\) adjust in such a way
that they are constant in space-time.

This can be rephrased in a symmetry-condition:
\begin{equation} \label{eq:symc}
\delta g_{\lambda\nu} = 2\varepsilon g_{\lambda\nu} \;, \qquad
\delta \psi_n = - \varepsilon f_n(\psi) \;.
\end{equation}
Once again, we want to have constant fields over space-time, so there
is a solution \(\psi^{(0)}\), such that
\begin{equation}
\frac{\partial\mathcal{L}}{\partial\psi_n} = 0 \quad \text{at} \quad
\psi_n = \psi_n^{(0)}.
\end{equation}
Apparently, \(\partial \mathcal{L} / \partial g_{\mu\nu} = 0\) is then
trivially fulfilled, imposing Eq.~\eqref{eq:adjust}.

However, the solution \(\psi^{(0)}\) does not exist (``\emph{without
  fine-tuning \(\mathcal{L}\)}''~\cite{Weinberg:1988cp}). As a proof, we
decompose the set of \(N\) fields \(\psi_n\) by \(N-1\) fields
\(\sigma_a\) that do not have to be scalars and one scalar \(\phi\). For
a particular choice of \(f_n(\psi)\), we find out of Eq.~\eqref{eq:symc}
\begin{equation}
\delta g_{\lambda\nu} = 2 \varepsilon g_{\lambda\nu} \;, \quad
\delta \sigma_a = 0 \;, \quad \delta \phi = -\varepsilon \;.
\end{equation}
Under these symmetry transformations, the Lagrangian can only depend on
\(g_{\lambda\nu}\) and \(\phi\) in the combination \(e^{2\phi}
g_{\lambda\nu}\). The Lagrangian satisfying \(\partial\mathcal{L}
/ \partial \sigma_a = 0\) thus takes the form
\begin{equation}
\mathcal{L} = e^{4\phi}\, \sqrt{\det(g)} \, \mathcal{L}_0 (\sigma) \;.
\end{equation}
The source of the field \(\phi\) is then found to be the trace of the
energy-momentum tensor \(T_{\mu\nu}\)
\begin{equation} \label{eq:phisource}
\frac{\partial \mathcal{L}}{\partial \phi} = {T^\mu}_\mu \sqrt{\det(g)}
\;, \quad \text{where} \quad
T^{\mu\nu} = g^{\mu\nu} \, e^{4\phi} \, \mathcal{L}_0 (\sigma) \;.
\end{equation}
We now can redefine the metric as \(\mathcal{L}\) depends only on
\(\phi\) and \(g_{\mu\nu}\) in the combination \(\hat g_{\mu\nu} \equiv
e^{2\phi} \mathcal{L}_0\) and individual derivatives. The field \(\phi\)
then only appears with derivative couplings and cannot take the role of
a dynamical field in the adjustment mechanism (especially its
contribution in the Lagrangian is then always zero for a constant
\(\phi\) to preserve translational invariance).

This is the easiest approach to the no-go theorem provided by
Weinberg~\cite{Weinberg:1988cp}. However, one has to be aware of the
``technical'' assumptions: all fields are taken constant in the flat
space solutions although they may only preserve some combination of
translational and gauge invariance. Another assumption is the
decomposition of the \(\psi_n\) into the \(\sigma_a\) and \(phi\) for
which it is not \emph{a priori} clear that it works in the full field
space.

\paragraph{Conformal Anomalies}
Another approach (``\emph{one example of many failed
  attempts}''~\cite{Weinberg:1988cp}) was given by Peccei, Sol\`a and
Wetterich~\cite{Peccei:1987mm}, where they break the corresponding
symmetry \eqref{eq:symc} by conformal anomalies. There is an effective
Lagrangian density\footnote{Weinberg traces back the appearance and
  disappearance of equivalences to this expressions in the preprint and
  published version of~\cite{Peccei:1987mm} and a paper by Ellis, Tsamis
  and Voloshin in the same year.} including the conformal anomaly
\({\Theta^\mu}_\mu\)
\begin{equation}
  \mathcal{L}_\text{eff} = \sqrt{\det(g)} \left[
    e^{4\phi} \mathcal{L}_0(\sigma) + \phi\, {\Theta^\mu}_\mu \right] \;.
\end{equation}
Now, Eq.~\eqref{eq:phisource} gets modified by the anomalous term
\begin{equation}
  \frac{\partial \mathcal{L}}{\partial \phi} = \left(
    {T^\mu}_\mu + {\Theta^\mu}_\nu \right) \sqrt{\det(g)},
\end{equation}
with \(T^{\mu\nu}\) unchanged. The equilibrium solution for the
field \(\phi\) at the constant value \(\phi_0\) is found to be
determined by the equation
\begin{equation}
4 e^{4\phi_0} \mathcal{L}_0 + {\Theta^\mu}_\nu = 0 \;.
\end{equation}
This again cannot provide a flat and constant metric which would be
determined in contrast by the equation
\begin{equation}
  0 = \frac{\partial\mathcal{L}_\text{eff}}{\partial g_{\mu\nu}} \propto
  e^{4\phi} \mathcal{L}_0 + \phi {\Theta^\mu}_\nu \;.
\end{equation}

\paragraph{Changing Gravity}
We have seen that is impossible to define a proper adjustment mechanism
where there is an equilibrium solution of all involved fields
(``matter'' fields as well as the metric field). A modification of the
laws of gravity without changing the observable phenomenology may allow
to calculate the CC as constant of integration and thus unrelated to the
fundamental parameters. The most promising approach maintains general
covariance but the determinant of the metric is not a dynamical field
anymore. Let us consider the action for matter and gravity
\begin{equation}
I[\psi, g] = \frac{-1}{16\pi G} \int d^4x \sqrt{g} R + I_M[\psi,g],
\end{equation}
where \(I_M\) represents the matter action for the generic matter fields
\(\psi\), including a possible cosmological term \(-\Lambda \int d^4x
\sqrt{g} / 8\pi G\) since \(\Lambda\) can be treated as the vacuum energy
caused by the fields \(\psi\).

The variation gives the set of Einstein equations
\begin{equation}
  \frac{\delta I}{\delta g_{\mu\nu}} = \frac{1}{8\pi G} \left(
    R^{\mu\nu} - \frac{1}{2} g^{\mu\nu} R \right) + T^{\mu\nu},
\end{equation}
where \(T^{\mu\nu} = \delta I_M / \delta g_{\mu\nu}\). These equations
hold for all \(\mu\) and \(\nu\). Although keeping the general covariant
formalism, one can treat parts of the metric not as dynamical
fields.\footnote{``\emph{For instance, we all learn in childhood how to
    write the equations of Newtonian mechanics in general curvilinear
    spatial coordinate systems, without supposing that the 3-metric has
    to obey any field equations at all.}''~\cite{Weinberg:1988cp}}
In a kind of minimal approach, we consider the determinant \(g\) of the
metric not as such a dynamical field, so the variations keep the
determinant fixed, \(g^{\mu\nu} \delta g_{\mu\nu} = 0\). Only the
\emph{traceless} part then determines the field equations
\begin{equation} \label{eq:traceless}
  R^{\mu\nu} - \frac{1}{4} g^{\mu\nu} R = -8\pi G \left(T^{\mu\nu} -
    \frac{1}{4} g^{\mu\nu} {T^\lambda}_\lambda \right) \;,
\end{equation}
which are the traceless part of the usual Einstein equations. The
conservation laws still hold, so energy-momentum conservation
\({T^{\mu\nu}}_{;\mu} = 0\), as well as the Bianchi identities
\[\left( R^{\mu\nu} - \frac{1}{2} g^{\mu\nu} R \right)_{;\mu} = 0 \;. \]
The covariant derivative with respect to the coordinate \(x^\mu\) then
gives
\begin{equation}
  \frac{1}{4} \partial_\mu R = 8\pi G \frac{1}{4} \partial_\mu
  {T^\lambda}_\lambda \;,
\end{equation}
and thus \(R - 8\pi G {T^\lambda}_\lambda = -4\tilde\Lambda = \text{const}\);
finally
\begin{equation}
R^{\mu\nu} - \frac{1}{2} g^{\mu\nu} R - \tilde\Lambda g^{\mu\nu} = -8\pi G
T^{\mu\nu},
\end{equation}
resembling the Einstein field equation with a CC that is not related to
a corresponding term in the action! There is actually no CC but merely a
constant of integration and thus no peculiar cancellation between vacuum
fluctuations and the CC is needed. The fluctuations automatically
cancel in Eq.~\eqref{eq:traceless} and indeed there are flat-space
solutions in the absence of matter and radiation. ``\emph{The remaining
  problem is: why should we choose the flat-space
  solutions?}''~\cite{Weinberg:1988cp}\footnote{A very good question
  that rather has to be put at the very beginning of the whole
  discussion.}

This collection of failures to get rid of the CC shows that there is no
feasible way to deal with the CC problem. The problem can be either
phrased why the observed CC is much much smaller than the expected one
or how to preserve radiative stability of this quantity that seems not
to be protected by a symmetry. At the end, the CC problem shows up as a
fine-tuning problem. The no-go theorem stated by Weinberg in this review
on the CC~\cite{Weinberg:1988cp} deals with a very peculiar assumption,
namely the request for a translational invariant theory that results in a
flat and constant space-time metric. The question is, however, why
should we rely on this assumption, especially since we know about the
cosmological expansion which appears to be even accelerated. Such
constant static solutions do not coincide with current observations of
the universe.

Besides those rather irregular attempts to deal with the problem, we
shortly motivate two more promising approaches in the following: a
symmetry argument to keep the CC small (Supersymmetry) and a
probabilistic argument which sets us in one of the most probable states
of the universe with vanishing CC (Quantum Gravity). Furthermore,
Weinberg devotes a whole section in his review on anthropic
considerations which will not be covered here.


\section{Solution: Supersymmetry (SUSY)}
It is known at least since Zumino~\cite{Zumino:1974bg} that
supersymmetric field theories have (as long as SUSY is unbroken) a
vanishing vacuum energy. The argument is very simple, based on the SUSY
algebra: the SUSY generators \(Q_\alpha\), fermionic operators, obey the
anticommutation relations
\begin{equation} \label{eq:SUSY}
\{ Q_\alpha, Q_\beta^\dag \} = (\sigma_\mu)_{\alpha\beta} \, P^\mu \,
\end{equation}
with the Pauli matrices \(\sigma_{1,2,3}\) and \(\sigma_0 =
\mathbbm{1}\); \(P^\mu\) being the 4-momentum operator and \(\alpha,
\beta = 1,2\) spinor indices. Unbroken SUSY is characterised by
\begin{equation} \label{eq:vac}
Q_\alpha \, |0\rangle = Q_\alpha^\dag \, |0\rangle = 0 \;,
\end{equation}
where the ground state \(|0\rangle\) is both annihilated by the creation
and annihilation operators.

Combining Eqs.~\eqref{eq:SUSY} and \eqref{eq:vac}, it is easy to find
that the vacuum has zero energy and momentum:
\begin{equation}
\langle 0 | \, P^\mu \, | 0 \rangle = 0 \;.
\end{equation}
The scalar potential \(V(\phi, \phi^*)\) is given by the superpotential
\(\mathcal{W}(\phi)\) in terms of its derivatives:
\begin{equation}
  V(\phi, \phi^*) = \sum_i \left| \frac{\partial
      \mathcal{W}(\phi)}{\partial\phi^i} \right|^2 \;.
\end{equation}
The condition for unbroken SUSY, that \(\mathcal{W}\) is stationary in
\(\phi\), apparently yields
\begin{equation}
  \langle \rho \rangle = V_\text{min} = 0 \;.
\end{equation}
Quantum effects obviously have no effect on this result since fermionic
and bosonic loops cancel. Unfortunately, SUSY is broken in the real
world. The story gets even more intricate once gravity is taken into
account. Any global SUSY including gravity is a locally supersymmetric
supergravity, where the CC is given by the expectation value of the
scalar potential, which in turn is determined by the superpotential and
the K\"ahler potential \(\mathcal{K}(\phi, \phi^*)\).

One realisation of broken SUSY (\(\mathcal{D}_i \mathcal{W} \neq 0\))
and \(V=0\) can be found with a K\"ahler potential of the type
\begin{equation}
\mathcal{K} = -3 \ln \left| T + T^* - h(C^a, {C^a}^*) \right| / (8\pi G)
+ \tilde{\mathcal{K}} (S^n, {S^n}^*)
\end{equation}
and a superpotential \(\mathcal{W} = \mathcal{W}_1 (C^a) + \mathcal{W}_2
(S^n)\), where \(T\), \(C^a\) and \(S^n\) are chiral superfields.


\section{Solution: Quantum Gravity and Quantum Cosmology}
The proposed ``solution'' of the CC problem (i.\,e.~\emph{Why is it
  small?}) dealing with a quantum universe relies on the fact that the
cosmological term can arise as constant of integration in a modification
of gravity. In that way, the observed CC appears as a superposition of
all possible choices and either anthropic or probabilistic (or both)
arguments take over. The main idea behind the quantum cosmological
approach is to provide a theory whose probability density peaks at
\(\Lambda_\text{eff} = 0\) as proposed by Hawking~\cite{Hawking:1984hk}.

Conceptually, one has to deal with the wave function of the universe
satisfying the Wheeler--DeWitt equation in three dimensions (on the
spacelike surface)
\begin{equation} \label{eq:WheelerDeWitt}
\left[ \frac{1}{2\sqrt{h}} \frac{\delta}{\delta h_{ij}} \sqrt{h}
  \mathcal{G}_{ij,kl} \frac{\delta}{\delta h_{kl}} - ^{(3)} R - 2\Lambda
  + 8\pi G T_{00} \right] \Psi [ h, \phi ] = 0 \;,
\end{equation}
with matter fields \(\phi\), the 3-metric \(h_{ij}\) and
\(\mathcal{G}_{ij,kl} \equiv h_{ik} h_{jl} + h_{il} h_{jk} - h_{ij}
h_{kl}\).

The solution of Eq.~\eqref{eq:WheelerDeWitt} can be expressed as
``Euclidean path integral''
\begin{equation}
  \Psi \propto \int \mathcal{D}g \; \mathcal{D}\Phi \;
  \exp \left( -S[g, \Phi] \right) \;,
\end{equation}
where the 3-metric \(h_{ij}\) and the matter fields \(\phi\) appear as
boundary of the usual 4-metric \(g_{\mu\nu}\) and fields \(\Phi\) on the
3-manifold \(M_3[h, \phi]\). The Euclidean action \(S\) is given by
\begin{equation}
  S = \frac{1}{16\pi G} \int_{M_4} \sqrt{g} (R + 2\Lambda)
  \; + \; \text{matter terms} \; + \; \text{surface terms}\;.
\end{equation}

The argument now is the following: Eq.~\eqref{eq:WheelerDeWitt} is a
differential equation in an infinite-dimensional space; it thus has
infinitely many solutions that are determined via the boundary
conditions. However, the main result shall not crucially depend on those
initial conditions.

There are some technical problems adherent to the formulation above
which we are not going to discuss here for brevity. Weinberg's
interpretation~\cite{Weinberg:1988cp} now takes \(\left| \Psi[h, \phi]
\right|^2\) as probability density. Furthermore, the CC is treated as a
dynamical variable (a field) by taking the constant of integration \(c =
c(x)\). The probability distribution for this scalar field at any point
\(c = x_1\) in presence of 3-form gauge field \(A_{\mu\nu\lambda}\)
tracking its origin\footnote{The exterior derivative of
  \(A_{\nu\rho\sigma}\) is given by the totally antisymmetric
  combination \(F_{\mu\nu\rho\sigma} = \partial_{[\mu}
  A_{\nu\rho\sigma}\) which can be written as \(F^{\mu\nu\rho\sigma} = c
  \, \varepsilon^{\mu\nu\rho\sigma} / \sqrt{g}\) with \(g \equiv -
  \det(g_{\mu\nu})\) and \(\varepsilon^{\mu\nu\rho\sigma}\) the
  Levi-Civit\`a tensor with \(\varepsilon^{0123} \equiv 1\).}
\begin{equation}
P(c) = \langle \delta \left( c(x_1) - c \right) \rangle \propto
\int \mathcal{D} A \; \mathcal{D} g \; \mathcal{D} \Phi \;
\delta \left( c(x) - c \right) \exp( -S[ A, g, \Phi ] ) \;;
\end{equation}
at the stationary point
\begin{equation}
P(c) \propto \exp(-\Gamma[ A_c, g_c, \Phi_c]) \;,
\end{equation}
with the field values \(A_c\), \(g_c\) and \(\Phi_c\) that leave
\(c(x_1) = c\) fixed at a point where the total action \(\Gamma\) is
stationary. Setting all other fields to their \(A\)- and \(g\)-dependent
stationary values results in an effective action relevant to large
4-manifolds
\begin{equation}
  \Gamma_\text{eff} [A, g] = \frac{\Lambda}{8\pi G} \int \sqrt{g} \; d^4x
  + \frac{1}{16\pi G} \int \sqrt{g} R \; d^4x
  + \frac{1}{48} \int d^4x \sqrt{g} F_{\mu\nu\lambda\rho}
  F^{\mu\nu\lambda\rho} + \ldots \;,
\end{equation}
where there are all terms with more than two derivatives of \(g\) and/or
\(A\) omitted. The stationary condition for \(A_{\mu\nu\lambda}\)
requires \(F_{\mu\nu\lambda\rho}\) to have vanishing covariant
divergence and \(c\) being constant, which gives
\begin{equation}
  \Gamma_\text{eff} = \frac{\Lambda(c)}{8\pi G} \int \sqrt{g} \; d^4x
  + \frac{1}{16\pi G} \int \sqrt{g} R \; d^4x + \ldots \;,
\end{equation}
with \(\Lambda(c) = \tfrac {c^2}{2} + \Lambda\). The stationary solution
satisfies Einstein's field equations for \(g_{\mu\nu}\) with a CC
\(\Lambda(c)\), thus \(R = -4\Lambda(c)\) and
\begin{equation}
  \Gamma_\text{eff} = -\frac{\Lambda(c)}{8\pi G} \int \sqrt{g} \; d^4x \;.
\end{equation}
The solution describes a 4-sphere for \(\Lambda(c)>0\) with proper
circumference \(2\pi r\) with \(r = \sqrt{3/\Lambda(c)}\) and the
probability density \(\propto \exp(-\Gamma_\text{eff}) = \exp[3\pi/G
\Lambda(c)]\). For \(\Lambda(c)<0\), in contrast, solutions can be made
compact with periodicity conditions; in any case they have
\(\Gamma_\text{eff} \geq 0\). The conclusion drawn by Hawking is thus
that the probability density peaks towards infinity at \(\Lambda(c) \to
0+\), which means
\begin{equation} \label{eq:result}
P(c) = \delta (c-c_0)\;,
\end{equation}
where \(c_0\) is the value for which \(\Lambda(c = c_0) = 0\). The
quantity \(\Lambda(c)\) is supposed to be the ``\emph{true effective
  cosmological constant}'' \(\Lambda_\text{eff}\), covering all quantum
fluctuations.

This closes the discussion on a quantum approach to the CC problem in
this notes: ``\emph{Hence the result \eqref{eq:result}, if valid, really
  does solve the cosmological constant
  problem.}''~\cite{Weinberg:1988cp} It might be discussed whether or
not this is a valid approach, since the derivation outlined above
depends on many assumptions which hardly can be proven. On the other
hand, the quantum cosmology described in an effective way in Weinberg's
review~\cite{Weinberg:1988cp} suggests that the infinite peak of any
probability density for \(\Lambda = 0\) arises very naturally.

However, after all, we stay far away from a true solution of the CC
problem. There are still open and unanswered questions besides the fact
that there are neglected terms in the effective action which might be of
relevance:
\begin{enumerate}
\item Does Euclidean quantum cosmology have anything to do with the real
  world?\footnote{The arguments crucially depend on a Euclideanised
    action.}
\item What are the boundary conditions?\footnote{They are not really
    fixed and may be affected by any perturbation.}
\item Are wormholes real?\footnote{The interpretation of quantum
    cosmology, which is not discussed here, relies on creation and
    annihilation of baby universes that serve as wormholes.}
\end{enumerate}

\subsection{Conclusion}
Weinberg's approach is to show, that for a generic set of parameters including a cosmological constant term, there no \emph{local field theory} that has a flat Minkowski space-time as solution. Classical gravity explicitly fails. Of course, supersymmetry cures the problem because of the well-known property of a vanishing vacuum energy in the unbroken phase. In Quantum Gravity, it appears naturally that the probabililty distribution favors a zero \(\Lambda\). The question of a static and flat space-time, however, stays uncovered. In view of an expanding universe, it is nevertheless questionable whether a static Minkowski universe should be preferred.
