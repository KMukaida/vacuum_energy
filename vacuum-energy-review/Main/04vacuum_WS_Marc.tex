%!TEX root = ../VacuumEnergy.tex

\chapter{Self-tuning from extra dimensions}
\label{cha:selftuningED}

In this section we present two different mechanisms that make use of an extra dimension to build a setup where the four dimensional curvature is sent to the extra dimension, leaving a flat four dimensional brane and a warped bulk. The first model was presented in Ref. \cite{ArkaniHamed:2002fu}, showing that a scalar field conformally coupled to the brane offers a technically natural solution to the CC problem. The second model was presented in Ref. \cite{Csaki:2000dm}, that shows how adding a black hole geometry in the extra dimension opens up the parameter space.
\medskip

\section{Extra dimensional model}
\smallskip
Consider a 5-d spacetime, with coordinates \mbox{$x^M=(x^\mu,y)$} with the $y\to -y$ identification, and a 3-brane at $y=0$ to which the SM is confined (see Refs. \cite{Polchinski:1996na,Lukas:1998yy,Rubakov:2001kp} for a general discussion on this type of scenarios). The action of the model is
\begin{equation}
S\,=\, \int d^5x \sqrt{g_5} \left( \frac{R}{2\kappa_5^2}-\frac{3}{2}(\nabla \phi)^2 - \delta(y-0) \mathcal{L}_{SM}(\psi,g_{\mu\nu}e^{\kappa_5 \phi(y)})\right).
\label{eq:STfXD.canonicalaction}
\end{equation}
The constant $\kappa_5$ is related to the 5D Planck scale $M_\star$ as $\kappa_5^2=1/M_\star^3$. At the brane $y=0$, the induced metric is given by $g_{\mu\nu}e^{\kappa_5 \phi(y)}$, so the scalar $\phi$ is conformally coupled to the SM fields, represented generically with $\psi(x)$. The choice for the conformal coupling, given by the normalization of the kinetic term, is unaffected by quantum loops on the brane. This can be seen by writing the action \ref{eq:STfXD.canonicalaction} in the string frame, which can be obtained by using the Weyl transformation $\bar{g}_{MN}=g_{MN}e^{\kappa_5\phi}$, so one gets
\begin{equation}
S\,=\, \int d^5x \sqrt{\bar{g}_5} e^{-3\kappa_5\phi/2}\frac{\bar{R}}{2\kappa_5^2}-\int d^4x \sqrt{\bar{g}_4(0)}\mathcal{L}_{SM}(\psi,\bar{g}_{\mu\nu}(0)).
\end{equation}
In this frame it is explicit that the brane action does not contain $\phi$ so the choice for the conformal coupling is stable under radiative corrections on the brane.
\medskip

We can now obtain the equations of motion for the fields $g_{\mu\nu}$, $\phi$ and $\psi$. For $\psi$, we can work in terms of the 1PI effective action $\Gamma_{eff}^{SM}(\psi,g_{\mu\nu}e^{\kappa_5 \phi(y)})$, that formally contains the quantum effects. Since we seek for solutions with 4D Poincar\'{e} symmetry, the effective action is given by the effective potential, and the SM equations of motion take the form
\begin{equation}
\frac{\delta }{\delta \psi}\Gamma_{eff}^{SM} \,=\, \frac{\delta }{\delta \psi}\left(  -\int d^4x \sqrt{g_4}V_{eff}(\psi)e^{2\kappa_5\phi} \right)\,=\,0 \hspace{.2cm} \rightarrow\hspace{.2cm} \frac{\partial}{\partial \psi}V_{eff} \,=\,0.
\end{equation} 
The extrema of this potential will be denoted by $V_{ext}$. We will show now how the field $\phi$ sends any brane curvature to the bulk such that the Poincar\'{e} solution is preserved for any value of $V_{ext}$. Let us assume this Poincar\'{e} solution by writing the 5d metric in the form
\begin{equation}
ds^2\,=\, a^2(y)\eta_{\mu\nu}dx^\mu dx^\nu + dy^2
\label{eq:STfXD.poincaremetrixansatz}
\end{equation}
with $a(y)$ the warp factor, and define the shifted scalar field 
\begin{equation}
\tilde{\phi}\,=\, \phi + \frac{1}{2\kappa_5}\ln\left(\frac{V_{ext}}{M_\star^4}\right).
\end{equation}
Then, the bulk field equations are given by
\begin{equation}
\frac{a^{\prime 2}}{a^2}=\frac{\kappa_5^2\tilde{\phi}^{\prime 2}}{4}\,,\hspace{.8cm}
\tilde{\phi}^{\prime \prime} + 4\frac{a^\prime}{a}\tilde{\phi}^{\prime}=0\,,\hspace{.8cm}
\frac{a^{\prime \prime}}{a}=-\frac{3\kappa_5^2 \tilde{\phi}^{\prime 2} }{4}
\label{eq:STfXD.bulkEOM}
\end{equation} 
with the matching conditions
\begin{equation}
a^\prime (0)=-\frac{M_\star}{6}e^{2\kappa_5\tilde{\phi}(0)}a(0)\,,\hspace{1.6cm} \tilde{\phi}^{\prime}(0)=\frac{M_\star^{5/2}}{3}e^{2\kappa_5\tilde{\phi}(0)}.
\label{eq:STfXD.matchingconditions}
\end{equation}
Notice that with the definition of $\tilde{\phi}$, the field equations are independent of the extremum of the SM potential $V_{ext}$. The solution to the Eqns. \ref{eq:STfXD.bulkEOM} and \ref{eq:STfXD.matchingconditions} is given by
\begin{eqnarray}\nonumber
ds^2 &=& \sqrt{1-\frac{2M_\star}{3}e^{2\kappa_5\tilde{\phi}_0}|y|}\eta_{\mu\nu}dx^\mu dx^\nu + dy^2\\
\tilde{\phi} &=& \tilde{\phi}_0 - \frac{1}{2\kappa_5}\ln\left(  1-\frac{2M_\star}{3} e^{2\kappa_5\tilde{\phi}_0}|y| \right)
\hspace{.2cm} \rightarrow\hspace{.2cm}
\phi = \phi_0 - \frac{1}{2\kappa_5}\ln\left(  1-\frac{2 V_{ext}}{3M_\star^3} e^{2\kappa_5\phi_0}|y| \right)
\label{eq:STfXD.solutions}
\end{eqnarray}
with $\phi_0$ an integration constant. This shows that the brane at $y=0$ has Poincar\'{e} symmetry regardless the value of $V_{ext}$. Moreover, one should check that the 4d Poincar\'{e} solution is the only 4d maximally symmetric solution. If one allows for a dS and AdS solutions with curvature $h$ and a scalar coupling to the brane given by $\exp(\xi \kappa_5 \phi)$, the brane curvature is given by
\begin{equation}
h^2\,=\, a^{\prime 2}(0) - \frac{\kappa_5^2 \tilde{\phi}^{\prime 2}(0)}{4} a^2(0)
\end{equation}
which vanishes for the choice in Eq. \ref{eq:STfXD.canonicalaction}, see Eq. \ref{eq:STfXD.bulkEOM}. So this coupling is the one that imposes a flat brane independently of the details of the SM physics, even after all quantum corrections are taken into account. Therefore, there is no fine tuning in the sense that a variation of the SM parameters as the Higgs vev, or the electron mass, do not break Poincar\'{e} symmetry.
\medskip

However, even if we have found a solution with Poincar\'{e} metric regardless the $V_{ext}$, we must ensure that we recover 4d gravity at long distances. Notice the important fact that there is a naked singularity in \ref{eq:STfXD.solutions} at proper distance
\begin{equation}
y_s \,=\, \frac{3}{2M_\star} e^{-2\kappa_5 \tilde{\phi}_0}\,=\, \frac{3 M_\star^3}{2V_{ext}}e^{-2\kappa_5 \phi_0}.
\end{equation}
Depending on the sign of $V_{ext}$, two types of solutions are possible, shown in Fig. \ref{fig:STfXD.singularity}. For negative $V_{ext}$, the warp factor monotonically increases and the scalar field dissipates. This means that the extra dimension is non-compact and has infinite volume, so the 4d gravity is never recovered. For positive $V_{ext}$, the warp factor goes to zero at finite distance, while the curvature and scalar field diverge.
\medskip
\begin{figure}
\includegraphics[scale=0.6]{./Figures/STfXD_singularity.pdf}
\caption{Two possible behaviours of the field $\phi(y)$ and the scale factor $a(y)$, for positive and negative $V_{ext}$.}
\label{fig:STfXD.singularity}
\end{figure}

There is an analogy to interpret this behaviour, based on \cite{deBoer:1999tgo,Verlinde:1999xm}, relying on the fact that the metric \ref{eq:STfXD.poincaremetrixansatz} can be understood as a Wick rotation of the FRW metrix, with $y$ playing the role of cosmological time and $a$ being the scale factor. Then, the $V_{ext}<0$ solution represents an eternally inflating universe while the  $V_{ext}>0$ solution is a universe with a \textit{Big Crunch} at $y=y_s$.
\medskip

Four dimensional gravity can only be recovered if space ends at the singularity, meaning that the extra dimension is compact. For instance, it might happen that near $y_s$ the geometry interpolates to an AdS space, sending the singularity at infinite proper distance keeping a finite volume {RS}. However, there is the possibility that the singularity is smoothed out such that at $y>y_s$ the theory can be described by Einstein gravity again and 4d gravity is never recovered. It is therefore crucial to understand the microscopic properties of quantum gravity to recover long distance, classical, gravity.
\medskip

\section{Self-tuning with black holes}

We have seen that we can couple a scalar mode to the brane to transfer the curvature to the bulk by adjusting the coupling. We will show how one can do the same employing a black hole bulk geometry. Pure gravity can offer this mechanism, but a flat brane requires a tuning of the parameters of the theory. However, a gauge field in the bulk gives freedom to find solutions without tuning.
\medskip

Consider a 5d theory with a cosmological constant $\Lambda_{bk}$ and a $U(1)$ gauge field propagating in the bulk, while the SM fields are confined in the 4d brane. The action is given by
\begin{equation}
S\,=\, \int d^5x \sqrt{g_5}\left(  \frac{1}{2\kappa_5^2}R-\frac{1}{4}F_{MN}F^{MN} -  \Lambda_{bk} \right)\,+\,\int d^4x \sqrt{g_4} \mathcal{L}_{SM}(\psi,g_{\mu\nu}).
\end{equation}
The matter fields in the brane can be described as a perfect fluid of energy density $\rho$ and pressure $p$. The brane term includes the brane tension $\rho=-p$. Consider the metric ansatz
\begin{equation}
ds^2\,=\, -h(r) dt^2 +h^{-1}(r) dr^2 + \frac{r^2}{\ell^2}d\Sigma_k^2
\end{equation}
where 
\begin{equation}
h(r) \,=\, k + \frac{r^2}{\ell^2}-\frac{\mu^2}{r^2}\,,\hspace{1.4cm} 
\frac{1}{\ell^2}\,=\, -\frac{1}{6}\kappa_5^2 \Lambda_{bk}
\end{equation}
and
\begin{equation}
d\Sigma_k^2 = \frac{1}{1-k\sigma^2/\ell^2}d\sigma^2+\sigma^2d\Omega_2^2
\end{equation}
is the metric of the spatial 3-sections with curvature parameter $k$. The bulk geometry is that of an AdS-Schwarzchild black hole (a \textit{black wall} for $k$=0) located at $r=0$ with mass $\mu$. For $\mu>0$ the $r=0$ singularity is hidden behind a horizon located at $r=r_h>0$, with $h(r_h)=0$.
\medskip

When considering the gauge field, we obtain an AdS-Reissner-Nordstr\"{o}m metric \cite{Barcelo:2000re}, and the function $h$ depends on the charge $Q$ of the black hole as
\begin{equation}
h(r)\,=\, k + \frac{r^2}{\ell^2}-\frac{\mu^2}{r^2} + \frac{Q^2}{r^4}.
\end{equation}
For $k=0$, the existence of an inner and outer horizons implies
\begin{equation}
Q^4 \,<\, \frac{4}{27}\mu^3\ell^2.
\label{STfXD.innerouterhorizoncondition}
\end{equation}
One can \textit{build} a brane at $r=r_0$ by gluing two solutions of the field equations, by the procedure explained in \cite{Csaki:2000dm} where the details can be found. The jump equations for a static brane with a vanishing 3d curvature ($k=0$) in presence of a AdS-Schwarzschild black hole are
\begin{equation}
36\left(  \frac{r_0^2}{\ell^2}-\frac{\mu}{r_0^2}  \right)\,=\, \kappa_5^4\rho^2r_0^2\,,\hspace{2cm} 
36\left(  \frac{r_0^2}{\ell^2}+\frac{\mu}{r_0^2}  \right)\,=\, -\kappa_5^4(2+3\omega)\rho^2r_0^2.
\label{STfXD.jumpequations}
\end{equation}
By combining them one gets an expression for the black hole mass
\begin{equation}
\mu\,=\, -\frac{1}{24}\kappa_5^4(1+\omega)\rho^2r_0^4
\end{equation}
If $\mu<0$, there is a naked singularity at $r=0$, while for $\mu>0$ the singularity is behind an horizon. This condition translates into $\omega < -1$, which violates the energy condition on the brane. However, this can be obtained if in addition to a positive brane tension there is a small negative matter energy density which is not accompanied by pressure, so $\omega=0$ for this component. However, even for $\omega <-1$ the solution is fine tuned, since the jump equations Eq. \ref{STfXD.jumpequations} lead to
\begin{equation}
\frac{\mu}{r_0^4}\,=\, \frac{1}{\ell^2} - \frac{\kappa_5^4\rho^2}{36}\,\hspace{.5cm}
\rightarrow \hspace{.5cm} \rho\,=\, \sqrt{\frac{-72}{1+3\omega}}\frac{1}{\ell\kappa_5^2}
\label{STfXD.rhofornocharge}
\end{equation}
which is a tuning of the energy density needed on the brane. Therefore, in this scenario the fine tuning of the cosmological constant is not avoided. However, the situation changes if the $U(1)$ field is taken into account. If the SM matter is not charged, there are no additional jump conditions and Eqs. \ref{STfXD.jumpequations} are modified to
\begin{equation}
36\left(  \frac{r_0^2}{\ell^2}-\frac{\mu}{r_0^2} +\frac{Q^2}{r_0^4}  \right)\,=\, \kappa_5^4\rho^2r_0^2\,,\hspace{1cm} 
36\left(  \frac{r_0^2}{\ell^2}+\frac{\mu}{r_0^2}-\frac{Q^2}{r_0^4}  \right)\,=\, -\kappa_5^4(2+3\omega)\rho^2r_0^2.
\label{STfXD.jumpequationsQ}
\end{equation}
The introduction of the charge gives an extra freedom to the system, since now the jump equations only give the mass $\mu$ and the charge $Q$ in terms of the brane parameters
\begin{equation}
\mu \,=\, 3\left(  \frac{1}{\ell^2} + \frac{1}{36}\kappa_5^4\omega \rho^2  \right)r_0^4
\hspace{1cm} Q^2\,=\, 2\left(  \frac{1}{\ell^2} + \frac{1}{72}\kappa_5^4(1+3\omega) \rho^2  \right)r_0^6.
\label{STfXD.massandchargeblackhole}
\end{equation}
There are two requirements for the parameters to ensure the self consistency of the solutions. First, the positivity of $Q^2$ in \ref{STfXD.massandchargeblackhole} only requires that 
\begin{equation}
\rho \leq \rho_0 = \sqrt{\frac{-72}{1+3\omega}}\frac{1}{\ell\kappa_5^2},
\label{STfXD.criticalenergydensityBHQ}
\end{equation}
which has to be compared with the condition \ref{STfXD.rhofornocharge}. This is not the only condition, since we must ensure that the black hole singularity is hidden behind a horizon, which requires to fulfill the relation in \ref{STfXD.innerouterhorizoncondition}, which in terms of the brane parameters reads
\begin{equation}
\frac{1}{\ell^2}\left(  \frac{1}{\ell^2}- \frac{1}{36}\kappa_5^4\rho^2 + \frac{1}{24}\kappa_5^4(1+\omega) \rho^2 \right)^2\,<\, 
\left(  \frac{1}{\ell^2}- \frac{1}{36}\kappa_5^4\rho^2 + \frac{1}{36}\kappa_5^4(1+\omega) \rho^2 \right)^3.
\end{equation}
Notice that for $\omega=-1$ this condition cannot be satisfied. For $\omega\neq -1$, The inequality can be written as a quadratic inequality for the brane energy density $\rho$, which has roots $\rho_\pm$ given by
\begin{equation}
\rho_\pm \,=\, \frac{6}{\ell \kappa_5^2}\sqrt{ \frac{1}{8\omega^3}\left(  1+6\omega-3\omega^2\mp \sqrt{(1+\omega)^3(1+9\omega)}  \right) }.
\end{equation}
The roots are both positive for $\omega<-1$, both negative in the interval $-1/9\leq \omega\leq 0$, and there is only one positive root for positive $\omega \geq 0$. Only positive roots correspond to a physical value of $\rho^2$, so in the region $-1/9\leq \omega\leq 0$ the singularities cannot be hidden. There is the extra constraint in Eq. \ref{STfXD.criticalenergydensityBHQ}, which further constrains  the allowed region since it can be shown that $\rho_-<\rho_0<\rho_+$. So in those regions, shown in Fig. \ref{fig:STfXD.phasediagbrane}, the singularity is hidden behind horizons. However, one last constraint is imposed by the fact that the horizons sit between the singularity at $r=0$ and the brane at $r=r_0$, which translates to $\omega <0$. The allowed region is 
\begin{equation}
\omega < -1 \text{ and } \rho_-<\rho<\rho_0.
\end{equation}
\begin{figure}
\centering
\includegraphics[scale=0.6]{./Figures/STfXD_phasediagbrane.pdf}
\caption{Allowed regions for $\omega$ and $\rho$ which give a solution where the $r=0$ singularity is hidden behind horizons. The regions for $\omega>0$ are cut off by requiring that the horizons sit between the $r=0$ and the brane position at $r=r_0$.}
\label{fig:STfXD.phasediagbrane}
\end{figure}
The allowed region is shown if Fig. \ref{fig:STfXD.phasediagbrane}.
\medskip

\subsection*{Phenomenological consequences}

The model just presented features the possibility that gravitational waves might propagate faster than the electromagnetic waves which are stuck in the brane. We present here a summary of the discussion in REF, valid for spacetimes with black holes in the bulk but that might not necessarily solve the cosmological constant problem.
\medskip

The difference in the speed of gravity and light propagation can be naively understood from an optical analogy. The local speed of light $c$ is a function of the extra dimension $c(r)$. By making the analogy of $c(r)$ with a refraction index $n(r)$, if $c(r)$ increases in the bulk then the shortest path for gravitational waves is to penetrate into the bulk. However, electromagnetic waves must be confined to the brane, which might lead to an apparent violation of Lorentz invariance from a brane observer point of view. This phenomenon is not exclusive to our particular model but is present in many brane-world scenarios, see for instance Refs. \cite{Kaelbermann:1998hu,Kaelbermann:1999jw,Chung:1999xg,Ishihara:2000nf,Chung:2000ji,Kraus:1999it,Youm:2000ax,Hashimoto:2000ys,Libanov:2005nv}.
\medskip

In the black hole model, the geodesics can leak into the bulk and come back at the brane at a distance $x_{ret}$ given by
\begin{equation}
x_{ret}(E/\vert \vec{p}\vert ) = \int_{r_T}^{r_0} \,dr\,\frac{2\ell^2}{r^2\sqrt{(E/\vert \vec{p}\vert)^2-h(r)\ell^2/r^2}},
\end{equation}
where $E/\vert \vec{p}\vert$
\begin{equation}
E/\vert \vec{p}\vert \,=\,\frac{ h(r)\ell^2 }{r^2}\frac{dt}{d\tau}\left\vert \frac{dx}{d\tau} \right\vert^{-1}\,=\, \left(  \frac{h(r_T)}{r_T^2}\ell^2  \right)
\end{equation}
is a conserved quantity, with $r_T$ being the \textit{turning point} of the geodesic in the coordinate $r$. It indicates how far into the bulk the gravitational wave penetrates depending on the source and the wavelength of the gravitational wave. The geodesic will return to the brane after a time
\begin{equation}
t_{ret}(E/\vert \vec{p}\vert ) = \int_{r_T}^{r_0} \,dr\,\frac{2 E/\vert \vec{p}\vert }{h(r)\sqrt{(E/\vert \vec{p}\vert)^2-h(r)\ell^2/r^2}}.
\end{equation}
This gives the average speed $c_{av}=x_{ret}/t_{ret}$ of the gravitational waves, which has to be compared with the brane speed of light, given by $c_{em} = \sqrt{h(r_0)\ell^2/r_0^2}$.

LIGO CONSTRAINS THIS A LOT? (we probably should mention)