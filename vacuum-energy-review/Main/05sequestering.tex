%!TEX root = ../VacuumEnergy.tex


\chapter{Sequestering mechanism}
\label{cha:sequestering}
\epigraph{sequester\\verb: 1. Isolate or hide away.}{Oxford Dictionary}
\section{Motivation}
Sequestering (also: The Sequester) is a new proposal \cite{Kaloper:2013zca,Kaloper:2014dqa} trying to find a way to circumvent Weinberg's No-Go theorem \cite{Weinberg:1988cp} for self-tuning mechanisms. Having classical GR coupled to quantum matter fields in mind, the idea of the new approach is based on the following observation. In the context of classical GR, the cosmological constant is a truly constant parameter over all space-time representing a modification of gravity, and was not thought to be related to a bare vacuum expectation value of locally defined quantum fields, historically speaking. Now what Weinberg's self tuning-mechanism does, is to take the modernist EFT perspective trying to add local field dynamics on top of GR in such a way that the overall cosmological constant becomes small. However, a tuning of the original truly constant parameter would be needed to be done once and for all time and space not referring to any space-time dynamics. Apart from the naive approach of setting the bare cosmological constant close to zero by hand (which does not work because of radiative instability), another option is to start thinking of GR extensions incorporating global dynamics which are capable of tuning the cosmological constant while not referring to any space-time dynamics at all. At first sight, this statement might sound obscure but this has some tradition in modifications of gravity namely the introduction of Lagrange multipliers which are not fields but nonetheless yield dynamical constraints in forms of additional equations of motion (obtained by varying with respect to the Lagrange Multiplier). For instance, in unimodular Gravity\footnote{Unimodular gravity is a restricted version of GR, fixing $\det g$ to be unity. Then the determinant has not be varied when obtaining the equation of motion which turns out to be Einstein equation, but traceless. The connection between unimodular gravity and the Sequestering is made in the corresponding chapters of A. Padilla's review on the Cosmological Constant \cite{Padilla:2015aaa}. We come back to it in section \ref{seqSecLocalVariant}}, the constraint for the metric tensor $g=\det g=-1$ can be implemented dynamically into the action by a global Lagrange multiplier $\xi$ to be identified with the cosmological constant:
\begin{equation}
S_{UMG}=\int\mathrm{d}^4 x \sqrt{-g}\left[\frac{M_{pl}^2}{2}R-\mathcal{L}_m(g^{\mu\nu},\Psi)\right]+\xi(\sqrt{-g}-1)
\end{equation}
Then the variation with respect to $\xi$ gives the unimodular constraint.\\

As it will turn out, the mechanism to be presented seems to make vacuum energy stop from gravitating at all which explains the name for the framework: Sequestering. Besides unimodular gravity, another influencing idea was already proposed in in the context of String Theory \cite{Tseytlin:1990hn}, where vacuum energy gets sequestered in a similar manner at 1-loop level. On the other hand, making use of the principle of equivalence, the sequestering proposal makes sure that all (matter!) loops get sequestered from gravity.

\section{Setting up the global sequestering mechanism}
The original Sequestering framework is presented in \cite{Kaloper:2013zca,Kaloper:2014dqa}. We will follow these papers in our discussion. Whenever it seems appropriate, we do short excourses in other directions of Sequestering literature. Another pedagogical review on the topic is \cite{Padilla:2015aaa}\\
\paragraph{The Action.} We start with the following extension to the action of General Relativity
\begin{equation}\label{seq1}
S=\int\sqrt{-g}\left[\frac{M_{pl}^2}{2}R-\Lambda-\lambda^4\mathcal{L}_m\left(\frac{g^{\mu\nu}}{\lambda^2},\Psi\right)\right]\mathrm{d }^4x+\sigma\left(\frac{\Lambda}{\lambda^4\mu^4}\right)
\end{equation}
which we consider to be semi-classical meaning classical GR coupled to quantum-matter fields $\Psi$, with $\mathcal{L}_m$ denoting the matter Lagrangian. We also focus our attention on the following subtleties. 
\begin{itemize}
\item The \emph{cosmological constant} $\Lambda$ is not just to be thought of being a parameter as in standard GR but a classical variable we can vary with respect to, obtaining an equation of motion. In this way, $\Lambda$ takes part in global dynamics. 
\item That is also true for $\lambda$ which we call the \emph{sequestering variable}. We devote to it an extra paragraph below, to explain its physical meaning. Bottom line is that it gets all vacuum energy from $\mathcal{L}_m$ sequestered. $\mathcal{L}_m$ is also called protected sector because of this.
\item The most important addition is a new unconventional global term outside the integral and thus also outside the part of the action where the space-time dynamics are set. There we write down an additional differentiable function $\sigma(z)$ determining the global dynamics of the theory by coupling the global variables $\Lambda$ and $\lambda$ with each other.
\item $\mu$ is some mass scale in order to render the argument of $\sigma$ dimensionless. 
\end{itemize}
We think of the global term as a phenomenological one, for which an underlying microscopic theory might exist, since the non-integral term in (\ref{seq1}) sparks doubt on whether this underlying theory can be quantum (we will come back to this shortcoming later).
Apart from that shortcoming, the choice of the functional form of $\sigma$ does not spoil the sequestering mechanism. For a natural choice, where $\mu\eqsim M_{pl}$, one might choose $\sigma(z)\sim \sinh z$ (For a discussion on the naturalness of the mechanism, see chapter 6 ff. in \cite{Kaloper:2014dqa}).

\paragraph{The Role of the Sequester variable $\lambda$.}
Primarily, $\lambda$ sets the hierarchy between $M_{pl}$ and physical mass $m_{ph}$:
\begin{equation}\label{seqRelationlambda}
\lambda\propto\frac{m_{ph}}{M_{pl}}
\end{equation}
from which we immediately see that $\lambda$ must not be zero in order to ensure that particle masses are not zero as well. Equation (\ref{seqRelationlambda}) holds because $\lambda$ relates the gravity scale and the matter scale in the action via the global conformal transformation
\begin{equation}\label{seq2}
\tilde{g}_{\mu\nu}=\lambda^2 g_{\mu\nu}\qquad\qquad\tilde{g}^{\mu\nu}=\frac{1}{\lambda^2}\ g^{\mu\nu}
\end{equation}
where in the Einstein (untilded) frame the gravity term looks canonical, whereas in the Jordan (tilded) frame this is true for the matter term, see figure \ref{seqJordanEinsteinFrameComparison}. 
This rescaling does not violate the equivalence principle because $\lambda$ couples to all matter defined in $\mathcal{L}_m$ universally. This includes of course vacuum energy to any loop calculation\footnote{This is only true in the limit, where gravity-loops do not come into play. Universality of the coupling to $\lambda$ is only well-off in the semi-classical limit}\\
\begin{figure}[t]
	\centering
	\fbox{
	\includegraphics[width=0.65\textwidth]{Figures/"SequesteringtikZ1"}
	}
	\caption{In Sequestering gravity is not canonically coupled to itself in the same frame as the frame where gravity is canonically coupled to matter.}
	\label{seqJordanEinsteinFrameComparison}
\end{figure}

Before we see how we can make use of $\lambda$ stopping vacuum energy from gravitating, let us quickly discuss another interpretation of the role of $\lambda$. Defining
\begin{equation}\label{seq5}
\kappa^2=\frac{M_{pl}^2}{\lambda^2}\qquad g_{\mu\nu}\frac{\kappa^2}{M_{pl}^2}\rightarrow g_{\mu\nu}\qquad\Lambda\rightarrow\Lambda\left(\frac{M_{pl}}{\kappa^2}\right)^2
\end{equation}
we switch to the Jordan frame in which the action looks like
\begin{equation}\label{seq6}
S=\int\sqrt{-g}\left[\frac{\kappa^2}{2}R-\Lambda-\mathcal{L}_m\left(\frac{g^{\mu\nu}}{\lambda^2},\Psi\right)\right]\mathrm{d }^4x+\sigma\left(\frac{\Lambda}{\mu^4}\right)
\end{equation}
Here, the matter term is canonically normalized.  We can see, what $\kappa$ (or $\lambda$ for the sake of argument) hiddenly does: Besides the Lagrange multiplier $\Lambda$ constraining the cosmological constant, it is the promotion of the Planck Mass to a second lagrange multiplier constraining the Ricci-Scalar and thus the geometry space-time. We did the transformation in a way that the global term gets independent from $\kappa$. \mbox{Equation (\ref{seq6})} will be of use when we generalize the Sequestering mechanism later in \mbox{section \ref{seqSecLocalVariant}}, but for now let us go back to the Einstein frame (\ref{seq1}) for convenience.

\paragraph{The Sequester of vacuum energy.}
We can explicate the two constraints on $\lambda$ and $\Lambda$ by writing down their equations of motion. For this, we vary equation (\ref{seq1}) with respect to $\Lambda$ and $\lambda$ respectively. We obtain
\begin{align}\label{seq7}
\frac{1}{\lambda^4\mu^4}\sigma'\left(\frac{\Lambda}{\lambda^4\mu^4}\right)&=\int\sqrt{-g}\mathrm{d}^4x\\\label{seq8}
\frac{4\Lambda}{\lambda^4\mu^4}\sigma'\left(\frac{\Lambda}{\lambda^4\mu^4}\right)&=\int\sqrt{-g}\lambda^4\tilde{T}^\alpha_{\ \alpha}\mathrm{d}^4x
\end{align}
where $\sigma'(z)=\frac{\mathrm{d}\sigma}{\mathrm{d}z}$ and $\tilde{T}_{\mu\nu}=\frac{2}{\sqrt{-\tilde{g}}}\frac{\delta}{\delta \tilde g^{\mu\nu}}\int\mathrm{d}^4x \sqrt{-\tilde{g}} \mathcal{L}_m$ is the matter energy-momentum tensor in the Jordan frame. The energy-momentum-tensor in the Einstein frame is $T^\mu_{\ \nu}=\lambda^4 \tilde T^\mu_{\ \nu}$ (mind the index structure). Now take us take a look of the gravity equation coming from $\ref{seq1}$: Variation with respect to $g_{\mu\nu}(x)$ yields
\begin{equation}\label{seq9}
M_{pl}^2G_{\mu\nu}=-\Lambda g_{\mu\nu}+T_{\mu\nu}
\end{equation}
These are Einstein's equations, which are not altered by Sequestering: Locally, gravity is exactly described by GR. Our extension to (\ref{seq1}) is minimal enough to do nothing else than introducing the additional constraints (\ref{seq7}) and (\ref{seq8}), for instance, equation (\ref{seq7}) implies that the space-time volume $\int\sqrt{-g}\mathrm{d}^4x$ must be finite because of $\lambda\neq0$.\\

The second constraint for the cosmological constant we obtain by dividing equation (\ref{seq8}) by equation (\ref{seq7}) and solving for $\Lambda$. We obtain
\begin{equation}\label{seq11}
\Lambda=\frac{1}{4}\left\langle T^\alpha_{\ \alpha} \right\rangle
\end{equation}
where $\left\langle Q\right\rangle$ denotes the space-time average for a quantity $Q$ defined as
\begin{equation}\label{seq12}
\left\langle Q\right\rangle=\frac{\int\mathrm{d}^4x\sqrt{-g}\ Q }{\int\mathrm{d}^4x\sqrt{-g}}
\end{equation}
Substituting the expression for the cosmological constant (\ref{seq11}) into Einstein's equations (\ref{seq9}) yields
\begin{equation}
M_{pl}^2G^\mu_{\ \nu}=T^\mu_{\ \nu}-\frac{1}{4}\langle T^\alpha_{\ \alpha} \rangle\delta^\mu_{\ \nu}
\end{equation}
What we can do now is to split the matter energy-momentum tensor $T_{\mu\nu}$ into a part coming from QFT vacuum energy and into a second part $\tau_{\mu\nu}$ representing local field excitations:
\begin{equation}
T_{\mu\nu}=-V_{vac}\ g_{\mu\nu}+\tau_{\mu\nu}
\end{equation}
We can stay agnostic of the loop order of our calculation, the splitting works any ways. In doing so, we see how the contribution from vacuum energy to Einstein's equation cancels out
\begin{equation}
M_{pl}^2G^\mu_{\ \nu}=\cancel{V_{vac}\ g^\mu_{\ \nu}}+\tau^\mu_{\ \nu}-\cancel{\frac{1}{4}V_{vac}\left\langle g^\alpha_{\ \alpha}\right\rangle\delta^\mu_{\ \nu}} -\frac{1}{4}\left\langle\tau^\alpha_{\ \alpha}\right\rangle\delta^\mu_{\ \nu}
\end{equation}
and we are left with Einsteins equation for local field excitations only:
\begin{equation}
\boxed{M_{pl}^2G^\mu_{\ \nu}=\tau^\mu_{\ \nu}-\frac{1}{4}\left\langle\tau^\alpha_{\ \alpha}\right\rangle\delta^\mu_{\ \nu}}
\end{equation}
This is the result we aimed for: Vacuum energy is sequestered from gravitational interaction. 
Additionaly, we are left with a residual cosmological constant
\begin{equation}\label{seqEinsteinEquation}
	\Lambda_{eff}=\frac{1}{4}\left\langle\tau^\alpha_{\ \alpha}\right\rangle
\end{equation}
which is independent from vacuum energy as well and is given by the average over all local field excitations in the overall history of the universe -- past, present and future. \\

\paragraph{Isn't the locic of the sequester circular and does not contain any new physics?} Einsteins equation is non-local because of the presence of space-time averages. In principle, it is far from clear whether solutions of equation (\ref{seqEinsteinEquation}) actually exist.\\
Questions from the audience
Thomas' question: Like UMG, there is a GR solution with the right amount of vacuum energy in the first place, so no new physics\\
Tomislav's question: This is not what vacuum energy is. Also causality violated, cannot define a propagator. ($\rightarrow$ There is a paper on it). Integrals A and B are not defined in a quantum gravity

\section{Implications for the cosmological constant}
We know experimentally that for our universe the observed cosmological constant is
\begin{equation}
\Lambda_{obs}\sim\rho_{crit}\sim\left(\text{meV}\right)^4
\end{equation}
So, an important consistency check of the framework is to show agreement to observation by showing that $\Lambda_{eff}$ does not exceed the observed value
\begin{equation}
\Lambda_{eff}\leq\Lambda_{obs}
\end{equation}
As we will discuss now, this is the case (for large enough universes, there is some weak anthropics in play).


\paragraph{A finite universe changes on how we need to think about DE.} First of all we have already seen, in Sequestering, for a non-vanishing mass gap ($\lambda\neq0$), the universe must be finite. This unfortunately means that we cannot identify Dark Energy from the $\Lambda CDM$ ($w_{DE}=-1$) with $\Lambda_{eff}$ because then the universe would never stop expanding again. The current state of acceleration must be transient. A transient acceleration phase can be implemented by a rolling scalar field \cite{Ratra:1987rm}. In this case, the acceleration parameter is given by
\begin{equation}
w=\frac{p}{\rho}=\frac{\frac{1}{2}m\dot{\phi}^2-V(\phi)}{\frac{1}{2}m\dot{\phi}^2+V(\phi)}
\end{equation}
Then, for a slowly rolling field ($\dot{\phi}\approx0$), the acceleration parameter can be $w_i\approx-1$, mimicking an accelerated universe for quiet some time. Sequesterig needs a mechanism like it leading to an observable prediction. The implications of the ending of the universe for the framework have been worked out \cite{Kaloper:2014fca}.\\ 
\begin{figure}[t]
	\fbox{
	\begin{minipage}[t]{0.5\textwidth}
		\centering
		Friedmann-eq.
		\begin{equation*}
			H^2=\left(\frac{\dot{a}}{a}\right)=\frac{1}{3M_{pl}^2}\sum_i\rho_i=\frac{1}{3M_{pl}^2}\sum_i\rho_{i_0}\left(\frac{a}{a_0}\right)^{-n_i}
		\end{equation*}
			$n_i=\frac{2}{3w_i+1}$\\
			\vspace{0.1 in}
			Null energy condition (NEC): \\$T_{\mu\nu}l^\mu l^\nu\geq 0$ for any light-like vector\\ $\rightarrow\rho+p\geq 0$\\
			\vspace{0.1 in}
			Dominant energy condition (DEC): \\$T_{\mu\nu}t^\mu t^\nu\geq 0$ for any time-like vector and $T^{\mu\nu}t_\mu$ non-spacelike \\$\rightarrow\rho\geq|p|$
	\end{minipage}
	\begin{minipage}[t]{0.5\textwidth}
	\centering
	Different fluid equations of state:\\
	\vspace{0.2 in}
	\begin{tabular}{lll}
		\multicolumn{1}{l|}{component}        & \multicolumn{1}{l|}{$w_i$}          & $n_i$ \\ \hline
		\multicolumn{1}{l|}{dust}             & \multicolumn{1}{l|}{0}              & $3$   \\
		\multicolumn{1}{l|}{radiation}        & \multicolumn{1}{l|}{$+\frac{1}{3}$} & $4$   \\
		\multicolumn{1}{l|}{curvature}        & \multicolumn{1}{l|}{$-\frac{1}{3}$} & $2$   \\
		\multicolumn{1}{l|}{dark energy (CC)} & \multicolumn{1}{l|}{$-1$}           & $0$   \\
		\multicolumn{1}{l|}{transient  DE}    & \multicolumn{1}{l|}{$\gtrsim -1$}       & $0$   \\
		\multicolumn{1}{l|}{stiff fluid}      & \multicolumn{1}{l|}{$1$}            & $6$   
	\end{tabular}
	\vspace{0.2 in}\\
	NEC+DEC implies:
	\begin{equation*}
		|w_i|=\left|\frac{\rho_i}{p_i}\right|\leq 1			
	\end{equation*}
	\end{minipage}
}
\caption{Cosmology reminder box. Conventionally the Friedmann equation is solved by dividing the energy content of the universe up into different ideal fluids with \mbox{$T^{i}_{\mu\nu}=\text{diag}\left(\rho_i,-p_i,-p_i,-p_i\right)$} in the rest frame. The equations of state for these are restricted by energy conditions reasonable forms of energy always agree to.}
\label{seqFigure1}
\end{figure}\\
\paragraph{Estimation of the residual cosmological constant.} The residual cosmological constant is
\begin{equation}
\left\langle \Lambda_{eff}\right\rangle=\frac{\int\mathrm{d}^4x\sqrt{-g}\ \tau^\alpha_\alpha }{\int\mathrm{d}^4x\sqrt{-g}}\equiv\frac{A}{B}
\end{equation}
An estimation on the classical level for the historical integrals $A$ and $B$ is laid out in \cite{Kaloper:2014dqa}. It goes as follows:
\begin{itemize}
\item Assume the equation of state of the universe to be an $n$-component classical fluid, such that the Friedmann equation (see figure \ref{seqFigure1}) describes the evolution of the universe. Then
\begin{align}
A&=\left[\int\mathrm{d}^4 x\sqrt{-g}\right]_{\text{FRW}}=vol_3\int_{t_{bang}}^{t_{crunch}}\mathrm{d}t\ a^3\\
B&=\left[\int\mathrm{d}^4 x \tau \sqrt{-g}\right]_{\text{FRW}}=vol_3\int_{t_{bang}}^{t_{crunch}}\mathrm{d}t\ \left(-\rho+3p\right) a^3
\end{align}
with $\rho=\sum_i\rho_i$. The comoving volume of the universe is $vol_3$.
\item Convince yourself that local inhomogenieties such as black holes do not contribute dominantly to historic integrals.
\item Assume the equation of state of each component of the fluid to fullfill NEC+DEC (see figure \ref{seqFigure1}): $|w_i|<1$
\item The Integrals A and B do converge classically, but for the case of $w_i=|1|$, where we need to cut off $B$ at Planck-Scale
\item  The idea of the estimate is to find the time in the history of the universe where the contributions are largest. For this we look at two adjacent times where the fluid components $\rho_i$ and $\rho_{i-1}$ dominate respectively. 
\end{itemize}
The first step is to divide the Integrals into contributions from different epochs $\Delta t_i=t_{i+1}-t_{i}$ dominated by $w_i$:
\begin{equation}
A_i=\int_{t_i}^{t_{i+1}}\mathrm{d}t\  a^3
=\int_{a_i}^{a_{i+1}}\mathrm{d }a\ \frac{a^2}{H_i}
=\frac{a_{i+3}^3}{H_{i+1}}\int_{a_i}^{a_{i+1}}\mathrm{d }a\ \frac{1}{a+1}\left(\frac{a_i}{a_{i+1}}\right)^2\frac{H_{i+1}}{H_i}
\end{equation}
where we just creatively multiplied by one so that we can use Friedman's equation for the time interval $\Delta t_i$
\begin{equation}
	H_i^2=H_{i+1}^2\left(\frac{a_{i+3}}{a_i}\right)^{3(1+w_i)}
\end{equation}
to get
\begin{equation}
	A_i=\frac{2}{3(3+w_i)}\left(\frac{a_{i+3}^3}{H_{i+1}}\right)\left[1-\left(\frac{a_i}{a_{i+1}}\right)^{3\frac{3+w_i}{2}}\right]
\end{equation}
The analogous for result for $B$ is
\begin{equation}
	B_i=\frac{2(3w_i-1)}{3(1-w_i)}\left(\frac{\rho_{i+1}a_{i+1}^3}{H_{i+1}}\right)\left[1-\left(\frac{a_i}{a_{i+1}}\right)^\frac{3(1-w_i)}{2}\right]
\end{equation}
For $|w_i|<1$, these integrals are regular even in the limit $a_i\rightarrow 0$. For $|w_i|=1$, the second integral is logarithmicly divergent, we cut off this integral at the Planck scale. By comparing adjacent timescales
\begin{align}
\frac{A_i}{A_{i-1}}&=\mathcal{O}(1)\left(\frac{a_{i+1}}{a_i}\right)^\frac{3(3+w_i)}{2}\\
\frac{B_i}{B_{i-1}}&=\mathcal{O}(1)\left(\frac{a_{i+1}}{a_i}\right)^\frac{3(1-w_i)}{2}
\end{align}
we see that these fractions are dominated by the timescale having the larger scale factor $a_i$. Conclusively, the dominant contribution comes from the turning point $T$ when the universe is largest: $a_{max}\equiv a(T)$. Assuming a symmetric cosmological evolution, the timescale $T$ is roughly the same (off a factor of $2$) as the total age of the universe $t_{age}$ from the Bang to the Crunch. The contribution to $A$ at the time $T$ is ergo approximately
\begin{equation}
	A_{turn}=\int_{0}^{t_{age}}\mathrm{d}t\ a_{max}^3\sim a^3 t_{age}\sim\frac{a_{max}^3}{H_{age}}\sim\frac{1}{H_{age}}
\end{equation}
Analogously
\begin{equation}
	B_{turn}\sim\frac{\rho_{age}}{H^4_{age}}
\end{equation}
where we introduced $\rho_{age}\sim M_{pl}^2 H_{age}^2$. Assuming that contributions from other times than $T$ are subdominant, $A\sim A_{turn},\ B\sim B_{turn}$, we have our final estimate 
\begin{equation}
A=\mathcal{O}(1) \frac{vol_3}{H^4_{age}}\qquad\qquad
B=\mathcal{O}(1)\frac{vol_3\ \rho_{age}}{H^4_{age}}
\end{equation}

\begin{equation}
\Lambda_{eff}=\frac{A}{B}=\frac{1}{4}\left\langle\tau\right\rangle\simeq\mathcal{O}(1)\rho_{age}\simeq\mathcal{O}(1)M_{pl}^2H_{age}^2
\end{equation}
Using $t_0=\frac{1}{H_0}<\frac{1}{H_{age}}=t_{age}$, we get
\begin{equation}\label{seqEstimationForLambdaEff}
	\Lambda_{eff}\leq M_{pl}^2 H_0^2\sim\rho_{crit}
\end{equation}
Equation (\ref{seqEstimationForLambdaEff}) is the result we needed for the consistency of the sequestering Ansatz: $\Lambda_{eff}$ is guaranteed to be bound by the critical density for a the universe that lives  $t_{age}\gtrsim t_{0}\sim 10^{10} \text{ yrs}$.

At this point we should not get too excited, we might still check the contributions from phase transitions in the early universe to the cosmological constant which already spoiled the SUSY solution to the Cosmological Constant Problem.\\

\paragraph{Consistency with phase transitions in the early universe.}
This section again follows \cite{Kaloper:2014dqa}. In Sequestering, the contributions from phase transitions to the cosmic average $\left\langle\tau^\alpha_{\ \alpha}\right\rangle$ are present but become automatically small in old enough universes. This is because $\Lambda_{eff}$ is dominated by contributions from when the universe is the largest, as we argued before. To quantify this, consider a phase transition in the early universe at time $t_\star$, approximated by the step potential
\begin{equation}
V=V_{before}\left(1-\Theta(t-t_\star)\right)+V_{after}\theta\left(t-t_\star\right)
\end{equation}
Depending on whether find ourselves before or after $t_\star$, for the right-hand side of Einstein's equation, we have
\begin{equation}
\tau^\mu_{\ \ \nu}-\frac{1}{4}\delta^\mu_\nu\left\langle\tau\right\rangle=\begin{cases}
-\left\langle V_{before}-V\right\rangle\delta^\mu_\nu&\quad\ t<t_\star\\
-\left\langle V_{after}-V\right\rangle\delta^\mu_\nu &\quad\ t>t_\star
\end{cases}
\end{equation}
where we have used the fact that the constants $V_{before/after}$ can be taken into the space-time average. Since we observe the phase transition from $t>t_\star$, we are interested in the second case. For $t>t_\star$, we have $V_{after}-V=0$, so the only contributions to the space-time average $\left\langle V_{after}-V\right\rangle$ comes from earlier times $t<t_\star$ where $V_{after}-V=V_{after}-V_{before}\equiv-\Delta V$. 
\begin{equation}
\left\langle V_{after}-V\right\rangle=-\Delta V\frac{\int^{t_\star}_{t_{bang}}\mathrm{d}t\ a^3}{\int^{t_{bang}}_{t_{crunch}}\mathrm{d}t\ a^3}\sim-\Delta V H_{age}^4\int_{t_{bang}}^{t_\star}\mathrm{d}t\ a^3
\end{equation}
Largest contributions come from epochs where scale factors are the largest, so for the integral from $t_{bang}$ to $t_\star$, we use the approximation
\begin{equation}
\int_{t_{bang}}^{t_\star}\mathrm{d }t\ a^3\sim\mathcal{O}(1)\frac{a_\star^3}{H_\star}
\end{equation}
with $H_\star=H(t_\star)$ and $a_\star=a(t_\star)$. With the additional assumption of one dominating fluid governing the evolution of the universe between $t_\star$ and $T$, we can use the\\ \mbox{Friedmann-equation $H_{age}^2=H_\star^2\left(\frac{a_\star}{a_{max}}\right)^\frac{1-w}{1+w}$} to get
\begin{equation}
\left\langle V_{after}-V\right\rangle=\mathcal{O}(1)\ \rho_{age}\ \frac{\Delta V}{M_{Pl}^2 H_\star^2}\left(\frac{H_{age}}{H_\star}\right)^\frac{1-w}{1+w}
\end{equation}
where $\rho_{age}\sim M_{Pl}^2 H_{age}$ and $a_{max}\sim\frac{1}{H_{age}}$.
With $\ H_\star^2\sim\frac{V_{before}}{M_{Pl}^2}>\frac{\Delta V}{M_{Pl}^2}$, together with $|w|\leq 1$, the final result is
\begin{equation}
\left\langle V_{after}-V\right\rangle\lesssim\rho_{age}<\rho_{crit}
\end{equation}

In large and old universes phase transitions in the early universe are automatically small.

\paragraph{Consistency with the inflation paradigm.} To work in the current model of our universe, the presumed phase of inflation must be consistent with the Sequestering mechanism in the early universe. In \cite{Kaloper:2014dqa}, a case is made for Starobinsky inflation being consistent with the framework as well as for slow roll large field inflation. Both scenarios do not get spoiled.

\section{Towards a manifestly local version of the sequestering mechanism}\label{seqSecLocalVariant}
This chapter is a condensed version of even more recent developments \cite{Kaloper:2015jra,DAmico:2017ngr} concerning the sequestering framework. We already hinted on the question whether  the global term in (\ref{seq1})  can be fundamental or whether an underlying microscopic theory is required. Arguments have been raised that this might not be the case \cite{Kaloper:2015jra}, because for the global term, the Markovian property
\begin{equation}
W(q_f,t_f,q_i,t_i)=\int\mathrm{d}q'\ W(q_f,t_f,q',t')W(q',t',q_i,t_i)
\end{equation}
of the path integral does not hold. This does not necessarily mean that the action (\ref{seq1}) is not compatible with quantum mechanics at all, but that microscopic degrees of freedom somehow must have been integrated out leading to the global term.\\

Following \cite{Kaloper:2015jra}, we construct a sequestering mechanism with an action that is manifestly local eluding the above problem. For this, we do the shift of thought to local lagrange multipliers $\kappa\rightarrow\kappa(x)$, $\Lambda\rightarrow\Lambda(x)$. We then try to engineer the action such that the equations of motion will yield
\begin{equation}
\partial_\mu\Lambda(x)=0 \qquad \partial_\mu \kappa(x)=0
\end{equation}
Then $\Lambda$ in particular is just an integration constant like in Unimodular Gravity (UMG). This is why we borrow from the UMG action. We will not use the version presented in the motivation but a generally covariant generalized variant introduced in \cite{Henneaux:1989zc}
\begin{equation}
S_{UMG}=\int\mathrm{d}^4 x \sqrt{-g}\left[\frac{M_{pl}^2}{2}R-\mathcal{L}_m(g^{\mu\nu},\Psi)\right]-\int\Lambda(x)\left[\sqrt{-g}\mathrm{d}^4x-\frac{1}{4!}F_{\mu\nu\lambda\sigma}\mathrm{d}x^\mu\mathrm{d}x^\nu\mathrm{d}x^\lambda\mathrm{d}x^\sigma\right],
\end{equation}
where we have introduced a $4$-form $F=\mathrm{d}A$$F_{\mu\nu\lambda\sigma}=4 \partial_{[\mu}A_{\nu\lambda\sigma]}$ in the gauge-fixing term instead of the number 1. $F$ is an auxiliary field not taking part in dynamics.
The equations of motion when varying with respect to the fields $A^{\nu\lambda\sigma}$ and $\Lambda$ are
\begin{align}
\frac{\delta\mathcal{L}}{\delta A^{\nu\mu\sigma}}&=0\qquad\rightarrow\qquad\partial_\mu\Lambda(x)=0\\
\frac{\delta\mathcal{L}}{\delta \Lambda}\ \ &=0\qquad\rightarrow\qquad\sqrt{-g}\epsilon_{\mu\nu\lambda\sigma}=F_{\mu\nu\lambda\sigma}
\end{align}
By specifying $F_{\mu\nu\lambda\sigma}$ to be $\epsilon_{\mu\nu\lambda\sigma}$ the second equation would become the gauge fixing condition for UMG ($g=-1$), but we are not after that (Nevertheless the second equation should be read as a constraint on $g$).
Instead, we are doing the same trick for $\kappa(x)$ using a second $4$-form $\hat{F}$. To match the global Sequester, we additionally add $\sigma$-functions as prefactors finally yielding
\begin{multline}
S=\int\mathrm{d}^4x\sqrt{-g}\left[\frac{\kappa(x)}{2}R-\Lambda(x)-\mathcal{L}_m(g^{\mu\nu},\Psi)\right]\\ +\frac{1}{4!}\int\mathrm{d}x^\mu\mathrm{d}x^\nu\mathrm{d}x^\lambda\mathrm{d}x^\sigma\left[\sigma\left(\frac{\Lambda(x)}{\mu^4}\right)F_{\mu\nu\lambda\sigma}+\hat{\sigma}\left(\frac{\kappa^2(x)}{M_{pl}^2}\right)\hat{F}_{\mu\nu\lambda\sigma}\right]
\end{multline}
with $F=\mathrm{d}A,\ \hat{F}=\mathrm{d}\hat{A}$.
The full equations of motion are. 
\begin{align}
\kappa^2 G^\mu_{\ \nu}&=\left(\nabla^\mu\nabla_\nu-\delta^\mu_{\ \nu}\nabla^2\right)\kappa^2+T^\mu_{\ \nu}-\Lambda(x)\delta^\mu_{\ \nu}\label{seqLocEoM1}\\
\frac{\sigma'}{\mu^4}F_{\mu\nu\lambda\sigma}&=\sqrt{-g}\epsilon_{\mu\nu\lambda\sigma}\label{seqLocEoM2}\\
\frac{\sigma'}{\mu^4}\partial_\mu\Lambda&=0\label{seqLocEoM3}\\
\frac{\hat{\sigma}'}{M_{pl}^2}\hat{F}_{\mu\nu\lambda\sigma}&=-\frac{1}{2}R\sqrt{-g}\epsilon_{\mu\nu\lambda\sigma}\label{seqLocEoM4}\\
\frac{\hat{\sigma}'}{M^2_{pl}}\partial_\mu\kappa^2&=0\label{seqLocEoM5}
\end{align}\\
Equations (\ref{seqLocEoM3}) and (\ref{seqLocEoM5}) are the equations we wanted and the analogies. Tracing out the gravity equation and taking the space-time average yields an expression for the cosmological constant
\begin{equation}
\Lambda=\frac{1}{4}\langle T^\alpha_{\ \alpha}\rangle+\frac{1}{4}\kappa\langle R^\alpha_{\ \alpha}\rangle\qquad 0=\frac{1}{4}\langle T^\alpha_{\ \alpha}\rangle\ \underbrace{-\frac{\mu^4}{2}\frac{\kappa^2\hat\sigma'}{M_{pl}^2\sigma'}\frac{\int \hat F_4}{\int F_4}}_{\Delta\Lambda}
\end{equation}
Resubstituting into gravity equation
\begin{equation}
\kappa^2G^\mu_\nu=T^\mu_\nu-\frac{1}{4}\delta^\mu_\nu\langle T \rangle-\Delta\Lambda \delta^\mu_\nu
\end{equation}
We see that again we achieved sequestering of vacuum energy up to a term $\Delta\Lambda$. $\Delta\Lambda$ depends on the auxiliary field. This is finite.\\

We now have an action resembling the global sequestering mechanism, but written in a manifestly local form. Local and global version are not equivalent and the resulting gravity equation is slightly different. In all cases the new Cosmological Constant Problem (The radiative instability of tuning) is solved at least. Also, other local variants using different generally covariant forms (e.g. $\mathrm{Tr}(G\wedge G)$, $G$ is a $2$-Form ) exist. Developing a minimal model of all vacuum energy sequestering terms (global and local variants combined) have been sought after \cite{DAmico:2017ngr}. 
