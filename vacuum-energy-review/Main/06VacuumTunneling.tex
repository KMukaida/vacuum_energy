%!TEX root = ../VacuumEnergy.tex


\chapter{Vacuum Tunneling}
\label{cha:tunneling}
\section{Particle in 1D}
\label{sec:tunnelingPart1d}

This section is entirely based on \cite{Coleman:1977py,Callan:1977pt,coleman_1985}.
We start by studying the well known problem of a quantum particle moving in a one dimensional potential. Our focus will be on unstable states, i.e. a particle that sits in a local minimum which is not the global one. The situation is represented in figure \ref{fig:1dtunnelingpotential} where the state at $x=0$ is an unstable state. 

\begin{figure}[t]
\centering
\includegraphics[width=10cm]{Figures/1dtunnelingpotential}
\caption{\small \textit{A typical potential with an unstable state at $x=0$.}}
\label{fig:1dtunnelingpotential}
\end{figure}

The idea is to compute the imaginary part of the energy of the state sitting at $x=0$. This imaginary part of the energy can then be identified with the decay width of this state and will therefore give us some information on the tunneling probability of this state. In order to compute this imaginary part, we will start by making some very general remarks about quantum mechanics.

Let us start by computing the transition probability between two position eigenstates. This can be done either in the matrix element formulation or equivalently in the Feynman path integral formulation. We will here restrict ourself to euclidean space-time:
\begin{equation}\label{eq:matrixelementpathintegral}
	\bra{x_f}e^{-HT/\hbar}\ket{x_i}=N\int\left[\df x\right] e^{-S/\hbar}
\end{equation}
where on the left hand side $H$ is the Hamiltonian, $\ket{x_f}$ and $\ket{x_i}$ are position eigenstates and $T>0$ is the time. We can expand the left hand side in term of energy eigenstates:
\begin{equation}
	\bra{x_f}e^{-HT/\hbar}\ket{x_i}=\sum_n e^{-E_n T /\hbar} \braket{x_f|n}\braket{n|x_i} ~.
\end{equation}
This makes it very transparent that for large $T$ the leading term is the one of lowest energy.

Let's now turn to the right hand side of equation \ref{eq:matrixelementpathintegral}. Here $N$ is a normalisation factor, $S=\int_{-T/2}^{T/2}\df t \left[\frac{1}{2} \left(\frac{\df x}{\df t}\right)^2 +V(x)\right]$ is the euclidean action and $\left[\df x\right]$ is the integration measure over all functions $x(t)$ such that $x(-T/2)=x_i$ and $x(T/2)=x_f$. To be more specific we can take any function $\hat{x}(t)$ that satisfies the boundary conditions %ZZZ and is a solution to the classical equations of motion ZZZ 
and then define
\begin{equation}
	x(t)=\hat{x}(t)+\sum_n c_n x_n(t)
\end{equation}
 where the $c_n$ are real coefficients and the $x_n$ form a complete set of real orthonormal functions that vanish at the boundary:
 \begin{equation}
 	\int_{-T/2}^{T/2}\df t x_n(t) x_m(t)=\delta_{nm} \qquad x_n(\pm T/2)=0~.
 \end{equation}
 In this case the integration measure becomes $\left[\df x\right]=\prod\limits_n (2\pi\hbar)^{-1/2} \df c_n$. The normalization factor $(2\pi\hbar)^{-1/2}$ will turn out to be a useful definition. Because we did not yet fix the over all normalization factor $N$ we are still free to chose whatever factor we want to put into the integration measure.

 In the semi classical (small $\hbar$) limit of $\int[\df x] e^{-S/\hbar}$ 
 %the stationary points in function space of $S$ clearly dominate ZZZ this doesn't seem quite right to me. E.g. perturbative corrections to the classical solution with smallest $S$ clearly dominate over other stationary points with bigger $S$. I think one should say something like this: 
 amplitudes can be written as a sum over classical saddle-points plus perturbations around them, i.e. $\mathcal{A}\sim \sum_i A_i e^{-S_i/\hbar}$, with $A_i=A_i^0+\hbar A_i^1+...$. Let's assume that there is only one stationary point and let's call it $\bar{x}(t)$. The fact that this is a stationary point implies:
 \begin{equation}\label{eq:stationarypoint}
 	\frac{\delta S}{\delta \bar{x}(t)} = -\frac{\df^2 \bar{x}(t)}{\df t^2} + V'(x)=0~.
 \end{equation}
 We will now expand the action around this stationary point:
 \begin{align}
 	S=& \int\df t \left[\frac{1}{2}\left(\frac{\df}{\df t}\left[\bar{x}+\sum_n c_n x_n\right]\right)^2 + V\left(\bar{x}+\sum_n c_n x_n\right)\right]\\
 \begin{split}
 	=& \int\df t \left[\frac{1}{2}\left(\frac{\df \bar{x}}{\df t}\right)^2 + \frac{\df \bar{x}}{\df t}\sum_n c_n \frac{\df x_n}{\df t} + \frac{1}{2}\sum_{n,m} c_n c_m \frac{\df x_n}{\df t}\frac{\df x_m}{\df t}+ \right.\\ 
 	&  \left. V\left(\bar{x}\right)+\sum_n c_n x_n V'\left(\bar{x}\right)+ \frac{1}{2}\sum_{n,m} c_n c_m x_n x_m V''\left(\bar{x}\right) + \dots \right]
 \end{split}
 \end{align}
 We then pull out the action of the stationary point $S(\bar{x})$ and integrate by parts:
 \begin{align}
 \begin{split}
 	=& S\left(\bar{x}\right) + \int \df t \left[-\frac{\df^2 \bar{x}}{\df t^2}\sum_n c_n x_n - \frac{1}{2}\sum_{n,m} c_n c_m x_n\frac{\df^2 x_m}{\df t^2}+ \right.\\ 
 	&  \left. \sum_n c_n x_n V'\left(\bar{x}\right)+ \frac{1}{2}\sum_{n,m} c_n c_m x_n x_m V''\left(\bar{x}\right) + \dots \right]
 \end{split}
 \\
 \begin{split}
 	=& S\left(\bar{x}\right) + \int \df t \left[ \sum_n c_n x_n \underbrace{\left( -\frac{\df^2 \bar{x}}{\df t^2}+V'\left(\bar{x}\right)\right)}_{=0} \right. \\ 
 	&  \left.+\frac{1}{2}\sum_{n,m}c_n c_m x_n \left(-\frac{\df^2 x_m}{\df t^2} +x_m V''\left(\bar{x}\right)\right) + \dots\right]
 \end{split}
 \end{align}
 So far we didn't specify what the functions $x_n$ are. We will now chose them to be the eigenfunctions of the second variational derivative of $S$ at $\bar{x}$ with eigenvalues $\lambda_n$:
 \begin{equation}
 	-\frac{\df^2 x_n}{\df t^2} + V''(\bar{x})x_n = \lambda_n x_n
 \end{equation}
 The action can therefore be approximated in the vicinity of the stationary point as (we are going to drop the $+\dots$ and abuse notation by still using equality signs):
 \begin{align}
 	S=&S(\bar{x}) + \frac{1}{2}\int \df t \sum_{n,m} c_n c_m x_n x_m \lambda_m\\
 	=& S(\bar{x}) + \frac{1}{2} \sum_n \lambda_n c_n^2~.
 \end{align}
This translates into a nice expression for the Feynman path integral close to the stationary point in the semi-classical approximation:
\begin{align}\label{eq:pathintegralatstationarypoint}
	\int [\df x] e^{-S/\hbar} &= e^{-S(\bar{x})/\hbar}\int (2\pi \hbar)^{-1/2} \prod\limits_n \df c_n e^{-\frac{1}{2\hbar}\sum_n \lambda_n c_n^2} \\
 	&= e^{-S(\bar{x})/\hbar} \prod\limits_n \lambda_n^{-1/2} \\
 	&\equiv e^{-S(\bar{x})/\hbar} \left[\det\left(-\partial_t^2-V''(x)\right)\right]^{-1/2}
\end{align}

There are a few remarks in order. Equation \ref{eq:stationarypoint} defining the stationary point is actually just the equation of motion of a particle moving in a potential $-V(x)$. This observation is going to be important when we want to compute the tunneling action numerically. Also, for a point particle moving in a one dimensional potential the energy $E=\frac{1}{2}\left(\frac{\df\bar{x}}{\df t}\right)^2-V(\bar{x})$ is a constant of the motion.


\subsection{A simple example}
\label{sec:simpleexampletunneling}

Let us study the case of a convex function with the global minimum located at $x=0$. An example is shown in figure \ref{fig:simpleexample}. We chose to study the case where $x_i=x_f=0$. Clearly in this case there is only one solution to equation \ref{eq:stationarypoint} defining a stationary point consistent with the boundary conditions $x_i=x_f=0$. It is the trivial state $\bar{x}(t)=0$.

\begin{figure}[t]
\centering
\includegraphics[width=10cm]{Figures/simpleexample}
\caption{\small \textit{A convex potential with a stable ground state at $x=0$.}}
\label{fig:simpleexample}
\end{figure}

It can be shown that for large $T$ the functional determinant can be evaluated:
\begin{equation}
	N\left[\det\left(-\partial_t^2 + V''(\bar{x}\right)\right]^{-1/2}=\left(\frac{\omega}{\pi\hbar}\right)^{1/2}e^{-\omega T/2}
\end{equation}
where $\omega^2 = V''(0)$. We can now plug this result into equation \ref{eq:matrixelementpathintegral}
\begin{equation}
	\sum_ne^{-E_nT/\hbar}\braket{x_f|n}\braket{n|x_i}\approx e^{-E_0T/\hbar}\braket{x_f|0}\braket{0|x_i}=\underbrace{e^{-S(\bar{x})/\hbar}}_{=1} \left(\frac{\omega}{\pi\hbar}\right)^{1/2}e^{-\omega T/2}~.
\end{equation}
This let's us identify the energy of the lowest energy eigenstate, $E_0=\frac{\hbar \omega}{2}$. This is obviously the old familiar result and the effort for determining it is clearly not justified. However we are going to see that this method, however tedious it was to compute the ground state energy in this example, is the method of choice to determine tunneling probabilities from unstable vacua to stable ones.

\subsection{The Bounce}
\label{sec:bounce}
We will now turn to a more interesting example of an unstable state. We sketched out the potential in figure \ref{fig:1dtunnelingpotential}. We are again interested in the state with $x_i=x_f=0$. This time the situation is much more interesting as now two solutions exist. There is still the solution $\bar{x}(t)=0$ but there is also the possibility that the particle moves away from $x=0$ to $x=\sigma$ then bounces back and returns to the point $x=0$. This second possibility is called the bounce and we show a sketched version of it on figure \ref{fig:bounce}. Clearly the bounce has a total energy $E=0$.

\begin{figure}[t]
\centering
\includegraphics[width=10cm]{Figures/bounce}
\caption{\small \textit{The evolution of the particle during a bounce. The particle starts of at $x=0$ then slowly starts rolling in the direction of $x=\sigma$. Once it reaches $x=\sigma$ it will have zero kinetic energy left and it will bounce back and return to $x=0$ at late times.}}
\label{fig:bounce}
\end{figure}

For $T\rightarrow\infty$ the true solution consists of many widely separated bounces. It is now easy to write down the action for one bounce:
\begin{equation}
	B\equiv S_{bounce} = \int_{-\infty}^{\infty} \frac{1}{2}\left(\frac{\df \bar{x}}{\df t}\right)^2 + V(\bar{x})\df t \underbrace{=}_{E=0}\int_{-\infty}^{\infty} \df t \left(\frac{\df \bar{x}}{\df t}\right)^2 = \int_0^\sigma \df x \left[2V(x)\right]^{1/2}~.
\end{equation}
One can show that the action for $n$ bounces is just given by $S_{\text{n bounces}}=nB$. With this we now have determined the factor $e^{-S(\bar{x})/\hbar}$ in our expanded path integral. What about the determinant factor?

We are going to make the assumption that for $n$ widely separated bounces the determinant can be written as:
\begin{equation}
	\det\dots=\left(\frac{\omega}{\pi\hbar}\right)^{1/2}e^{-\omega T/2}K^n
\end{equation}
where $K$ encodes the corrections of one bounce to the determinant of $\bar{x}=0$.

As the path integral takes into consideration all the possible histories, we have to integrate over the positions of the individual bounces. Those integrals can be nicely reduced:
\begin{equation}
	\int_{-T/2}^{T/2}\df t_1 \int_{-T/2}^{t_1}\df t_2 \dots \int_{-T/2}^{t_{n-1}}\df t_n = \frac{T^n}{n!} ~.
\end{equation}
The path integral also sums over the total number of bounces:
\begin{align}
	N\int[\df x] e^{-S/\hbar}=&\sum_{n=0}^\infty \left(\frac{\omega}{\pi\hbar}\right)^{1/2}e^{-\omega T/2}\frac{\left(Ke^{-B/\hbar}T\right)^n}{n!}\\
	=&\left(\frac{\omega}{\pi\hbar}\right)^{1/2}\exp\left[-\omega T /2 + K e^{-B/\hbar}T\right]~.
\end{align}
Which in turn allows us to identify the energy of the lowest lying energy eigenstate:
\begin{equation}
	E_0=\frac{\hbar\omega}{2}-\hbar K e^{-B/\hbar}~.
\end{equation}
One might justifiably question the sense of what we have just computed. The correction term $\hbar K e^{-B/\hbar}$ is exponentially smaller than the $\mathcal{O}(\hbar^2)$ terms that we have neglected. Why bother keeping it? It will turn out that $K$ has an imaginary part and hence $\hbar K e^{-B/\hbar}$ is the leading contribution to $\operatorname{Im}(E_0)$ and therefore to the decay width.

So let us calculate $K$. When we do so we quickly realize that there are two major problems.

\paragraph{First problem:} 
We can easily see that there is one eigenvalue of $-\frac{\df^2 x_n}{\df t^2}+V''(\bar{x})x_n$ that is zero. We can actually easily construct the corresponding eigenfunction:
\begin{equation}
	x_1=B^{-1/2}\frac{\df \bar{x}}{\df t}~.
\end{equation}
The integration over $c_1$ is therefore going to diverge. This is potentially a disaster. However we have actually already performed the integral in the disguise of the integral over the location of the bounces. This can be easily seen by noting the following equalities:
\begin{align}
	\df x =& x_1 \df c_1 \qquad \text{(change induced by small change in $c_1$)}\\
	\df x =& \frac{\df x}{\df t} \df t \qquad \text{(change induced by small change in $t$)}
\end{align}
Therefore the following equality holds:
\begin{equation}
	\left( 2\pi\hbar\right)^{-1/2}\df c_1 = \left(\frac{B}{2\pi\hbar}\right)^{1/2} \df t~.
\end{equation}
This shows that indeed, the integration over $c_1$ is identical to the integration over $t$ that we have already performed, up to constant factors.


\paragraph{Second problem:}
The function $\frac{\df \bar{x}}{\df t}$ has a zero. This can be easily seen just by looking at figure \ref{fig:bounce}. This means that the eigenfunction $x_1$ has a node. We know from the study of eigenfunctions of this type of differential equations that the eigenfunction with the lowest eigenvalue has no node, the second one has one and so on. Given that $x_1$ has one node, there must be an eigenfunction $x_0$ with no nodes and a lower eigenvalue than $\lambda_1$. But $\lambda_1=0$, which means that one of the eigenfunctions has a negative eigenvalue. When we want to compute the integral over $c_0$ in \ref{eq:pathintegralatstationarypoint} it will diverge.

What went wrong here? We are trying to compute an eigenvalue (ground-state energy) that is not in the spectrum of the Hamiltonian because it is the energy of an unstable state. The way out is by analytic continuation. Doing this will give the following result:
\begin{equation}
	\left(\operatorname{Im}\left[{N\int[\df x] e^{-S/\hbar}}\right]\right)_{\text{one bounce}}=\frac{1}{2}Ne^{-B/\hbar}\left(\frac{B}{\hbar 2 \pi}\right)^{1/2} T \left| {\det {'}}\left[-\partial_t^2+V''(\bar{x})\right]\right|^{-1/2}
\end{equation}
where $\det'$ means that the zero eigenvalue is omitted in the computation of the determinant.

We can now compute $K$
\begin{equation}
	\operatorname{Im}(K)=\frac{1}{2}(B/2\pi\hbar)^{1/2}\left|\frac{\det'\left[-\partial_t^2+V''(\bar{x})\right]}{\det\left[-\partial_t^2+\omega^2\right]}\right|^{-1/2}
\end{equation}
and the decay rate
\begin{equation}
	\Gamma=-2\Im(E_0)/\hbar=(B/2\pi\hbar)^{1/2}e^{-B/\hbar}\left|\frac{\det'\left[-\partial_t^2+V''(\bar{x})\right]}{\det\left[-\partial_t^2+\omega^2\right]}\right|^{-1/2} ~.
\end{equation}
This result concludes the first part of the discussion on vacuum tunneling. Let us now move to study the case of a quantum field theory.


\section{Field Theory}
\label{sec:tunnelingFieldTheory}
This section is entirely based on \cite{Coleman:1977py,Callan:1977pt,coleman_1985,Kolb:1990vq}. The step from ordinary quantum mechanics to quantum field theory is rather easy in this case. The euclidean action for a scalar field $\phi$ moving in a potential $U(\phi)$ (see figure \ref{fig:qftpotential}) is written as
\begin{equation}
	S_E=\int\df^4x \left[\frac{1}{2}\left(\partial_\mu \phi\right)^2 + U(\phi)\right]
\end{equation}
We already know most of what we need to do!

\begin{figure}[t]
\centering
\includegraphics[width=10cm]{Figures/qftpotential}
\caption{\small \textit{The potential $U(\phi)$ in a scalar field theory. The unstable minimum is at $\phi_+$ whereas the stable one is located at $\phi_-$.}}
\label{fig:qftpotential}
\end{figure}

The bounce is the solution of the Euclidean equations of motion
\begin{equation}\label{eq:euclideaneom}
	\partial_\mu\partial_\mu\bar{\phi}=U'(\bar{\phi})
\end{equation}
with the boundary condition
\begin{equation}
	\lim_{x_4\to\pm\infty} \bar{\phi}\left(\vec{x},x_4\right)=\phi_+
\end{equation}
and we also demand that the action of the bounce is finite, which gives us additional boundary conditions:
\begin{equation}
	\lim_{|\vec{x}|\to\infty}\bar{\phi}\left(\vec{x},x_4\right)=\phi_+~.
\end{equation}
Once the bounce is found the decay rate can be easily computed:
\begin{equation}
	\frac{\Gamma}{V}=Ke^{-S_0/\hbar}
\end{equation}
where $V$ is the volume, $S_0=S(\bar{\phi})$ and $K$ is a factor that involves $\det'$ and $\det$.


Some Remarks are in order:
\begin{itemize}
	\item This is a very powerful method. We reduce the study of a system with infinitely many degrees of freedom to the study of a single classical partial differential equation.\\
	\item The factor $V$ in $\frac{\Gamma}{V}$ arises naturally as non-trivial solutions to the bounce equation are not translationally invariant. This means that we must integrate over the location of the bounces. \\
	\item We now have four eigenfunctions with zero eigenvalues, one for each $x^\mu$. Hence there is a factor of $\left(\frac{B}{2\pi\hbar}\right)^{1/2}$ for each $x^\mu$. Therefore the decay rate is given by:
	\begin{equation}
	 	\frac{\Gamma}{V}=\frac{B^2}{4\pi^2\hbar^2}e^{-B/\hbar}\left|\frac{\det'\left[-\partial^2+U''(\bar{\phi})\right]}{\det\left[-\partial^2+U''(\phi_+)\right]}\right|^{-1/2}~,
	\end{equation} 
	where $B=S_0$ is the action of one bounce.
\end{itemize}

\subsection{How to compute the bounce}
\label{sec:computebounce}
Let us now discuss how we are going to compute the bounce solution. First we remark that so far, all the equations had a $O(4)$ symmetry. It seems therefore totally reasonable to assume that%ZZZ there exist $O(4)$-symmetric solutions ZZZ 
also the solutions to those equations will be $O(4)$ symmetric. This is indeed the case. One can show that a $O(4)$ symmetric solution always exist and that its action is lower than any non $O(4)$ symmetric solution 
%ZZZ I thought that there was a mistake in the proof of this statement. ZZZ. 
The second statement is hard to prove, but the first one can be argued for very intuitively.

The $O(4)$ symmetric field can be written as $\phi(r)$ where $r^2=t_E^2+|\vec{x}|^2$. Therefore the equation motion \ref{eq:euclideaneom} simplifies to:
\begin{equation}\label{eq:o4bounce}
	\frac{\df^2 \bar{\phi}}{\df r^2}+\frac{3}{r}\frac{\df \bar{\phi}}{\df r}=U'(\bar{\phi})
\end{equation}
with the boundary conditions
\begin{equation}
	\lim_{r\to\infty}\bar{\phi}(r)=\phi_+~.
\end{equation}
In order to obtain non-singular solutions we have to require $\left.\frac{\df \bar{\phi}}{\df r}\right|_{r=0}=0$.

Equation \ref{eq:o4bounce} is a mechanical equation of motion for a classical particle moving in a potential $-U$ subject to a strange viscous damping force proportional to $\frac{1}{r}$. In this picture $r$ plays the role of time. We will now show that this equation always admits a solution. In order to do so, we will consider the classical picture of a particle moving in a potential $-U$. Finding a solution to equation \ref{eq:o4bounce} requires finding the release point. This is the point where we have to release the particle in the potential $-U$ such that at $r\to\infty$ the particle gets at rest at $\phi_+$. Let us discuss two extreme cases:
\begin{itemize}
	\item[Case 1:] The particle is released to the left of $\phi_0$. In this case it doesn't have enough energy to reach $\phi_+$. The particle is said to undershoot.
	\item[Case 2:] The particle is released very close to $\phi_-$. In this case it will sit on top of the hill at $\phi_-$ and only very slowly start rolling towards $\phi_+$. This means that it takes a long time for the particle to really start moving (i.e. $r\gg1$). This in turn implies that the damping force gets arbitrarily suppressed. It can therefore be neglected and the particle will freely slide down the hill and will, by conservation of energy, reach the hill with non zero velocity. Hence the particle will overshoot and continue rolling down past $\phi_+$.
\end{itemize}
By continuity there must exist a point between the two extremal cases discussed above, where the particle will exactly end at $\phi(r)=\phi_+$ at $r\to\infty$. Hence the $O(4)$ symmetric solution is guaranteed to exist. This argument can actually be used in order to determine the bounce numerically by the so called overshoot/undershoot method. The release point is found in a binary search and equation \ref{eq:o4bounce} can easily be solved numerically.

Once $\bar{\phi}(r)$ is known it is straight forward to compute the Euclidean action:
\begin{equation}
	S_E^{O(4)}=2\pi^2\int_0^\infty \df r r^2 \left[\frac{1}{2}\left(\frac{\df \bar{\phi}}{\df r}\right)^2+ U(\bar{\phi})\right]
\end{equation}

\subsubsection{Thin wall approximation}
\label{sec:thinwall}
There are some cases where an analytical approximation to the bounce solution can be found. If the potential presents two almost degenerate vacua the so called thin wall approximation is a very good analytical approximation to the true solution. We model this situation by defining the potential as
\begin{equation}
	V(\phi)=V_0(\phi)+V_\epsilon(\phi)
\end{equation}
where $V_0$ is a potential with two degenerate minima and $V_\epsilon$ is a small perturbation that lifts this degeneracy. For example we could consider the potential
\begin{align}
	V_0(\phi)=& \frac{\lambda}{4}\phi^2(\phi-\phi_0)^2\\
	V_\epsilon=& -\frac{\lambda}{2}\epsilon \phi_0\phi^3~.
\end{align}
In the limit $\epsilon\to0$ the two minima at $0$ and $\phi_0$ are exactly degenerate. For very small $\epsilon$ the particle has to be released very close to the true vacuum $\phi_0$. The potential very close to $\phi_0$ is almost flat. Hence the particle will sit there for a long "time" $r=R$ and the damping term can be neglected. There are therefore a number of approximations that hold in the limit of small $\epsilon$. This greatly simplifies the equations
\begin{equation}
	\frac{\df ^2 \phi}{\df r^2}=V'(\phi)\simeq V_0'(\phi)~.
\end{equation}
We can integrate this equation by parts
\begin{equation}
	\frac{\df \phi}{\df r}=-\sqrt{2V_0(\phi)} \quad \Rightarrow \quad r=\int^\phi \df \phi' \frac{1}{\sqrt{2V_0(\phi')}}~.
\end{equation}
In the example given above, this can be explicitly solved and one finds:
\begin{equation}
	\phi^{TW}(r)=\frac{1}{2}\phi_0\left(1-\tanh\frac{r-R}{\Delta}\right)~,
\end{equation}
where $\Delta=\phi_0^{-1}\sqrt{8/\lambda}$. This equation is valid for $r\sim R$, which we will shortly compute to be $R=(\epsilon \phi_0 \sqrt{2\lambda})^{-1}$. We note that the width of the bubble wall is given by $\Delta$ and in the small $\epsilon$ limit
\begin{equation}
	\frac{\Delta}{R}=\frac{2\epsilon}{\lambda} \ll 1~.
\end{equation}
Therefore the wall is very thin compared to the typical size of the bubble, hence the name.

The complete bounce solution can in this case be given by
\begin{equation}
	\phi(r)=\begin{cases} \phi_0 & r\ll R \\
	\phi^{TW} & r\sim R \\
	0 & r\gg R

	\end{cases}~.
\end{equation}
We can therefore easily compute the Euclidean action.
\begin{align}\label{eq:determineRinthinwall}
	S_E =& 2\pi^2 \int_0^\infty r^3 \df r \left[\frac{1}{2}\left(\frac{\df \phi}{\df r}\right)^2 + V(\phi)\right] \\
	=& \underbrace{-2\pi^2 \frac{R^4}{4}\Delta V}_{\text{from } r\ll R} + \underbrace{2\pi^2R^3\int_0^{\phi_0}\df \phi \sqrt{2V_0(\phi)}}_{\text{from } r\sim R}\\
	=& -\frac{1}{4}\pi^2 R^4 \lambda \epsilon \phi_0^4 +\frac{1}{6} \pi^2 R^3 \sqrt{\lambda} \phi_0^3~,
\end{align}
where the last equality hold in our example and where $\Delta V=|V(0)-V(\phi_0)|=\frac{\epsilon\lambda\phi_0}{2}$.

Finding the promised value for $R$ is now easy. We just minimize the Euclidean action with respect to $R$.
\begin{equation}
	\frac{\df S_E}{\df R}=0 \quad \Rightarrow \quad R=2(\Delta V)^{-1}\int_0^{\phi_0} \df\phi \sqrt{2V(\phi)}~.
\end{equation}
The Euclidean action is therefore given by
\begin{equation}\label{eq:thinwallaction}
	S_E=\frac{27\pi^2\left[\int_0^{\phi_0}\df\phi \sqrt{2V_0(\phi)}\right]^4}{2(\Delta V)^3}~.
\end{equation}
And in our example this is readily evaluated $S_E=\pi^2/48\lambda\epsilon^3$. 

When looking at equation \ref{eq:determineRinthinwall} we see that there are two contributions to the action. One is coming from the region inside the bubble and it is generically negative. This contribution can be associated to the free energy released during the phase transition The other contribution comes from the bubble wall and can be attributed to the surface tension of the bubble wall. This second contribution is positive. For a consistent result we have to require that the action be positive, so one could be worried if this is always the case. From equation \ref{eq:thinwallaction} we see that in the thin wall approximation this is indeed the case. We can however answer this question more generally. To do so we embed $\bar{\phi}$ in a family of one parameter functions $\phi_\lambda(x)=\bar{\phi}(x/\lambda)$. The action is then given by
\begin{equation}
	S_E(\phi_\lambda)=\frac{1}{2}\lambda^2\int \df^4 x(\partial_\mu \bar{\phi})^2 + \lambda^4 U(\bar{\phi})~.
\end{equation}
The field $\bar{\phi}$ is a solution to the equations of motion. Therefore $S$ has to have a stationary point at $\lambda=1$, i.e. $\left.\frac{\df S}{\df \lambda}\right|_{\lambda=1}=0$. This will relate different parts of the action and will allow us to prove the positivity of the action.
\begin{equation}
	\int \df^4 x (\partial_\mu \bar{\phi})^2 = -4 \int \df^4 x U(\bar{\phi})~.
\end{equation}
And therefore the action can be easily shown to be positive
\begin{equation}
	S=\frac{1}{4}\int\df^4x(\partial_\mu \bar{\phi})^2 > 0~.
\end{equation}

\subsection{Remarks on finite temperature}
\label{sec:finitetemptunneling}
Finite temperature $T$ introduces a periodicity of $T^{-1}$ in imaginary time. This results in an additional constraint for the tunneling field, namely
\begin{equation}
	\phi(t_E,\vec{x})=\phi(t_E+T^{-1},\vec{x})~.
\end{equation}
This is a clear violation of the $O(4)$ symmetry that was used in the previous sections to find the bounce solution. One can show that if $T$ is large enough the solution with the smallest action has an $O(3)$ symmetry. The resulting Euclidean action is defined as
\begin{align}
	S_E=&S_3/T\\
	S_3=&\int\df^3x\left[\frac{1}{2}(\nabla^2\phi)^2+V_T(\phi)\right]~,
\end{align}
where $V_T(\phi)$ is the finite temperature potential. The solution that minimizes the $S_3$ action therefore obeys
\begin{align}\label{eq:o3equation}
	&\frac{\df^2\phi}{\df s^2}+\frac{2}{s}\frac{\df \phi}{\df s}-V_T'(\phi)=0\\
	& \phi(s\to\infty) = \text{false vacuum}\\
	& \left.\frac{\df \phi}{\df s}\right|_{s=0}=0~.
\end{align}
And the formula for the $O(3)$ symmetric action simplifies to
\begin{equation}
	S_3=4\pi\int s^2\df s\left[\frac{1}{2}\left(\frac{\df \phi}{\df s}\right)^2+V_T(\phi)\right]~.
\end{equation}

\section{Two Field Tunneling}
\label{sec:tunnelingTwoFields}
We next discuss what happens in a potential that depends on more than one scalar field. For simplicity, let us focus on $O(3)$-symmetric bubbles. For multiple fields, the equation of motion \ref{eq:o3equation} becomes
\begin{equation}\label{eq:O3equationMultifield}
\frac{d^2\vec{\phi}}{d s^2} \, + \, \frac{2}{s}\frac{d\vec{\phi}}{ds} \, =\, \nabla V(\vec{\phi})\, ,
\end{equation}
while the boundary conditions now read
\begin{equation}\label{eq:O3boundaryconditionsMultifield}
\lim_{s\to \infty}\vec{\phi}(s) \, = \, \vec{0} \quad \text{and} \quad \left.\frac{d\vec{\phi}}{ds}\right|_{s=0}  = \, \vec{0} \, . 
\end{equation}



Clearly the overshoot/undershoot method does not work any more in higher-dimensional field space as the path in this case does not have to pass by $\vec{\phi}=\vec{0}$ but can also avoid this point by going around it in field space. A binary search is therefore impossible. In order to numerically find the path in field space that minimizes the action, a modified procedure is required. To this end, 
one starts with an initial guess $\vec{\phi}_g(x)$ for this path, where $x$ is a parameter that measures the distance along the path. If we normalize the path according to $\left|d\vec{\phi}_g/dx\right|=1$, the equation of motion \ref{eq:O3equationMultifield} can be nicely separated into parts parallel and perpendicular to the path \cite{Beniwal:2017eik,Wainwright:2011kj}:
%In order to nicely divide equation \ref{eq:O3equationMultifield} into a part parallel to the path and a part that is perpendicular to the path we need to fix $\left|\frac{d\vec{\phi}_g}{dx}\right|=1$ along the path. Then equation \ref{eq:O3equationMultifield} can be separated as \cite{Beniwal:2017eik,Wainwright:2011kj}:
\begin{eqnarray}\label{eq:eomAlongPath}
	\frac{d^2x}{ds^2} \, + \, \frac{2}{s}\frac{dx}{ds} \, = \, &\partial_x V[\vec{\phi}_g(x)] \\
	\label{eq:eomPerpPath}
	\frac{d^2\vec{\phi}_g}{dx^2}\left(\frac{dx}{ds}\right)^2 \, = \, &\nabla_\perp V[\vec{\phi}_g]\, .
\end{eqnarray}
The first equation is just the usual bounce equation for a one dimensional potential which can be solved using the overshoot/undershoot method. The second equation, on the other hand, can be understood
%Whereas the second equation is the one constraining the path. It can therefore be seen as 
as defining a normal force 
\begin{equation}\label{eq:normalForceAlongPath}
\vec{N} \, \equiv \, \frac{d^2\vec{\phi}_g}{dx^2}\left(\frac{dx}{ds}\right)^2 - \, \nabla_\perp V[\vec{\phi}_g]
\end{equation}
that acts perpendicularly on the path until the second equation is fulfilled. The procedure to find the path that minimizes the action is then the following:
\begin{enumerate}
\item Guess an initial path. One could for example take a straight line connecting the false and the true vacuum.
\item Calculate the bounce for $x(s)$ from the equation of motion  \ref{eq:eomAlongPath}.\label{it:bounce}
\item Determine the normal force \ref{eq:normalForceAlongPath} and deform the path in the direction of the force. \label{it:bounce2}
\item Repeat steps \ref{it:bounce} and \ref{it:bounce2} until the path does not get significantly deformed any more.
\end{enumerate}
Note that for certain potentials, with multiple valleys, this algorithm might not automatically converge to the right solution. In those cases there is still some manual adjusting needed.

In practice the force is only calculated at a finite number $n$ of points along the path and then those points are displaced in the direction of the force. In our case, with only two fields $\vec{\phi}(x)=\left(\phi^1(x), \phi^2(x)\right)$, the algorithm for the deformation of the path then is:
\begin{enumerate}
	\item Define points $\phi^{1,2}_i=\phi^{1,2}(x_i)$ along the path, where $x_i=x_{max}/n*i$ and $x_{\textrm{max}}$ is the value of $x$ for which $\vec{\phi}(x_{\textrm{max}})=\vec{0}$.
	\item Displace the points, $\hat{\phi}^{1,2}_i = \phi^{1,2}_i+\rho\vec{N}(x_i)$, where $\rho$ is a small step size (typically we use $\rho=0.02$).
	\item Fit a function to the displaced points, $\phi^{1,2}(x)=\operatorname{Fit}[\{x_i,\hat{\phi}^{1,2}_i\},P_l]$ with $P_l$ being a polynomial of order $l$.
\end{enumerate}
We have found that $l=6$ is sufficient and adding higher orders does not significantly improve the result.