%!TEX root = ../VacuumEnergy.tex

	\chapter{Tunneling \& Gravity}
	In this section we will review the generalization of Coleman's field theory tunneling processes at finite Planck mass $M_P$, as was pioneered by Coleman and de Luccia (CdL) \cite{Coleman:1980aw}. Obviously, the pure field theory formulas derived in \cite{PhysRevD.15.2929,PhysRevD.16.1762}, and reviewed in Chapter $7$ and \ref{ssec:nograv} will receive Planck suppressed corrections upon incorporating gravitational effects. However, and somewhat more interestingly, qualitatively new and exciting effects arise that have no direct analogue in the pure field theory as we shall see starting from Section \ref{ssec:CdL}.
	
	Before we will get to these new effects, in Section \ref{ssec:nograv} we briefly review tunneling in a field theory with a single scalar field (i.e. in the limit $M_P\longrightarrow \infty$), in order to pave the ground for its generalization at finite $M_P$ (Section \ref{ssec:CdL}). All calculations are done in the \textit{thin-wall approximation}.
	
	Throughout this chapter our conventions are such that the metric signature is \textit{mostly plus}, i.e. $\eta=(-,+,+,+)$. The reduced Planck-mass $M_P$ is,
	\begin{equation}
	M_P\equiv (8\pi G_N)^{-1/2}\, ,
	\end{equation}
	where $G_N$ is Newtons constant.
	Moreover, we often use units in which $\hbar=1$ (and sometimes also $M_P=1$).
	\section{Review (no gravity)}\label{ssec:nograv}
	We consider the following action
	\begin{equation}
	S[\phi]=\int d^4 x\left(-\frac{1}{2}(\del\phi)^2-V(\phi)\right)\, ,
	\end{equation}
	of a single scalar field $\phi$ with scalar potential $V(\phi)$. 
	
	We assume that the scalar potential has two distinct local classical minima, $\phi_-$ and $\phi_+$ with $-\epsilon\equiv V(\phi_-)<V(\phi_+)= 0$ and $\phi_-$ is a global minimum (see Figure \ref{fig:potential}). 
	\begin{figure}
		\centering
		\includegraphics[keepaspectratio,width=11cm]{Figures/potential}
		\caption{The scalar potential: There exists a false (left) and a true (right) minimum.}
		\label{fig:potential}
	\end{figure}
	Note that we can always choose $V(\phi_+)=0$ since the overall potential is not observable in pure field theory. Classically, both $\phi(x)\equiv \phi_{\pm}$ are solutions to the equations of motion and hence stable \textit{vacua} of the theory. Quantum mechanically, we expect a unique ground state $\ket{0}\equiv \ket{\phi_-}$ and a finite decay rate (per volume) 
	\begin{equation}
	\Gamma/V \left(\underset{\text{via tunneling}}{\ket{\phi_+}\longrightarrow \ket{\phi_-}}\right)\, ,
	\end{equation}
	where $\ket{\phi_+}$ is a state that corresponds to the classical vacuum $\phi=\phi_+$. We will discuss what is meant by this momentarily.
	\subsection{The analogous Quantum Mechanics problem}\label{sssec:QM}
	To clear up potential ambiguities it is useful to first consider the analogous Quantum Mechanics (QM) problem of a single particle trapped in a potential $V(x)$ with the above properties (i.e. we replace $\phi\longrightarrow x$). If the ground state energy $E_0$ satisfies $E_0\ll V(x_+)$, the ground state $\ket{0}$ is well localized in the region $x\sim x_-$, in other words $\bra{0}\hat{x}\ket{0}\approx x_-$. Otherwise, the ground state wavefunction $\psi_0(x)$ has comparable support in both regions $x\approx x_{\pm}$. Defining an appropriate quantum analogue of the classical vacuum $x=x_+$ is less straightforward. Loosely speaking it should be an initial wavefunction that is localized within the region $x\approx x_+$, approximately stable, but subject to barrier penetration.
	
	One may define a state that satisfies these criteria (henceforth \textit{false vacuum} state) $\ket{x_+}$ by \textit{choosing} an analytic continuation of the scalar potential $V(x)$ that lifts the minimum $x=x_-$ but leaves the potential essentially untouched in the vicinity of $x_+$ (see figure \ref{fig:ancont}).	
	\begin{figure}
		\centering
		\includegraphics[keepaspectratio,width=11cm]{Figures/an_cont_potential}
		\caption{The analytic continuation: The true vacuum is deformed into to false vacuum. Now the ground-state wavefunction localizes in the used-to-be-false vacuum.}
		\label{fig:ancont}
	\end{figure}
	We define $\ket{x_+}$ via the ground-state wavefunction $\psi_+(x)$ of \textit{that} system \cite{coleman_1985}. Then, one can compute the time evolution
	\begin{equation}
	\ket{\psi_+}(t)\equiv \exp\left(-\frac{iHt}{\hbar}\right)\ket{\psi_+}\, .
	\end{equation}
	Very intuitively, tiny drops of the wavefunction start to leak through the barrier and start to oscillate in the well around $x_-$ (see Figure \ref{fig:double-well}). If we take the QM problem literally, the wavepacket cannot evolve to the true ground state since energy is conserved. We will assume that the drops that have leaked through the barrier can relax to the ground state of the potential well around $x=x_-$ by dissipating their kinetic energy to degrees of freedom that are left unspecified.
	\begin{figure}[t]
		\begin{center}
		\begin{tabular}{c | c }
			\includegraphics[keepaspectratio,width=7cm]{Figures/double-well_0} & \includegraphics[keepaspectratio,width=7cm]{Figures/double-well_1} \\ 
			\includegraphics[keepaspectratio,width=7cm]{Figures/double-well_2} &
			\includegraphics[keepaspectratio,width=7cm]{Figures/double-well_3}
		\end{tabular}
		\end{center}
		\caption{The time-evolution of the initial state "false-vacuum" in a double-well potential (shaded step function with infinite height on both sides of the interval that is shown). A "drop" detaches from the false-vacuum peak, and passes through the barrier. Since the potential is locally flat, the wave-packet dissipates.}
		\label{fig:double-well}
	\end{figure}
	In this case, the drops go through the barrier one-by-one, and thus reduce the part of the wavefunction that is localized near $x=x_+$ exponentially on sufficiently long time scales,
	\begin{equation}\label{exp-decay}
	\bra{x_+}\exp\left(-\frac{iHt}{\hbar}\right)\ket{x_+}\overset{t\gg \epsilon^{-1}}{\longrightarrow}e^{-\frac{i\epsilon t}{\hbar}}e^{-\frac{\Gamma t}{2\hbar}}\equiv e^{-\frac{i E_+ t}{\hbar}}\, ,
	\end{equation}
	with slightly non-real "energy" $E_+\equiv \epsilon-i\frac{\Gamma}{2}$. This can be computed using the euclidean path integral
	\begin{equation}
	\bra{x_+}\exp\left(-\frac{HT}{\hbar}\right)\ket{x_+} \cong\int_{x(0)=x_+}^{x(T)=x_+} \mathcal{D}x\,  e^{-\frac{S}{\hbar}}\, ,
	\end{equation}
	where the r.h. side is the euclidean path integral. Its detailed evaluation is discussed in Coleman's classic \textit{Aspects of Symmetry} \cite{coleman_1985} and reviewed also in Chapter $7$ of these notes.
	
	In the saddle point approximation, the euclidean path integral is evaluated by summing over well separated \textit{bounces} that are solutions to the euclidean equations of motion. In doing so, it is implicitly assumed that the kinetic energy that is released in the tunneling events plays no role, i.e. that it dissipates.
	\subsection{The procedure (see also Chapter $7$)}\label{sssec:procedure}
	We now turn back to the field theory case which is briefly summarized. For details we refer the reader to Chapter $7$ of these notes. Similar to the QM case, the leading non-perturbative contribution to the euclidean path-integral comes from a \textit{single} instanton, i.e. solutions to the euclidean equations of motion (not $\phi\equiv \phi_+$) that satisfy
	\begin{equation}
	\phi(\bar{x},x_4)\longrightarrow \phi_+\, ,
	\end{equation}
	in either limit $|x|\longrightarrow \infty$, $x_4\longrightarrow\pm\infty$. Such a solution is called a \textit{bounce}.
	
	The decay rate per volume is given by
	\begin{equation}
	-i\Gamma/V=K\, e^{-S_E}\, ,
	\end{equation}
	where the 'determinant factor' $K$ is proportional to $(\det'\frac{\delta^2 S}{\delta \phi^2}|_{\text{bounce}})^{-1/2}$, and $S_E$ is the euclidean bounce action.
	
	Note that in $\det'(...)$, four translational zero modes are excluded. For the action we consider there exists precisely one negative eigenvalue of $\frac{\delta^2 S}{\delta \phi^2}|_{\text{bounce}}$ \cite{coleman_1985}. This ensures that $\Gamma/V$ is real as needed for the interpretation as a decay rate.
	\subsection{The thin-wall approximation}\label{sssec:thinwall}
	We are interested in computing the action of the $\mathcal{O}(4)$-symmetric bounce\footnote{This is the bounce of minimal action \cite{Coleman:1977th}.} in the \textit{thin-wall approximation} which it is worthwhile to phrase out once more:
	
	We consider a potential $V(\phi)=V_+(\phi)+\mathcal{O}(\epsilon)$ where $V_+(\phi_+)=V_+(\phi_-)=0$, where the parameter $\epsilon$ is defined as the difference in the scalar potential of the two "vacua". The $\mathcal{O}(\epsilon)$ part of the scalar potential is itself function of $\phi$. Assuming $\mathcal{O}(4)$ symmetry, the equations of motion for $\phi$ become a second order differential equation in the radial coordinate $r$,
	\begin{equation}
	\phi''(r)+\frac{3}{r}\phi'(r)=V'(\phi)\, .
	\end{equation}
	Applying the thin-wall approximation means assuming that the solution can be split into three separated regions, an inside-region where $\phi\approx \phi_-$, an outside region with $\phi\approx \phi_+$ and a transition region (\textit{the domain wall}) of \textit{bubble thickness} $\mu^{-1}$ at radius $R\gg \mu^{-1}$. Inside the domain wall, the field profile can be calculated by neglecting the "viscous damping" term (second on the l.h. side) which is indeed justified if $\mu^{-1}$ is much smaller than the critical bubble radius $R$. The equation of motion becomes $\phi''(r)=V'(\phi)$ which is the equation of motion of a particle in one spatial dimension with a potential $-V(\phi)$.
	
	Neglecting the $\mathcal{O}(\epsilon)$ terms in the inside-region, the bubble radius $R_0$ and bounce action $S_0$ are easily determined to be
	\begin{equation}\label{result_nograv}
	R_0=\frac{3T}{\epsilon}\, ,\quad S_0=\frac{27\pi^2 T^4}{2\epsilon^3}\, ,
	\end{equation}
	in terms of the \textit{wall tension}
	\begin{equation}\label{eq:wall-tension}
	T=\int_{\phi_-}^{\phi_+}d\phi \sqrt{2V_+(\phi)}\, .
	\end{equation}
	\section[Turning on gravity or "the Coleman-de Luccia instanton"]{Turning on gravity or "the Coleman-de Luccia instanton" \cite{Coleman:1980aw}}\label{ssec:CdL}
	We are now ready to "turn on" gravity and obtain a first glimpse what the effects are. This chapter is essentially a review of the celebrated paper \cite{Coleman:1980aw} by S. Coleman and F. de Luccia.
	\subsection{The bounce \& its action}\label{sssec:bounceaction}
	We are interested in the theory of a single scalar field $\phi$ with potential $V(\phi)$ minimally coupled to gravity, with euclidean action
	\begin{equation}
	S_E[\phi,g]=\int d^4 x \sqrt{g}\left(-\frac{M_P^2}{2}R+\frac{1}{2}(\del \phi)^2+V(\phi)\right)\, .
	\end{equation}
	We still assume that the potential has two minima, but we no longer have the freedom to set $V(\phi_+)=0$ in general, and we define $\epsilon\equiv V(\phi_+)-V(\phi_-)$. This means in particular that in order to compute the decay rate (per volume) we have to replace the euclidean bounce action by 
	\begin{equation}\label{Bdef}
	B\equiv {S_E}|_{bounce}-S_E|_{\text{false vacuum}}\, .
	\end{equation}
	The most general $\mathcal{O}(4)$-symmetric metric ansatz is
	\begin{equation}
	ds^2=d\xi^2+r^2(\xi)d\Omega^2\, ,
	\end{equation}
	where $\xi$ is a radial coordinate, $d\Omega^2$ is the standard unit metric on $S^3$ and $r(\xi)$ is the (to be determined) radius of curvature of the angular three-sphere. Euclidean space is recovered for $r(\xi)=\xi$. The euclidean Einstein equations and scalar field equations of motion can be reduced to the two equations
	\begin{align}\label{eq.ofmotion}
	M_P^2R_{\mu\nu}=\del_{\mu}\phi\del_{\nu}\phi +g_{\mu\nu}V(\phi)\, ,\quad \rightarrow\quad &r'(\xi)^2=1+\frac{r^2}{3M_P^2}\left(\frac{1}{2}\phi'(\xi)^2-V\right)\, ,\\
	\nabla^2\phi\quad\equiv \quad&\phi''(\xi)+\frac{3r'(\xi)}{r(\xi)}\phi'(\xi)=\frac{\del V(\phi)}{\del \phi}\, .
	\end{align}
	The (on-shell) bounce action can be massaged as follows
	\begin{align}
	S_E&=2\pi^2\int d\xi \left[r^3\left(\frac{1}{2}\phi'^2+V\right)+3M_P^2(r^2r''+rr'^2-r)\right]\\
	&\overset{i.b.p.}{=}2\pi^2\int d\xi \left[r^3\left(\frac{1}{2}\phi'^2+V\right)-3M_P^2r(r'^2+1)\right]+boundary\\
	&\overset{e.o.m.}{=}-12\pi^2M_P^2\int d\xi\, r\left[1-\frac{r^2}{3M_P^2}V\right]+boundary\, .
	\end{align}
	We can neglect boundary terms because in \eqref{Bdef} we subtract the actions of two solutions that become identical at infinity.
	
	The result is that in the thin-wall approximation the equations of motion of the scalar field are not affected by gravitational effects since only the viscous damping term is altered which we neglect anyway (but the regime of validity will have to be readdressed). Hence, again neglecting the $\mathcal{O}(\epsilon)$ term in the potential, the bounce solution is given by 
	\begin{equation}
	\int_{\frac{1}{2}(\phi_+-\phi_-)}^{\phi}d\phi' \left[2(V_+(\phi')-V_+(\phi_-))\right]^{-\frac{1}{2}}=\xi-\xi_0\, ,
	\end{equation}
	where it is chosen that $\xi=\xi_0$ corresponds to the "middle" of the domain wall $\phi=\frac{\phi_++\phi_-}{2}$.
	
	Next, the Einstein equations require a choice of an integration constant which is chosen to be $r(\xi_0)=R$, in terms of the yet undetermined bubble radius $R$. The contributions to $B$ are again split into the three distinct regions. The outside and wall regions give
	\begin{equation}
	B_{outside}(R)\equiv 0\, ,\quad B_{wall}(R)=2\pi^2R^3 T\, ,
	\end{equation}
	as in the pure field theory case with tension again given by eq. \eqref{eq:wall-tension}. The inside region is readily shown to give
	\begin{align}
	B_{inside}(R)&=-12\pi^2M_P^2 \int_0^{\xi_0} d  \xi \, r \left[1-\frac{r^2}{3M_P^2}V(\phi_-)\right]-(\phi_-\longrightarrow\phi_+)\\
	&\overset{eq. \eqref{eq.ofmotion}}{=}-12\pi^2M_P^2\int_0^{R} dr\, r\left[1-\frac{r^2}{3M_P^2}V(\phi_-)\right]^{\frac{1}{2}}-(\phi_-\longrightarrow\phi_+)\\
	&=12\pi^2\frac{M_P^4}{V(\phi_-)}\left[\left(1-\frac{R^2}{3M_P^2}V(\phi_-)\right)^{\frac{3}{2}}-1\right]-(\phi_-\longrightarrow\phi_+)\, .
	\end{align}
	The bounce that minimizes $B(R)=2\pi^2R^3T+B_{inside}(R)$ is called a Coleman-de-Luccia (CdL) instanton. The euclidean four-geometry in the inside (outside) region is obtained by solving the $r(\xi)$ equation of motion which gives
	\begin{align}
	r(\xi)=\begin{cases}
	\Lambda^{-1}\sin(\Lambda (\xi-\xi_0)) & V>0\\
	\xi-\xi_0 & V=0\\
	\Lambda^{-1}\sinh(\Lambda (\xi-\xi_0)) & V<0
	\end{cases}\, ,\quad\text{with}\quad \Lambda\equiv \sqrt{\frac{|V|}{3M_P^2}}\, ,\\
	\end{align}
	where $V$ denotes the value of the potential in the true (false) vacuum.
	
	This is respectively (part of) the $4$-sphere, euclidean space, and hyperbolic space equipped with their "canonical" metrics. These are sewn together at the position of the bubble wall. Its analytic continuation to Minkowskian signature will be discussed momentarily\footnote{One may obtain all the upcoming results directly in Minkowskian signature by sewing together different maximally symmetric spaces along a domain wall of postulated tension $T$, by solving the Israel junction conditions \cite{Israel:1966rt}, without referring to its possible (but not necessary) resolution in terms of a scalar field profile \cite{Harlow:2010az}.}.
	
	The simplest examples are the following:
	\begin{enumerate}
		\item $V(\phi_+)=\epsilon$, $V(\phi_-)=0$, i.e. tunneling from a vacuum of positive vacuum energy ($dS_4$) to a Minkowski vacuum. 
		
		Minimizing $B(R)$ gives
		\begin{equation}
		R=\frac{12T}{4\epsilon+\frac{3T^2}{M_P^2}}=\frac{R_0}{1+\frac{1}{4}\left(H_{\epsilon} R_0\right)^2}\, ,
		\end{equation}
		where $H_{\epsilon}\equiv \left(\frac{\epsilon}{3M_P^2}\right)^{\frac{1}{2}}$ is the Hubble parameter of $dS_4$, and $R_0$ is the bubble radius in the limit of gravitational decoupling $M_P\longrightarrow\infty$ that was given in \eqref{result_nograv}. \footnote{Note that the maximum bubble radius in units of the Hubble radius is given by $H_{\epsilon}R_0=2$, where $H_{\epsilon}R=1$. In this limit, the euclidean $4$-geometry is that of a full $4$-sphere and the bubble wall collapses to the pole opposite to $r=0$. $HR>1$ is not possible as in that case the instanton would not "fit" into the euclidean $4$-sphere.}
		
		The corresponding bounce "action" is
		\begin{equation}
		B=\frac{S_0}{(1+\frac{1}{4}(H_{\epsilon}\, R_0)^2)^2}\leq S_0\, ,
		\end{equation}
		where $S_0$ is the bounce action of \eqref{result_nograv}. Clearly, gravitational effects make the decay more likely.
		\item $V(\phi_+)=0$, $V(\phi_-)=-\epsilon$, i.e. tunneling from a Minkowski vacuum to a vacuum of negative vacuum energy ("something like $AdS_4$"). In this case, the bubble radius is
		\begin{equation}
		R=\frac{12T}{4\epsilon-\frac{3T^2}{M_P^2}}=\frac{R_0}{1-\frac{1}{4}\left(\Lambda_{AdS} R_0\right)^2}\, ,
		\end{equation}
		and
		\begin{equation}
		B=\frac{S_0}{(1-\frac{1}{4}(\Lambda_{AdS} R_0)^2)^2}\geq S_0\, ,
		\end{equation}
		where $\Lambda_{AdS}\equiv \left(\frac{\epsilon}{3M_P^2}\right)^{\frac{1}{2}}$
		Here instead, we observe that the $B\geq S_0$ and the decay becomes less likely (we will get back to this issue in Section \ref{sssec:nodecay}).
	\end{enumerate}
	\subsection{Validity of the thin-wall approximation}\label{sssec:valthinwall}
	We have observed that in the thin-wall approximation the calculation is formally very similar to the field theory case, since the viscous damping term is simply dropped in the $\phi$ equations of motion. However, the realm of validity of the approximation has to be readdressed. By the $r$ equation of motion, the coefficient of the damping term reads
	\begin{equation}
	\left(\frac{r'(\xi)}{r}\right)^2=\underbrace{\frac{1}{r^2}}_{\text{as before}}+\underbrace{\frac{\frac{1}{2}\phi'^2-V(\phi)}{3M_P^2}}_{\leq \Lambda^2}\, .
	\end{equation}
	Therefore, the thin-wall approximation is good when the bubble thickness $\mu^{-1}$ is much smaller than both the bubble radius $R$, as well as $\Lambda^{-1}$. However, the dimensionless quantity $\Lambda R_0$ which controls the "importance of gravity" is unconstrained. This has important consequences, as we shall see.
	\subsection{Analytic continuation to Minkowskian signature}\label{sssec:ancont}
	In the preceeding section we have derived the euclidean $\mathcal{O}(4)$-symmetric instanton, so now we should analytically continue to Minkowskian signature. First, $\mathcal{O}(4)$-symmetry becomes $\mathcal{O}(1,3)$ symmetry in Minkowskian signature, i.e. outside the bubble the metric is given by
	\begin{equation}
	ds^2=d\xi^2+r(\xi)^2(d\Omega_S)^2\, ,
	\end{equation}
	where $d\Omega_S$ is the line element of a unit-hyperboloid, embedded in Minkowski space with space-like normal vector, i.e. the space $H_S=\{x\in \mathbb{R}^{1,3}|x^{\mu}x_{\mu}=1\}$ endowed with the induced metric,
	\begin{equation}
	(d\Omega_S)^2=-d\eta^2+\cosh^2(\eta)(d\Omega_2)^2\, ,
	\end{equation}
	and canonical (i.e. round) $S^2$-metric $(d\Omega_2)^2$.
	
	At $r(\xi)=0$ (inside the bubble), there is a coordinate singularity and we may analytically continue past the singularity by choosing $\xi=0$ at $r=0$ and continue to $\xi=i\tau$ with real $\tau$. The metric becomes
	\begin{equation}
	ds^2=-d\tau^2+(ir(i\tau))^2(d\Omega_T)^2\, ,
	\end{equation} 
	where $d\Omega_T$ is the induced metric on a unit-hyperboloid embedded in Minkowski space with time-like normal vector, i.e. $H_T=\{x\in \mathbb{R}^{1,3}|x^{\mu}x_{\mu}=-1\}$ with induced metric
	\begin{equation}
	(d\Omega_T)^2=dr^2+\sinh^2(r)(d\Omega_2)^2\, .
	\end{equation}
	This means that the inside region can be interpreted as a FRW universe of open type.
	
	For positive (constant) potential $V$, the analytic continuation produces
	\begin{align}
	ds^2|_{outside}&=d\xi^2+\Lambda^{-2}\sin^2(\Lambda\xi)(d\Omega_S)^2\, ,\\
	ds^2|_{inside}&=-d\tau^2+\Lambda^{-2}\sinh^2(\Lambda\tau)(d\Omega_T)^2\, ,
	\end{align}
	describing patches of four-dimensional de Sitter space ($dS_4$) with Hubble parameter $H=\Lambda$.
	
	For negative potential $V$, one obtains instead
	\begin{align}
	ds^2|_{outside}&=d\xi^2+\Lambda^{-2}\sinh^2(\Lambda\xi)(d\Omega_S)^2\, ,\\
	ds^2|_{inside}&=-d\tau^2+\Lambda^{-2}\sin^2(\Lambda\tau)(d\Omega_T)^2\, ,
	\end{align}
	which is the metric on patches of four-dimensional Anti de Sitter space ($AdS_4$). Naively, tunneling events with $V(\phi_-)<0$ are transitions to $AdS_4$. However, it has been argued by Coleman and de Luccia \cite{Coleman:1980aw}, as well as Coleman and Abbott \cite{ABBOTT1985170}, that tiny perturbations away from the thin-wall approximation turn the coordinate singularity of the inside $AdS_4$ at $\tau=0$ into a physical singularity. The inside space is thus expected to crunch within an $AdS$-time $t_{AdS}=1/\Lambda$.
	\subsection{Gravity can "stop" the decay}\label{sssec:nodecay}
	In the second example, the decay of the false vacuum with vanishing vacuum energy to the "true vacuum" with negative vacuum energy, one immediately observes that as $\epsilon^{\frac{1}{2}}\longrightarrow \frac{\sqrt{3}}{2M_P}T$ (from below), both the bubble radius $R$ and the bounce action $B$ diverge. In other words, if
	\begin{equation}\label{stability_bound}
	T\geq \frac{2M_P}{\sqrt{3}}(|V(\phi_-)|^{\frac{1}{2}}-|V(\phi_+)|^{\frac{1}{2}})\, ,
	\end{equation}
	the decay channel is completely frozen \cite{Coleman:1980aw,Harlow:2010az}, despite the existence of a vacuum with lower energy\footnote{We have written the formula such that it holds for arbitrary non-positive $V(\phi_{\pm})$}. This is to be contrasted with the case of a false $dS_4$ vacuum where the decay rate is always finite (if a vacuum with lower energy exists).
	
	Incidentally, the stability bound eq. \eqref{stability_bound} can be phrased as a bound on the charge-to-mass ratio of domain walls coupled to a three-form $A_3$ with charge (in Planck units) $q=\sqrt{2}(|V(\phi_-)|^{\frac{1}{2}}-|V(\phi_+)|^{\frac{1}{2}})$ \cite{Cvetic:1992st}. Indeed, for an action
	\begin{equation}
	S=\int_{M_4}*\frac{M_P^2}{2}R+ \frac{1}{2}dA_3\wedge *dA_3-q\int_{\text{dw}}A_3\, ,
	\end{equation}
	the (otherwise constant) value of $*dA_3$ changes by an amount $q$ across a domain wall, and hence one may identify the r.h. side of \eqref{stability_bound} with $\sqrt{\frac{2}{3}}q$. This is an important way to rephrase the bound since it has been conjectured that in such theories, if they can be embedded in a theory of quantum gravity, there should always exist a domain wall that satisfies the bound \textit{opposite} to \eqref{stability_bound}, i.e. 
	\begin{equation}\label{WGCbound}
	\exists\quad \text{domain wall with tension }T\text{ and charge }q\text{, s.t.}\quad T/q\lesssim 1\, .
	\end{equation}
	This is the weak-gravity-conjecture (WGC) \cite{ArkaniHamed:2006dz} applied to three-form potentials/domain walls \cite{Hebecker:2015zss}.
	
	If this is true, Anti-de-Sitter (AdS) space can be at most marginally stable. Moreover, it has been conjectured that the bound \eqref{WGCbound} can be saturated only in supersymmetric vacua of a supersymmetric theory \cite{Ooguri:2016pdq,Freivogel:2016qwc}. In that case, non-supersymmetric AdS space would always be unstable. This is particularly troublesome since there might not to exist the notion of a parametrically long-lived but only meta-stable $AdS$ space. It was argued in \cite{Harlow:2010az} that any observer is hit by \textit{any} domain wall that is nucleated \textit{anywhere} in the bulk in proper time smaller than $t_{death}=\frac{\pi}{2}\Lambda_{AdS}^{-1}$. The survival chance after proper time $t$ would be
	\begin{equation}
	P(t)=\exp\left(-2\pi\frac{\Gamma}{\Lambda_{AdS}^4}\int_0^{t\cdot \Lambda_{AdS}}dx\, \tan^2(x)\right)\, ,
	\end{equation}
	which indeed vanishes as $t\longrightarrow t_{death}$. For those "$AdS$-vacua" that are the outcome of a tunneling from a vacuum with higher energy, this does not matter much since these will crunch anyway in an $AdS$-time.
	
	As a final note, for those who are worried that the solutions obtained in the thin-wall approximation do not lift to full solutions of the equations of motion let us point out that there exist full analytic CdL solutions \cite{Dong:2011gx}.
	\section[The Hawking-Moss instanton]{The Hawking-Moss instanton \cite{Hawking:1981fz}}\label{ssec:HM}
	Next we turn to an alternative decay channel that is of interested (only) if the false vacuum is a $dS_4$ vacuum, as first introduced by Hawking and Moss \cite{Hawking:1981fz}. The reason why de Sitter vacua are special in this discussion is that one may assign an effective finite \textit{temperature} to de Sitter space,
	\begin{equation}\label{dStemp}
	T_H=\frac{H}{2\pi}\, ,
	\end{equation}
	which indicates that \textit{upward tunneling} from a vacuum with potential $V(\phi_1)$ to a vacuum (or critical point) with $V(\phi_2)>V(\phi_1)$ should be possible. Indeed, for constant potential $V$ the euclidean on shell action reads
	\begin{equation}
	S_E=-\int d^4 x \sqrt{g}\,V=-24\pi^2 \frac{M_P^4}{V}\, ,
	\end{equation} 
	so the difference in actions $B\equiv S[\phi=\phi_2]-S[\phi=\phi_1]$ that determines the decay rate from $\phi=\phi_1\longrightarrow \phi=\phi_2$ is given by
	\begin{equation}
	B=24\pi^2\left(\frac{M_P^4}{V(\phi_1)}-\frac{M_P^4}{V(\phi_2)}\right)\, .
	\end{equation}
	This instanton is called a \textit{Hawking-Moss} (HM) instanton.
	
	Note that the transition is frozen (i.e. $B\longrightarrow \infty$) in the limit $V(\phi_1)/M_P^4\longrightarrow 0$, where the initial vacuum has vanishing vacuum energy. This is as it should be because the temperature \eqref{dStemp} vanishes in this limit.
	
	This instanton is qualitatively very different from the CdL instanton since there does not exist a domain wall at all. Instead it seems as though the \textit{whole} $dS$ universe tunnels to a higher $dS$ critical point at once. More precisely, the analytic continuation of the $4$-sphere is a bit ambiguous, in the sense that already from a single static patch of $dS_4$ (which is the region accessible to a single observer) one can obtain all of a single four-sphere \cite{Anninos:2012qw}. 
	
	It seems more plausible to regard the HM instanton as a transition of the Hubble patch of an observer. In that way, the instanton looks like a horizon-scale upwards fluctuation of the scalar field $\phi$, qualitatively the same mechanism that can give rise to eternal inflation \cite{Linde:1986fc}.
	\section[And now for something completely different: "Creating a universe in a lab"]{And now for something completely different: "Creating a universe in a lab" \cite{Farhi:1986ty}}\label{ssec:FG}
	
	The amusing question raised by Farhi and Guth in \cite{Farhi:1986ty} is: \textit{Is it possible to create a bubble of false vacuum inside our "true" vacuum?} At the technical level, the task is to find an "inside-$dS_4$" that is sewn together with an "outside" Schwarzschild geometry along a domain wall (again a "thin" wall), by solving the Israel junction conditions \cite{Israel:1966rt}.
	\begin{figure}[ht]
		\includegraphics[keepaspectratio,width=\textwidth]{Figures/FG_Penrose}
		\caption{The result of the sewing process as depicted in \cite{Farhi:1989yr} ($\chi\equiv H$): The l.h. diagram is the Penrose diagram of $dS_4$, with the shaded area corresponding to the "universe in a lab" bounded by the domain wall (thick black line). The r.h. diagram is the Penrose diagram of Schwarzschild, now with shaded area denoting the "outside world" bounded by the domain wall. The nucleation point is a singularity.}
		\label{fig:FG}
	\end{figure}
	The Schwarzschild geometry in (gloabl) Kruskal-Szekeres coordinates reads
	\begin{equation}
	ds^2_{Schwarzschild}=32\frac{(GM)^3}{R}\exp\left(-\frac{R}{2GM}\right)(-dV_s^2+dU_s^2)+R^2d\Omega_2^2\, ,
	\end{equation}
	with black-hole mass $M$, and function $R(U_s,V_s)$ that satisfies
	\begin{equation}
	\left(\frac{R}{2GM}-1\right)\exp\left(\frac{R}{2GM}\right)=U_s^2-V_s^2\, .
	\end{equation}
	The $dS_4$ metric in Gibbons-Hawking coordinates is given by
	\begin{equation}
	ds^2=H^{-2}(1+H R)^2(-dV_d^2+dU_d^2)+R^2d\Omega_2^2\, ,
	\end{equation} 
	with Hubble parameter $H$, and function $R(U_d,V_d)$ that satisfies
	\begin{equation}
	\frac{1-HR}{1+HR}=U_d^2-V_d^2\, .
	\end{equation}
	The result of this "sewing process" is not simple to write down explicitly but is depicted in Figure \ref{fig:FG} \cite{Farhi:1989yr}: The nucleation point of the lab-universe is an initial singularity, which is shown to be unavoidable without including a fluid that violates the null-energy condition (NEC). It is however possible to write down a solution with lab-universe that is nucleated, grows to a maximum size, and re-collapses without the need for an initial singularity. In reference \cite{Farhi:1989yr} it is investigated whether such a bubble could then tunnel towards a state of ever increasing size of the lab-universe. 
	
	Reassuringly, all such proposed solutions would grow inside the Schwarzschild radius of the parent universe and thus never "eat up" our vacuum.
	
	This concludes our introduction to gravitational effects in tunneling processes. 
% 	\bibliography{CdL}
% 	\bibliographystyle{JHEP}
% \end{document}
