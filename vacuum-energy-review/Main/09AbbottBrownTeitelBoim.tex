%!TEX root = ../VacuumEnergy.tex

\chapter{Cosmological relaxation \`a la Abbott and Brown-Teitelboim}
\label{cha:Abbott}


\section{The idea: a stepwise adjustment of the Cosmological Constant}

In this section, we review two proposals of dynamical adjustment of the CC: the Abbot mechanism \cite{Abbott:1984qf} and the Brown-Teitelboim mechanism \cite{Brown:1987dd}, \cite{Brown:1988kg}. They both suppose the existence of a compensating field whose vacuum energy contribution $\lambda_{CF}$ dynamically adjusts itself in order to cancel the large contribution coming from the conventional particle physics $\lambda_{bare}$. The idea is that the "observable vacuum energy" $\Lambda_{obs} = \lambda_{bare} + \lambda_{CF}$ as represented in Fig.\eqref{weinberg_window_CCadjustment}, decreases step by step until having a value compatible with the existence of observers, inside the Weinberg window 
\begin{equation}
-10^{-122} \lesssim \Lambda \lesssim 10^{-120}, \qquad [\text{Planck unit}]
\label{weinberg_window}
\end{equation}
the value mesured with current observations\cite{Agashe:2014kda} being $\Lambda_{obs}^{0} = 1.19 \pm 0.06 \times 10^{-123}$. \\
The lower bound prevents the Universe to undergo a Big Crunch before the galaxy formation \cite{Kallosh:2002gg} while the upper bound cancels the possibility to have a CC domination before the galaxy formation \cite{Weinberg:1987dv}. \\

\begin{figure}[htp]
\centering
\resizebox{0.8\textwidth}{!}{\includegraphics[width=1\textwidth]{Figures/weinberg_window_CCadjustment.pdf}}
\caption{Dynamical adjustment of the CC to a value compatible with the existence of observers.}
\label{weinberg_window_CCadjustment} 
\end{figure}

We immediately warn the reader that, for the same reason as eternal inflation \cite{Vilenkin:1983xq}, both Abbott and BT mechanisms naturally give rise to a multiverse with different vacuum energy for each of its patches. In this framework, our Universe would be one of the patches of the multiverse with a vacuum energy inside the Weinberg window. In this section, we do not consider $\Lambda = 0$ as an attractor but instead we will insist on the two main deficiencies of the proposals, following \cite{Bousso:2007gp}.
\begin{enumerate}
\item
One can judge the applicability of the adjustment mechanism by its capacity to generate a spectrum of CC which contains the Weinberg window. By looking at Fig.\eqref{weinberg_window_CCadjustment}, we can see that we must impose the step-size to be smaller than the width of the Weinberg window
\begin{equation}
\label{step-size_condition_gen}
|\Delta \Lambda| \lesssim 10^{-120}.
\end{equation}
Then a microphysics parameter has to be unnatural. This is called the Step-Size Problem.
\item
We will see that the step decrease of the CC is performed by tunneling of the compensating field with a rate exponentially suppressed when we reach the Weinberg window. Then, the adjustment time scale is very large. Since the Universe is in expansion during the process, it becomes largely diluted and empty. This is called the Empty Universe Problem.
\end{enumerate}

\section{The Abbott mechanism}
We just sketch the results from \cite{Abbott:1984qf}.
\subsection{Framework}

We start with a scalar field $\phi$ whose couplings are restricted by the shift symmetry 
\begin{equation}
\label{shift_sym}
\phi \rightarrow \phi + \text{constant}
\end{equation}
The potential for $\phi$ arises from breaking the shift symmetry in two ways
\begin{enumerate}
\item
First, we couple the scalar field $\phi$ to a dark gauge sector
\begin{equation}
L_{int} = \frac{\alpha_{D}}{4\pi}\frac{\phi}{f_{\phi}}\epsilon^{\mu\mu\alpha\beta}Tr\left[ F_{\mu\nu}F_{\alpha\beta} \right]
\end{equation}
where $\alpha_{D}$ is the coupling constant of the hidden gauge theory and $f_{\phi}$ is a mass scale fixed by the UV, of order $1$ in Planck unit. \\
Since $\epsilon^{\mu\mu\alpha\beta}Tr\left[ F_{\mu\nu}F_{\alpha\beta} \right]$ is a total derivative, the shift symmetry seems to be preserved up to surface terms. However, these surface terms generate instantons which break the shift symmetry in Eq.\eqref{shift_sym}. This leads to a potential for the scalar field \cite{Peccei:1977hh}
\begin{equation}
V_{1}(\phi) = -\Lambda_{D}^{4} \cos{(\frac{\phi}{f_{\phi}})}
\end{equation}
where $\Lambda_{D}$ is a mass scale of the dark gauge theory. \\
However the following symmetry are still respected
\begin{align}
\label{second_sym}
&\phi \rightarrow \phi + 2\pi f_{\phi}, \\
\label{second_sym_2}
&\phi \rightarrow -\phi
\end{align}
\item 
Second, we softly break the remaining symmetries in Eq.\eqref{second_sym} by adding
\begin{equation}
V_{2}(\phi) = \frac{\epsilon\phi}{f_{\phi}}
\end{equation}
\end{enumerate}

We get the total vacuum energy
\begin{equation}
\Lambda(\phi) = \underbrace{\frac{\epsilon \phi}{f_{\phi}} - \Lambda_{D}^{4}\cos{\frac{\phi}{f_{\phi}}}}_\textrm{compensating sector} + \lambda_{bare}
\end{equation}
$\Lambda_{D}$ and $\epsilon$ can be naturally small because since they break the symmetries in Eq.\eqref{shift_sym} and in Eq.\eqref{second_sym}, their radiative corrections are proportional to themselves. \\
If $\epsilon \ll \Lambda_{D}^{4}$, as shown in Fig.\eqref{abbott_potential}, the potential $\Lambda(\phi)$ has local minima at $\phi = 2\pi N f_{\phi}$ with energy densities $\Lambda_{N} = 2\pi N \epsilon - \Lambda_{D}^{4} + \lambda_{bare}$.

\begin{figure}[htp]
\centering
\resizebox{0.8\textwidth}{!}{\includegraphics[width=1\textwidth]{Figures/abbott_potential_v2.pdf}}
\caption{Dynamical adjustment of the CC to a value compatible with the existence of observers.}
\label{abbott_potential} 
\end{figure}

\subsection{Condition on the step-size}

The Abbott mechanism is able to predict a universe with observers if at least one value of $\Lambda_{N}$ is inside the Weinberg window. As already expressed in Eq.\eqref{step-size_condition_gen}, this imposes
\begin{equation}
\Delta \Lambda = 2\pi\epsilon < 10^{-120} \sim (0.01\; eV)^{4}.
\end{equation}
In order for the model to be relevant, the parameter $\epsilon$ has to be extremelly unnatural. This is the Step-Size Problem. However, since $\epsilon$ comes from a hidden sector and that it is protected against radiative corrections thanks to symmetries in Eq.[\ref{second_sym}, \ref{second_sym_2}], we can still debate what does naturalness really mean in that case.

\subsection{Dynamic: The descent of the potential}

\begin{enumerate}
\item
We start with a large vacuum energy and then with a large de Sitter temperature $T = H/2\pi \sim \sqrt{\Lambda_{N}}/M_{p}$ with $M_{p}$ the Planck mass. As long as the temperature is larger than the cosine barrier, $T > \Lambda_{D}$, quantum fluctuations make the scalar field $\phi$ jump over the barrier. Besides the instantons which produce the cosine term are suppressed by finite-temperature effect. Then, as shown in Fig.\eqref{abbott_potential} the effective potential taking into account finite-temperature effects is barrier-less and the motion of the scalar field is a combination of quantum fluctuations and classical roll.
\item
When $T<\Lambda_{D}$, the cosine barrier shows up and freezes the scalar field in its minima. Then the scalar field tunnels from a minimum to the other with a rate $\Gamma \approx e^{-B}$. The step-size $\epsilon$ being small, the thin-wall approximation is valid and near the Weinberg window we can use formula (Ref.\cite{Coleman:1980aw} and Jakob Moritz section)
\begin{equation}
B = \frac{B_{0}}{(1 + (\frac{R_i}{2R_{s}})^2)^{2}}
\end{equation}
with 
\begin{align}
&B_{0} = \frac{27\pi^{2}S_{1}^{4}}{2e^3} \\
&R_{i} = \frac{3 S_{1}}{e} \\
&R_{s} = M_{p} \sqrt{\frac{3}{8\pi e}} \\
&S_{1} = \int_{\phi_{N-1}}^{\phi_{N}} d\phi \left[ (V(\phi) - V(\phi_{N+1})) \right]^{1/2} = 8 f_{\phi} \Lambda_{D}^{2} \\
&e = 2\pi\epsilon
\end{align}
We check that the small $\epsilon$ limit is compatible with the limit where gravitational effects are large
\begin{equation}
R_{i} \gg 2 R_{s} \qquad \Leftrightarrow \qquad \epsilon \ll 10^{3} \left(\frac{f_{\phi}}{M_{p}}\right)^{2} \Lambda_{D}^{4}.
\end{equation}
Then, we get
\begin{equation}
B = \frac{3}{8}\frac{M_{p}^{4}}{2\pi\epsilon} \sim 10^{120}.
\end{equation}
Near the Weinberg window, the lifetime becomes extremelly long and the Universe undergoes very long de Sitter expansion phases which strongly dilute its content. Finally when the CC is adjusted to zero, the Universe is empty. This is the Empty Universe Problem.

\end{enumerate}


\section{The Brown and Teitelboim mechanism}

As explained in Ref.\cite{Brown:1987dd} and \cite{Brown:1988kg}, this is the analogue in $(3+1)$-D of the Schwinger process in $(1+1)$-D ($e^{-}/e^{+}$ pair creation).
\subsection[The Schwinger process in (1+1)-D]{The Schwinger process in $(1+1)$-D}

Let's consider two oppositely charged, parallel and infinite plates as in Fig.\eqref{schwinger_process}. In presence of the electric field, a $e^{-}/e^{+}$ pair can tunnel out of the vacuum and discharge the plates by one unit of charge. Then the eletric field is also decreased by one unit of charge. 
\begin{figure}[htp]
\centering
\resizebox{0.8\textwidth}{!}{\includegraphics[width=1\textwidth]{Figures/schwinger_process.pdf}}
\caption{A $e^{-}/e^{+}$ pair tunnels out of the vacuum inside two oppositely charged, parallel and infinite plates. After the pair creation, the electric field is decreased by one unit of charge.}
\label{schwinger_process} 
\end{figure}

The description using plates in $(2+1)$-D, borrowed from \cite{Bousso:2007gp}, has only a pedagogical purpose here. The Schwinger process we are really interested in, is the one occuring in $(1+1)$-D, so with only one space dimension as shown in Fig.\eqref{schwinger_process_1D}. 
\begin{figure}[htp]
\centering
\begin{tikzpicture}
  \node (A) at (-5,0)   {$E_{o}=ne$};
  \node (A0) at (-2,0)   {$\bullet$};
  \node (A1) at (-1.8,0)   {};
  \node (A2) at (-3.5,0)  {};
  \node (B) at (0,0)    {$E_{i}=(n-1)e$};
  \node (B0) at (2,0)   {$\bullet$};
  \node (B1) at (1.8,0)   {};
  \node (B2) at (3.5,0)   {};
  \node (C) at (5,0)    {$E_{o}-ne$};
  \draw [thick,->] (A1) -- (A2) node[midway,sloped,above] {$e^{+}$};
  \draw [thick,->] (B1) -- (B2) node[midway,sloped,above] {$e^{-}$};
\end{tikzpicture} 
\caption{Instantaneous picture ($t$ fixed) of the Schwinger process in $(1+1)$-D. After the pair creation, the electric field inside the pair $E_{i}$ is decreased by one unit compared to the electric field outside $E_{0}$.}
\label{schwinger_process_1D} 
\end{figure}

There are two important differences between the Schwinger process occuring inside plates in $(2+1)$-D and the one taking place in $(1+1)$-D
\begin{enumerate}
\item
The reduction of the electric field by one unit $E_{o}-E_{i}=e$ is not due to the $e^{-}/e^{+}$ discharging the plates but really occurs inside the $e^{-}/e^{+}$ pair and is intrinsic to the unidimensionality of the process. The electric field jumps by one unit when crossing the wordline of a charge particle \cite{Brown:1988kg}.
\item
The quantization of the electric field $E=ne$ is not due to the presence of plates but it is a condition that we impose.
\end{enumerate}

The electric field being constant inside and outside the $e^{-}/e^{+}$ pair, we can write its tensor strenght as
\begin{equation}
F^{\mu\nu} = E \epsilon^{\mu\nu}
\end{equation}
with $\epsilon^{\mu\nu}$ the antisymetric tensor.\\
Its stress tensor 
\begin{align}
T_{\mu\nu} &= F_{\mu\alpha}F^{\alpha}_{\nu} - \frac{1}{4}(F_{\mu\nu}F^{\mu\nu})g_{\mu\nu} \\
&= g_{\mu\nu}\frac{E^{2}}{2}
\end{align}
is proportional to the Lorentz metric $g_{\mu\nu}$ so it is a direct contribution to the vacuum energy with an energy density
\begin{equation}
\rho = \frac{n^{2}e^{2}}{2}.
\end{equation}
Therefore, the total vacuum energy is
\begin{align}
\Lambda_{o} &= \lambda_{bare} + \frac{n^{2}e^{2}}{2} \\
\Lambda_{i} &= \lambda_{bare} + \frac{(n-1)^{2}e^{2}}{2} \\
&= \Lambda_{o} - \Delta\Lambda
\end{align} 
where 
\begin{equation}
\Delta\Lambda = \left( n-\frac{1}{2} \right)e^{2}
\end{equation}
is the step-size of the adjustment mechanism as represented on Fig.\eqref{weinberg_window_CCadjustment}


\subsection[From the (1+1)-D Schwinger process to the BT mechanism]{From the $(1+1)$-D Schwinger process to the BT mechanism}


The Brown-Teitelboim mechanism is the analogue in $(3+1)$-D of the Schwinger process in $(1+1)$-D. The $2$-form $F^{\mu\nu}$ sourced by a point particle becomes a $4$-form $F^{\mu\nu\alpha\beta}$ sourced by a $2$-dimensional membrane, a $2$-brane. The creation of a $e^{-}/e^{+}$ pair is mapped to the nucleation of a $2$-brane. The analogy between the two processes is presented in the table below.
\newline\newline
\resizebox{1\textwidth}{!}{
\begin{tabular}{l|c|c|}
  \cline{2-3}
  & Schwinger $(1+1)$-D & BT $(3+1)$-D  \\
  \hline
  \multicolumn{1}{ |c|  }{Strenght tensors} & $2$-form $F^{\mu\nu}$ & $4$-form $F^{\mu\nu\alpha\beta}$ \\
  \hline
  \multicolumn{1}{ |c|  }{which derive from gauge fields} & $1$-form $A^{\mu}$ & $3$-form $A^{\mu\nu\alpha}$ \\
  \hline
  \multicolumn{1}{ |c|  }{which are sourced by}  &  point particle of charge $e$ & $2$-brane of charge $q$  \\
  \hline
  \multicolumn{1}{ |c|  }{Tunneling process} & pair creation & $2$-brane bubble nucleation \\
  \hline
  \multicolumn{1}{ |c|  }{Energy density} & $\rho = n^{2}e^{2}/2$ &  $\rho = n^{2}q^{2}/2$ \\
  \hline
   \multicolumn{1}{ |c|  }{\raisebox{40pt}{Topology}} 
   &
	\raisebox{40pt}{
 \begin{tikzpicture}
  \node (A) at (-3,0)   {$\Lambda_{0}$};
  \node (A0) at (-1,0)   {$\bullet$};
  \node (A1) at (-0.8,0)   {};
  \node (A2) at (-2.5,0)  {};
  \node (B) at (0,0)    {$\Lambda_{i}$};
  \node (B0) at (1,0)   {$\bullet$};
  \node (B1) at (0.8,0)   {};
  \node (B2) at (2.5,0)   {};
  \node (C) at (3,0)    {$\Lambda_{0}$};
  \draw [thick,->] (A1) -- (A2) node[midway,sloped,above] {$e^{+}$};
  \draw [thick,->] (B1) -- (B2) node[midway,sloped,above] {$e^{-}$};
\end{tikzpicture}
}
  &
  \begin{tikzpicture}
  \shade[ball color = gray!40, opacity = 0.1] (0,0) circle (2cm);
  \draw (0,0) circle (2cm);
  \draw (-2,0) arc (180:360:2 and 0.6);
  \draw[dashed] (2,0) arc (0:180:2 and 0.6);
  \node at (0,0)    {$\Lambda_{i}$};
  \node at (-3,0)    {$\Lambda_{0}$};
  \node at (3,0)    {$\Lambda_{0}$};
\end{tikzpicture}
 \\
 \hline
 \multicolumn{1}{ |c|  }{Step-size} &  $\Lambda_{i} = \Lambda_{o} - \Delta\Lambda$ with $\Delta\Lambda = \left( n-\frac{1}{2} \right)e^{2}$ &  $\Lambda_{i} = \Lambda_{o} - \Delta\Lambda$ with $\Delta\Lambda = \left( n-\frac{1}{2} \right)q^{2}$ \\
  \hline
 
\end{tabular}
}
\newline\newline\newline
As for the $2$-form field, in the absence of charge the $4$-form field is constant 
\begin{equation}
F^{\mu\nu\alpha\beta} = E \epsilon^{\mu\nu\alpha\beta}
\end{equation} 
where 
\begin{equation}
E=nq.
\end{equation}
This last condition which was imposed by hand in the $(1+1)$-D case, comes now from consistency in string theory analogue to the Dirac quantization as explained in Ref.\cite{Bousso:2000xa}. \\
As for the $2$-form field, the $4$-form field directly contributes to the vacuum energy through its energy density
\begin{equation}
\rho = \frac{n^{2}q^{2}}{2}.
\end{equation}
After each $2$-brane nucleation, the vacuum energy in the bubble is decreased by 
\begin{equation}
\Delta\Lambda = \left( n-\frac{1}{2} \right)q^{2}.
\end{equation}
So after many bubble nucleations, we expect the vacuum energy to reach small values and to possibly enter in the Weinberg window in Eq.\eqref{weinberg_window}.

\subsection{Ability to solve the CC problem}

\begin{itemize}
\item
Since the $2$-brane nucleation can only decrease the vacuum energy (but not increase), we have to start with a positive bare vacuum energy $\lambda_{bare}$.
\item
In order for the mechanism to predict at least one value of the vacuum energy inside the Weinberg window, we have to impose 
\begin{equation}
\Delta\Lambda = (n_{*}-\frac{1}{2})q^{2} < 10^{-120}
\end{equation}
with $n_{*}$ being such that
\begin{equation}
\lambda_{bare} + \frac{n_{*}^{2} q^{2}}{2} = 0 \quad \rightarrow \quad n_{*} = \sqrt{\frac{2|\lambda_{bare}|}{q^{2}}}.
\end{equation}
We get 
\begin{equation}
q < \frac{10^{-120}}{|\lambda_{bare}|^{1/2}}.
\label{upper_bound_q_1flux}
\end{equation}
Even if we take the smalless value that we can predict for $|\lambda_{bare}|$, namely the value predicted by $SUSY$, $\lambda_{bare} = \lambda_{SUSY} = 10^{-65}$, the $2$-brane charge has to be extremely small
\begin{equation}
q < 10^{-88}.
\end{equation}
This is the Step-Size Problem.
\item
As for the Abbott mechanism, we can show that the $2$-brane nucleation rate is exponentially suppressed when we reach a vanishing vacuum energy (Refs.\cite{Brown:1987dd} and \cite{Brown:1988kg}) and then we also have to deal with the Empty Universe Problem.
\end{itemize}


\section{Bousso-Polchinski approach}

In their proposal \cite{Bousso:2000xa}, Bousso and Polchinski introduce multiple $4$-form fields in order to solve the Step-Size Problem and the Empty Universe Problem.

\subsection[Multiple 4-form fields]{Multiple $4$-form fields}

Let's introduce $J$ $4$-form fields:
\begin{equation}
\label{definition_ni}
F_{i}^{abcd} = n_{i}q_{i}\epsilon^{abcd}, \qquad i\in[1..J].
\end{equation}
Then the total vacuum energy is
\begin{equation}
\Lambda = \lambda_{bare} +\frac{1}{2}\sum^{J}_{i=1}n_{i}^{2}q_{i}^{2}.
\end{equation}
The proposal is relevant if it exists a configuration $\{n_{i}\}$ which gives a vacuum energy in the Weinberg window, i.e. if
\begin{align}
&0 < \lambda_{bare} + \frac{1}{2}\sum_{i=1}^{J} n_{i}^{2}q_{i}^{2} < \Delta\Lambda = 10^{-120} \\
\Longrightarrow \quad &2|\lambda_{bare}| < \sum_{i=1}^{J} n_{i}^{2} q_{i}^{2} < 2\left(|\lambda_{bare}|+\Delta|\Lambda|\right)
\end{align}
Let's consider a $J$-dimensional grid with axes $n_{i}q_{i}$ where each point of the grid is a configuration $\{n_{i}\}$ as shown in Fig.\eqref{Jdim_grid_ww}. The last inequality means that a configuration $\{n_{i}\}$ leads to a vacuum energy inside the Weinberg window if the corresponding point on the grid sits inside a shell of radius 
\begin{equation}
r=\sqrt{2|\lambda_{bare}|}
\label{ww_radius}
\end{equation}
and of thickness 
\begin{equation}
\Delta r = \frac{\Delta\Lambda}{\sqrt{2|\lambda_{bare}|}}. 
\label{ww_thickness}
\end{equation}
The number of configurations $\{n_{i}\}$ which are inside the Weinberg shell is the volume of the shell divided by the volume of an unit cell of the grid
\begin{equation}
\#_{conf} = \frac{\omega_{J-1} \; r^{J-1} \Delta r}{\prod_{i=1}^{J} q_{i}} 
\end{equation}
where $\omega_{J-1} = \frac{2\pi^{J/2}}{\Gamma(J/2)}$. \\
At least one configuration $\{n_{i}\}$ leads to a Universe with observers if
\begin{align}
&\#_{conf} > 1 \\
\Longrightarrow \quad & \prod_{i=1}^{J} q_{i} \; < \; \omega_{J-1} \; |2\lambda_{bare}|^{\frac{J}{2}-1} \Delta \Lambda
\label{weinberg_window_shell_grid_inequality}
\end{align}

\begin{figure}[htp]
\centering
\resizebox{0.8\textwidth}{!}{\includegraphics[width=1\textwidth]{Figures/Jdim_grid_ww.pdf}}
\caption{Each set of integers $\{n_{i}\}$ can be visualized as a point of a $J$-dimensional grid. The configuration $\{n_{i}\}$ corresponds to a vacuum energy inside the Weinberg window if the point sits inside the Weinberg shell.}
\label{Jdim_grid_ww} 
\end{figure}

\subsection{Step-Size Problem treatment}
Assuming all the $2$-brane charges $q_{i}$ to be of the same order of magnitude, the Eq.\eqref{weinberg_window_shell_grid_inequality} leads to the following upper bound 
\begin{equation}
q_{i}  \; < \;  \left( \omega_{J-1} \; |2\lambda_{bare}|^{\frac{J}{2}-1} \Delta\Lambda \right) ^{\frac{1}{J}}.
\label{upper_bound_q_Jfluxes}
\end{equation}
The values of $q$ which saturate the last inequality are plotted in Fig.\eqref{plot_upper_bound_q_vs_J}.  We can see that $q_{max}$ increases quickly with the number of fluxes $J$ and then follows a plateau with a natural value for $\lambda_{bare}=1$ but with an unnatural value for $\lambda_{bare}=\lambda_{SUSY}$.
Indeed for $J=100$\footnote{In the framework of M-theory compactification, a large value for $J$ is actually welcome \cite{Bousso:2000xa}.} and $|\lambda_{bare}| = |\lambda_{SUSY}| = 10^{-65}$, we get 
\begin{equation}
q_{i} < 10^{-32}
\end{equation}
which is still very small. But if we take  $|\lambda_{bare}|=1$, then the $2$-brane charges can now have natural values
\begin{equation}
\label{q_upper_bound}
q_{i} < 0.5
\end{equation} 
and the Step-Size Problem is cured.

\begin{figure}[htp]
\centering
\resizebox{0.8\textwidth}{!}{\includegraphics[width=1\textwidth]{Figures/charge_q_flux_number_J.pdf}}
\caption{Maximum values of the $2$-brane charge $q$ for having a least one configuration of fluxes $\{n_{i}\}$ which gives a vacuum energy inside the Weinberg window $\Delta \Lambda < 10^{-120}$.}
\label{plot_upper_bound_q_vs_J} 
\end{figure}

While for $J=1$ a small value for $\lambda_{bare}$ was welcome (recall Eq.\eqref{upper_bound_q_1flux}), we can see in Eq.\eqref{upper_bound_q_Jfluxes} that for $J>2$, a large value of $\lambda_{bare}$ is welcome in order to have the upper bound on $q$ as large as possible. We can understand this difference of behaviour by writing Eq.\eqref{weinberg_window_shell_grid_inequality} as 
\begin{equation}
\prod_{i=1}^{J} q_{i} \; < \;  \omega_{J-1} \; r^{J-1} \Delta r
\label{ww_inequality_surface_term}
\end{equation}
where the radius $r$ and the thickness $\Delta r$ of the "Weinberg shell" are defined in Eq.\eqref{ww_radius} and Eq.\eqref{ww_thickness}.
When the bare vacuum energy grows $|\lambda_{bare}|\nearrow$, the surface term grows $r^{J-1}\nearrow$ while the shell becomes thinner $\Delta r \searrow$. The ingenuity of adding many $4$-form fields is that it increases the exponent of the surface term $r^{J-1}$ in Eq.\eqref{ww_inequality_surface_term} which triumphs over the shell thickness $\Delta r$ for $J>2$. Then the larger the bare vacuum energy $\lambda_{bare}$, the larger the number of configurations inside the Weinberg window.

\subsection{Membrane nucleations naturally lead to a multiverse}

When a membrane nucleates, the vacuum energy changes only inside the membrane bubble. Even though the bubble of new vacuum expands nearly at the speed of light, it is not fast enough to catch up with the expansion of the Universe. Then the whole Universe is never converted into the new vacuum and there is always some of the old vacuum left as shown in Fig.\eqref{bubble_cascade}. We say that the bubbles do not percolate \cite{Guth:1982pn}. \\
\begin{figure}[htp]
\centering
\resizebox{0.8\textwidth}{!}{\includegraphics[width=1\textwidth]{Figures/bubble_cascade.pdf}}
\caption{Cascades of bubbles.}
\label{bubble_cascade} 
\end{figure}
Starting from the old vacuum configuration, e.g. the red point in Fig.\eqref{Jdim_grid_ww}, successive bubble nucleations correspond to a brownian walk on the $J$-dimenional grid.
There are infinitely many cascades of bubbles, each one corresponding to a different path on the grid and to a different causal patch of a multiverse. Universe with observers are the remnant of cascades of bubbles corresponding to a path on the $J$-dimensional grid ending inside the Weinberg shell, as the one shown in Fig.\eqref{Jdim_grid_ww}.


\subsection{Empty Universe Problem treatment}

When the vacuum energy enters the Weinberg window, after the last membrane bubble nucleates, the Universe (understood as the interior of a cascade of bubbles) has undergone successive long period of expansion and then is empty (understood as strongly diluted). Let's now explain how adding many $4$-form fields can prevent the general mechanism of inflation followed by reheating to start until the last membrane nucleates. Then the reheating fills the Universe with radiation only after the vacuum energy is ajusted. \\
The vacuum energy just before the last bubble nucleation is
\begin{equation}
\Lambda_{last} = (n-\frac{1}{2})q^{2}.
\end{equation}
As shown in Fig.\eqref{inflation_potential}, the dynamic of the inflaton $\phi$ is a competition between the classical roll $\Delta \phi$
\begin{equation}
\Delta \phi = \frac{1}{2} V'(\phi) (\Delta t)^{2}.
\end{equation}
and the quantum fluctuation $\delta \phi$ which during a Hubble time $H^{-1}$ is equal to the de Sitter temperature $T=H/2\pi$
\begin{equation}
\delta \phi = \frac{H}{2\pi}
\end{equation}
with $H^{2} = 8\pi \Lambda_{last}/3M_{p}^{2} \sim q^{2}$. \\
For $J=100$ and $\lambda_{bare} \sim 1$ and assuming $q$ close to its upper bound in Eq.\eqref{q_upper_bound}, $\delta \phi$ is large (of order 1) and dominates over $\Delta \phi$ before the last membrane nucleates (we check it more carefully just below). The large de Sitter temperature gives a large thermal distribution to the inflaton and prevents it to roll classically. When the last membrane nucleates, the vacuum energy jumps from a value of order $1$ to less than $10^{-120}$ and the inflation can start, followed by reheating.\\

\begin{figure}[htp]
\centering
\resizebox{0.8\textwidth}{!}{\includegraphics[width=1\textwidth]{Figures/inflation_potential.pdf}}
\caption{Inflation potential.}
\label{inflation_potential} 
\end{figure}


\textbf{Question:} How much have the $2$-branes charges $q$ to be large in order to provide enough inflation ? \\
In order to prevent the inflation to start, the random motion of the inflaton $\delta \phi$ has to dominate over its classical decrease $\Delta \phi$ 
\begin{equation}
\label{inequality_delta_phi}
\delta \phi > \Delta \phi.
\end{equation}
Forgetting about numeric factors, this leads to 
\begin{equation}
\Lambda_{last}^{3/2} M_{p}^{-3} > V'(\phi)
\end{equation}
with $\Lambda_{last} \approx q^{2}$. \\
When the last membrane nucleates, quantum fluctuations $\delta \phi$ jump to zero and the inflation starts. But if the charges $q$ are too small, the condition Eq.\eqref{inequality_delta_phi} can be lost before the Weinberg window is reached. In that case, the inflation starts at $\phi_{start}$ such that
\begin{equation}
\label{equality_delta_phi}
\delta \phi|_{\phi_{start}} = \Delta \phi|_{\phi_{start}}.
\end{equation}
We can compute the minimal value of $q$ for having at least $60$ e-fold of inflation, with a quadratic inflaton potential\footnote{The value $10^{-12}$ in the potential for the inflaton is fixed in order to predict the amplitude of scalar fluctuations $\Delta_{s}^{2}$ equal to the mesured value $10^{-9}$, for $60$ e-fold \cite{Baumann:2009ds}.}.
\begin{align}
&V(\phi)=10^{-12} M_{p} \frac{\phi^{2}}{2}, \\
&\phi* \equiv 60 \text{ e-folds} = 15 M_{p}.
\end{align}
We must have
\begin{equation}
\phi_{start} > \phi* \qquad \Rightarrow \qquad \Lambda_{last}^{3/2} M_{p}^{-3} > 10^{-10}M_{p}^{3}
\end{equation}
which yields to
\begin{equation}
q > 10^{-3}.
\end{equation}
In order to provide at least $60$ e-fold of inflation, the $2$-brane charges have to be larger than $10^{-3}$ which is compatible with the upper limit in Eq.\eqref{q_upper_bound}.

\section{Conclusion}

We reviewed two old mechanisms of dynamic adjustment where the CC decreases step by step, after quantum tunneling of a scalar field or a $2$-brane bubble. They suffer from two big difficulties. First, in order to predict an almost vanishing vacuum energy, the step-size of the dynamic has also to be vanishing. Second, since the step-size is very small, the tunneling rate is very large therefore the Universe expands during a long time et becomes empty. A more recent proposal trades the unnaturalness of the step-size for a large number of $4$-form fields. Introducing many $4$-form fields, it predicts almost vanishing vacuum energies without assuming any parameter to be unnatural. The step-size being now large, the vacuum energy stays of order $1$ until the last step. Then, the large de Sitter temperature delays the common mechanism of inflation followed by reheating. After the last step, the standard Big-Bang cosmology can start. One other proposal, instead of introducing a large number of $4$-form fields, introduces one $4$-form field with large extra-dimensions (Refs.\cite{Bousso:2000xa} and \cite{Feng:2000if}).

\textbf{Question:} Can we consider the Weinberg window as an attractor ? \\
It is possible to think of $\Delta \Lambda < 10^{-120}$ as an attractor or at least as a stable configuration since the tunneling rate if the step-size is small enough becomes extremelly large and can even identically vanish for an anti-de-Sitter Universe, assuming some conditions (Ref.\cite{Coleman:1980aw} and Jakob Moritz section). But this is forgetting about the change in the inflaton potential and about the existence of phase transitions in the Early Universe\footnote{Assuming large membrane charges $q$, the large de Sitter temperature kicks all the fields out of their minimum. These are all contributions to the vacuum energy.} which will add new contributions to the already tuned vacuum energy. And there is no apparent reason why the window
\begin{equation}
V_{shift} < \Lambda < 10^{-120} + V_{shift}
\end{equation}
with $V_{shift}  = V(\phi_{last}) - V(0) - V_{SUSY} - V_{EW} - V_{QCD}$ would be an attractor.
