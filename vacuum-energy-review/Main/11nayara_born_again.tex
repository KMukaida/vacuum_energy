%!TEX root = ../VacuumEnergy.tex

\chapter{Bouncing cosmologies and vorticity in compact extra dimensions}
\label{cha:BornAgain}

\section{Motivation}

This section is mainly based on Ref.\,\cite{Graham:2017hfr} which introduces   a new class of non-singular bouncing cosmologies with a nonfactorizable metric.  Recently, there has been a renewed interest in  bouncing cosmologies  (see e.g. the reviews \cite{Battefeld:2014uga, Brandenberger:2016vhg, Cheung:2016wik}), which  propose  that the universe could have undergone a bounce connecting a contracting phase to the currently expanding one instead of  originated from  a primordial singularity. 

 
For the purpose of this series of lectures, we need a bounce  to inject energy  in a cold universe  in order to solve the empty universe problem in Abbott's model. During the crunching phase, the universe can get very hot, i.e. with  very high energy density, then one can consider the possibility of   \emph{reheating the universe with a bounce}. We list below some reasons why this framework can be  particularly interesting to face the cosmological constant (CC) problem. 
\begin{itemize}
\item Weinberg's no-go theorem: this approach   avoids for instance the  Weinberg's no-go theorem which says that a dynamical solution to the CC problem needs  simultaneously  to satisfy  $V \approx 0$ and $\partial V/\partial \phi \approx 0$.  For a generic potential   this is hard to  be accomplished without fine-tuning, but for an Abbott-like potential the condition  $\partial V/\partial \phi \approx 0$ is trivially satisfied thanks to the large density of local minima.
\item Phase transitions:    the usual issue regarding the CC adjustment at high temperatures  is evaded in this scenario. Here the tune of CC happens in an `empty universe' when the energy density  is comparable to the energy density today.  The universe is then reheated through a bounce and the CC will change. However, as during this period the universe is radiation dominated,  changes on the CC are not observable. Then after the universe cools down, the CC goes back to the vacuum value.
\end{itemize}

 A concrete   realization that makes use of an Abbott-like model to tune the  CC  and then reheats  the universe with a bounce is still to be implemented.   The discussion presented in this section represents  a step in this direction. 

\section{Bounce}

Let us first establish what kind of matter can produce a bounce using an example. Assuming a flat, homogeneous, and isotropic universe we can write the metric  as
\begin{equation}
ds^2 = g_{\mu \nu} dx^\mu dx^\nu = - dt^2 + a^2(t)(dx^2 + dy^2 + dz^2),
\end{equation}
where $a(t)$ is the scale factor. In order to get the Friedman equations, let us consider the non-zero components of the  Einstein's field equation:
\begin{eqnarray} \label{eq:Einstein}
R_{\mu \nu} -\frac{1}{2} \mathcal{R} g_{\mu \nu} = 8 \pi G T_{ \mu\nu}, \\
R_{00} = - 3\frac{\ddot a}{a}, \quad R_{ij} = \left[\frac{\ddot{a}}{a} + 2 \left(\frac{\dot{a}}{a}\right)^2\right]g_{ij},
\end{eqnarray}
with $\mathcal{R}$ being the Ricci scalar ($\mathcal{R}= R^\mu_{~\mu} = g^{\mu \nu} R_{\mu \nu}$).
By assuming a perfect fluid we can write the energy-momentum tensor as
\begin{equation} \label{eq:energy-momentum}
T^\mu_{~~\nu}= \textrm{diag}(-\rho, p,p,p),
\end{equation}
 where $\rho$ and $p$ are respectively  energy density and pressure as measure in the rest frame. Therefore,  the Friedman equations are written as
\begin{equation}
\left(\frac{\dot{a}}{a}\right)^2 = \frac{8 \pi G}{3} \rho, \qquad \left(\frac{\ddot{a}}{a}\right) = -\frac{4 \pi G}{3} (\rho + 3p) ~~\Rightarrow
\end{equation}
\begin{equation} \label{eq:Feqs}
\left(\frac{\ddot{a}}{a}\right)  - \left(\frac{\dot{a}}{a}\right)^2 = -\frac{12 \pi G}{3} (\rho + p).
\end{equation}
At the bounce, one has:  $\dot{a}=0$ and $\ddot{a}>0$ (see Fig.\,\ref{fig:scalefactor} for a sketch of the time evolution of the scale factor). Finally, from Eq.\,\ref{eq:Feqs} we conclude that the condition for a bounce is
\begin{equation} \label{eq:NECv}
\rho + p < 0,
\end{equation}
as $p =w \rho$, this means that $w <-1$. Eq.\,\ref{eq:NECv} simply tells us that a matter that can produce a bounce violates the null energy condition (NEC). Similarly, using Eqs.\,\ref{eq:Einstein}--\ref{eq:energy-momentum} and contracting  the energy-momentum  tensor with a null vector ($u_\mu u^\mu =0$) one can  check that at the bounce ($\dot{a}=0$ and $\ddot{a}>0$):  $T_{\mu \nu} u^\mu u^\nu <0$.
\begin{figure}
\begin{center}
\begin{tikzpicture}
    \begin{axis}[
        domain=-.1:1,
        xmin=-0.08, xmax=0.65,
        ymin=-.08, ymax=.35,
        x=10cm,
        y=7.5cm,
        samples=400,
        axis y line=middle,
        axis x line=middle,
        x label style={at={(axis cs:0.66,0)},anchor=west},
        y label style={at={(axis cs:0,.36)},anchor=south},
        xlabel={$t$},
        ylabel={$a(t)$},
       % \draw (3.3,-.1)--(3.3,.1);
%\node at (-1.5,-.5) {$ f', M_I$};
        ticks=none
    ]
        \addplot+[mark=none] {0.1+ 6*(x-.30)^(2)};
        \addplot[dashed] coordinates {(0.30,0.1)(0.30,0)};
         \node at (axis cs: 0.30, -0.055) {$t_{\textrm{bounce}}$};
                      \end{axis}
\end{tikzpicture}
\end{center}
\caption{Sketch of the scale factor $a(t)$ as a function of the cosmic time. \label{fig:scalefactor}  }
\end{figure}







\section{A class of solutions}

The space-time  is chosen to be $R^4 \times X$, where $X$ is  a compact manifold with dimensionality larger than three. In the following, we will choose the compact space to be a torus, namely $X= T^3$. The metric ansatz is given by \cite{Graham:2017hfr} 
\begin{equation} \label{eq:metric}
ds^2 = \underbrace{-dt^2 + a^2(t)(dx^2 + dy^2 + dz^2)}_{\textrm{FRW metric}} + \underbrace{l^2(d\theta^2 + d\phi_1^2 + d\phi_2^2)}_{\textrm{Compact extra-dimensions}} \underbrace{- 2 \epsilon l \,( \sin\theta \, dt \,d\phi_1 +  \cos\theta \, dt\, d\phi_2)}_{\textrm{Vorticity}},
\end{equation}
where $a(t)$ is the scale factor for the $R^4$ space and $l$ is the stabilized radius for $T^3$.  Note that this metric is non-singular for any value of $\epsilon$ (for convenience in \cite{Graham:2017hfr} they assume  $\epsilon \ll 1$). In addition, although this metric is isotropic and homogeneous in  $R^3$,  due to the vorticity term, (\ref{eq:metric}) is not homogeneous along $T^3$ and cannot be factorized.  The metric above represents a class of metrics  built to  satisfy:
\begin{enumerate}
\item NEC is violated but the violation is only along the compact space $X$ where it can be provided by stable sources as Casimir energy;
\item given  that the condition 1. is fulfilled, the remaining part of the stress-energy tensor  preserves NEC.
\end{enumerate}

Now we will use the Einstein's equations to get the stress tensor and check the conditions under which  $T^3$  violates NEC and if the NEC violation is restricted to the compact extra dimensions.  The 4D components of the stress tensor satisfying the Einstein's equations up to $\mathcal{O}(\epsilon^2)$ are given by
\begin{eqnarray} \label{eq:T00}
T_{00} &=& - M_7^5\left(\frac{3 \epsilon^2 \ddot{a}(t)}{a(t)}+\frac{3 \left(\epsilon ^2-1\right) \dot{a}^2(t)}{a^2(t)}-\frac{3 \epsilon ^2}{4 l^2} \right),\\  \label{eq:Tii}
 T_{ii}&=& - M_7^5\left[\frac{\epsilon ^2 a^2(t)}{4 l^2} -2 \left(\epsilon^2-1\right) a(t) \ddot{a}(t)-\left(\epsilon ^2-1\right) \dot{a}^2(t)\right],
\end{eqnarray}
where  $i = x,y,z$ and $M_7$ is the 7D Planck scale that is related to the 4D reduced Planck mass by $M_{\textrm{Pl}}^2 = M_*^{n+2} V_n$ where $V_n$ is the volume of the extra dimensional space, in our case $n=3$. Using the stress tensor above we can   study 4D  geodesics during a bounce.  To this end, let us consider a null vector $u_\mu$ in $R^4$ ($u_\mu u^\mu =0$) along the $x$ direction, $u^\mu= (1, u^x, 0,0,0,0,0)$, and check  if NEC is violated along this direction.
%\begin{equation} \label{eq:4d}
%T_{\mu\nu} u^{\mu}u^{\nu}  = M_7^5\left[\epsilon ^2 \left(-\frac{{\ddot{a}(t)}}{a(t)}-\frac{2 \dot{a}^2(t)}{a^2(t)}+\frac{1}{2 l^2}\right)-\frac{2 \left(a(t) \ddot{a}(t)-\dot{a}^2(t)\right)}{a^2(t)} \right].
%\end{equation}
Note that one can keep track of the vorticity through the terms proportional to $\epsilon^2$. Let us   examine what happens at the bounce, namely $\dot{a}=0$ and $\ddot{a}>0$,
\begin{equation} \label{eq:4dappr}
T_{\mu\nu} u^{\mu}u^{\nu}  \approx M_7^5\left[\frac{\epsilon^2}{2 l^2}-\frac{2 \ddot{a}}{a}\right],
\end{equation}
which for $\epsilon \neq 0$   can be arranged such that  $T_{\mu\nu} u^{\mu}u^{\nu} >0$. In Eq.\ref{eq:4dappr}, besides  $\epsilon\ll 1$, we considered the   limit 
\begin{equation} \label{eq:llimits}
 l^2\frac{\ddot{a}}{a} \ll 1
\end{equation}
that  represents the fact that the Hubble scale during the bounce is much smaller than the compactification scale, indicating that it should be possible   to describe the bounce in a 4D effective field theory. 


 After one has established that $T_{\mu\nu} u^{\mu}u^{\nu} >0$ is satisfied for the non-compact space, one should also check if  NEC is violated in the compact extra dimensions. To this end, let us consider the following null vector, $v^M = (1, 0,0,0,0,v^6,0)$. We can then compute the stress tensor along its direction,

\begin{equation} \label{eq:4dappr2}
T_{M N} v^{M}v^{N}  = M_7^5\left[\frac{\epsilon}{2 l^2}\sin\theta -\frac{3 \ddot{a}}{a}\right],
\end{equation}
which depends on the sign of $\sin \theta$ and can be arranged to be $T_{M N} u^{M}u^{N} < 0$. In order to obtain such   NEC,  one can  consider Casimir energy in the compact space.

Finally, it is convenient to split the stress-energy tensor into two parts
\begin{equation}
T = T_C + T_D, 
\end{equation}
where $T_D$ refers to the piece that preserves NEC and $T_C$ provides the stable source of NEC violation along the extra dimensions which is obtained by an appropriate choice of Casimir energy densities. Although Ref.\,\cite{Graham:2017hfr} does not discuss the microscopic physics from which $T_D$ can be obtained, using the freedom one has to construct the $T_C$ part and the fact  that the Hubble scale during the bounce is sufficient small, it is possible to show that $T_D$ always satisfy NEC. 

 The class of metrics in (\ref{eq:metric}) can evade the singularity theorems  avoiding the focusing of null congruences that are along the non-compact dimensions. The idea is that geodesics which are entirely in $R^4$ have vorticity  and then are forced to rotate into the compact extra dimensions. This avoids  the geodesics  to converge into a sigularity during a bounce even if there is no NEC violation in the non-compact dimensions.  


\section{Effective description}

As we have  anticipated in the previous section, it should be possible to describe the bounce evolution  in a 4D effective field theory since the Hubble scale at the bounce can be much smaller than the compactification  scale. 
  In the following,  after performing a dimensional reduction, we show that the 4D effective matter that generates the bounce  violates NEC. This is a consistency check   
 as there is no vorticity in four dimensions,  then the singularity theorems imply that the  matter which generates a bounce has to violate NEC.   A concrete description which specifies  matter and Lagrangian for such four dimensional effective  theory is still unknown.

Before we discuss the dimensional reduction  7D $\rightarrow$ 4D  of Ref.\,\cite{Graham:2017hfr}, let us first review the Kaluza-Klein (KK) theory. 
To this end, we will consider an extra dimensional theory with $4 +1$ dimensions within the extra  dimension   compactified on a circle of radius $R$   (see e.g. \cite{Csaki:2004ay, Ponton:2012bi} for reviews). At low energies (compared to the compactification scale $\sim 1/R$),  the relevant field configurations are the so called zero-modes, which have  constant profile along the extra dimension. A five dimensional metric can always be  parameterize as \cite{Appelquist:1983vs}:
\begin{eqnarray} \label{eq:metric5D}
\mathfrak{g}_{MN} = \phi^{-1/3} \left(\begin{array}{cc} g_{\mu\nu} + A_\mu A_\nu \phi & A_\mu \phi \\
 A_\mu \phi       & \phi  \end{array} \right),
\end{eqnarray}
where the Greek indices refer to  the non-compact  dimensions.  For smooth functions on a finite interval,  each component of $\mathfrak{g}_{AB}$ can  be decomposed in Fourier modes as
\begin{equation}
\mathfrak{g}_{MN}(x^\mu, y) = \sum_{n=-\infty}^{n=\infty} \mathfrak{g}^{(n)}_{MN}(x^\mu)\, f^{(n)}(y)
\end{equation} 
within $f^{(n)}(y)= e^{iny/R}$ (for a circle) and $y$ being the coordinate along the extra dimension, $0\leq y<2 \pi R$. In the low energy limit, it is convenient to parameterize the metric only as a function of the zero-modes ($n=0$).
%\begin{equation} \label{eq:metric5D}
%ds^2 = \mathfrak{g}_{MN} dx^M dx^N = \phi^{-1/3} g_{\mu \nu}dx^\mu dx^\nu + \phi^{2/3}(dy + A_\mu dx^\mu)^2
%\end{equation}
Therefore, using  the ansatz above (\ref{eq:metric5D}) we can write down the 5D Einstein-Hilbert action, 
\begin{equation}
S_5 \supset - \frac{1}{2}M_5^3 \int d^4x \,dy \sqrt{\mathfrak{g}}\,\mathcal{R}_5[\mathfrak{g}],
\end{equation}
which after the integration over the extra dimension gives 
\begin{equation} \label{eq:S4}
S_4 \supset - \frac{1}{2}M_{\textrm{Pl}}^2 \int d^4x \sqrt{g} \left( \mathcal{R}_4[g] +\frac{1}{4} \phi F_{\mu \nu}F^{\mu \nu} +\frac{1}{6} \frac{\partial_\mu\phi \partial^\mu \phi}{\phi^2}\right),
\end{equation}
where $M_5$ is the 5D Planck mass related to the 4D Planck scale by $M_{\textrm{Pl}}^2 = M_5^3 (2\pi R)$.  All the fields in (\ref{eq:S4}) are now  independent of $y$. One can canonically normalize the field $\phi$ by performing a field redefinition $\phi \rightarrow e^{-\sqrt{6}\phi/M_{\textrm{Pl}}}$ which makes evident that $\phi$ has couplings of a dilaton field. We can then conclude that, in the low energy limit, the KK theory describes: the 4D gravity,
a $U(1)$ gauge theory, and a real scalar field which couples like a dilaton. 

Now we are going to consider the case   $D>5$. We will describe the  metric (\ref{eq:metric}) using the following parametrization \cite{Appelquist:1983vs}:
\begin{eqnarray}
\mathfrak{g}_{AB} = (\textrm{det} \Phi)^{-1/5} 
\left(\begin{array}{cc}g_{\mu\nu} + B_\mu^a B_\nu^b \Phi_{ab} & B_\mu^c \Phi_{ca} \\B_\nu^c \Phi_{cb} & \Phi_{ab}\end{array}\right),
\end{eqnarray}
where as before Greek indices refer to the non-compact  dimensions and  lower-case latin indices count the  three extra compact dimensions. 
As we discussed before, the fields $g, B,$ and $\Phi$ can be expanded in KK modes in the extra dimensions.  The next step is to write down the Einstein tensor and then to use the  Einstein's equations to define the effective stress-energy tensor.  
The 4D components of the Einstein tensor are \cite{Graham:2017hfr}: 
\begin{eqnarray} \label{eq:G00}
G^{(7)}_{00} &=& G_{00} - \frac{1}{4}[(\partial_\theta B_0^{\phi_{1}})^2 + (\partial_\theta B_0^{\phi_{2}})^2] - (B_0^{\phi_{1}}\partial^2_\theta B_0^{\phi_{1}} + B_0^{\phi_{2}}\partial^2_\theta B_0^{\phi_{2}}) + \ldots \\ \label{eq:Gii}
G^{(7)}_{ii} &=& G_{ii} - \frac{a^2(t)}{4}[(\partial_\theta B_0^{\phi_{1}})^2 + (\partial_\theta B_0^{\phi_{2}})^2] + \ldots,
\end{eqnarray}
where the dots  refer to terms containing  $\Phi$ fields,  higher order terms in $B$, and terms with derivatives with respect to the extra-dimensional coordinates. The $G_{\mu \nu}$ terms above contain only the zero-mode part of $g_{\mu \nu}$, i.e. they do not depend on $\theta, \phi_1$ or $\phi_2$. Then we are going to define the  components of  the stress-energy tensor in the 4D effective theory as the remaining terms in the right hand side of Eqs.\,(\ref{eq:G00}) and (\ref{eq:Gii}).
Therefore, we are able to define the 4D components of the effective stress-energy tensor \cite{Graham:2017hfr} 
\begin{eqnarray} \label{eq:Tefftt}
M_7^5 T^{\textrm {eff}}_{00} &\equiv &  \frac{1}{4} [(\partial_\theta B_0^{\phi_{1}})^2 + (\partial_\theta B_0^{\phi_{2}})^2]+ (B_t^{\phi_{1}}\partial^2_\theta B_0^{\phi_{1}} + B_0^{\phi_{2}}\partial^2_\theta B_0^{\phi_{2}})
, \\ \label{eq:Teffii}
M_7^5 T^{\textrm {eff}}_{ii} &\equiv &   \frac{a^2(t)}{4 } [(\partial_\theta B_t^{\phi_{1}})^2 + (\partial_\theta B_t^{\phi_{2}})^2].
\end{eqnarray}
We  note  that the off-diagonal terms of  metric (\ref{eq:metric}) can be seen as a vacuum expectation value (vev) for the vectors $B_\mu^{\phi_1}, B_\nu^{\phi_2} $ in the direction of time, i.e. $\langle B_0^{\phi_1}\rangle= (\epsilon/L) \sin\theta$ and $\langle B_0^{\phi_2}\rangle= (\epsilon/L) \cos \theta$. Taking this into account,  one can replace the vevs into Eqs.\,\ref{eq:Tefftt} and  \ref{eq:Teffii}, which gives
\begin{eqnarray}
M_7^5 T_{00}^{\textrm {eff}} &=&  - \frac{3\epsilon^2}{4 L^2},\\ 
M_7^5 T_{ii}^{\textrm {eff}} &= &   a^2(t) \frac{\epsilon^2}{4L^2}.
\end{eqnarray}
Finally, it is possible to  check that  a null vector $n^\mu$ pointing in any direction in the 4D space leads to $n^{\mu}n^{\nu}T_{\mu \nu}^{\textrm{eff}} = -\epsilon^2/(2 L^2) <0$. Then,  as we  expected, the effective matter that produces a bounce violates  NEC. 

\section{Conclusion}

In this section we  introduce a nonfactorizable class of metrics that contains vorticity in the compact extra dimensions.  This  allows for a non-singular bouncing cosmology without the requirement  of violation of  NEC along the non-compact space. 
Although NEC can be violated in such class of solutions, the violation is restricted to the compact space where it can be generated through known stable sources as Casimir energy densities. Additionally, since the Hubble scale at the bounce can be parametrically smaller than the compactification  scale, it should be possible to describe the bounce evolution  in a 4D effective field theory.

  Finally, we should stress that the idea discussed here may be applied to the empty universe problem in Abbott's model  since a bounce can be used to reheat the universe after the CC has been adjusted to the tuned value.


