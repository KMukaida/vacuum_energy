%!TEX root = ../VacuumEnergy.tex

\chapter{Eternal inflation, the multiverse and the measure problem}
\label{cha:multiverse}

This section studies eternal inflation, its multiverse implications and the measure problem. The topic is very broad so here we only provide a summary of some of the main ideas. We illustrate some of them with explicit calculations (showing e.g. that different measures lead to different results) but sometimes just comment the key points to understand an idea/problem. 

\section{Eternal inflation \& the multiverse }
 
 There is a lot of literature on eternal inflation,   \cite{Guth} being a nice one where many of the following results are included. 
We start by recalling that  for $\Lambda>0$ the Universe   has de Sitter   (dS) geometry, so for a metric ansatz
\begin{equation}
ds^2=-dt^2+a^2(t)d\vec{x}^2
\end{equation}
Einstein's equation is $H^2\equiv (\dot{a}/a)^2=\Lambda/3$, and so $a(t)=e^{Ht}$. This tells that the volume of a region grows indefinitely with   $ e^{3Ht}$. 


In order to describe inflation, where the Universe expands very quickly for a finite amount of time, the proposal is to consider a scalar field (the inflaton) with a   positive    potential   which  behaves approximately as a positive cosmological constant \ $\Lambda \rightarrow  V(\phi)/M_P^{2}>0$   for some values of the scalar field $\phi$. Then Einstein's equations and the scalar field equations read\begin{equation}
3M_P^2H^2=\dfrac{1}{2}\dot{\phi}^2+V\quad\quad , \quad\quad \ddot{\phi}+3H\dot{\phi}=-V' \ . 
\end{equation} 
In the first proposals  of this mechanism, the so-called old inflation scenario, the Universe inflates while being \textbf{ in} a false vacuum state $\phi_i$, then tunnels to a lower vacuum state $\phi_j$ 
with a rate given by the Coleman-de Luccia \cite{Coleman:1980aw} formula for a bubble nucleation process  
\begin{equation}
\Gamma_{ i  j }\sim P(i \rightarrow j ) \sim e^{ S_i - S_j } = \exp \left( - \dfrac{ 24\pi^2 M_P^4 }{ V ( \phi_i )} + \dfrac{ 24 \pi^2 M_P^4}{ V ( \phi_j )} \right)%= \exp \left( \dfrac{ 24\pi^2 M_P^4 }{ V ( \phi_i )}  \dfrac{\Delta V }{V ( \phi_j )} \right) \ .
\end{equation}
This expression happens to also describe  the   Hawking-Moss instanton \cite{Hawking:1981fz} rate if \ $\phi_j=\phi_{top}$ corresponds to a top of the scalar potential. This fact will be explained shortly.

Let us now study what happens to a region with initial volume $\mathcal{V}_0 $ where the scalar field is   in a configuration $\phi_i$.   During the time interval $H_i^{-1}$ this region will 
grow by a factor   $e^3\sim 20$. We know that part of the region, whose volume now is $\sim 20 \mathcal{V}_0 $,  will tunnel to the true vacuum, and part of it will remain   in the false vacuum.
The corresponding fractions are given by the tunneling probability, and it is easy to see that  if $P(i\rightarrow j)< (1-e^{-3}) $, the volume of the region now in the false vacuum will be $\mathcal{V}'> \mathcal{V}_0$.
 So, the volume of the region with  $\phi_i$ will become larger with time. Therefore, there will always be regions  in the false vacuum, or more generally,   in the \textit{highland region} of the scalar potential, 
eternally inflating.  Note that this also implies the existence of a multiverse   in the false vacuum with many \textit{pocket universes} (bubbles) of true vacuum, or more generally other lower energy vacua. 
 This idea is schematically described in Fig. \ref{fig:old-eternal}.
 
%%%%%%%%%%%
\begin{figure}[htb]
\begin{center}
\includegraphics[scale=.5]{Figures/old-eternal-inflation}
\label{fig:old-eternal}
\caption{Schematic picture of old eternal infation, where more and more pocket universes are created as time passes, but there are always regions in the false vacuum, eternally infalting .}
\end{center}
\end{figure}
%%%%%%%%%%%

For later use, it is convenient to recall that AdS bubbles collapse \cite{Coleman:1980aw}, so we will   call  vacua with \ $V<0$ \
terminal vacua or \textit{sinks} of the multiverse. This name has to do with the decrease of comoving volume (without taking Hubble expansion into account) they produce in dS with time: it all ends in sinks! 

 In the new inflation scenario (see e.g. \cite{Linde-old}), instead,  there is no tunneling but  a slowly rolling scalar field  producing a  $\Lambda_{eff}$ for a period of time. Slow roll sets some requisites the scalar potential must fulfil (at least in a region of the potential):  \begin{equation}
 \varepsilon = -\dfrac{\dot{H}}{H^2}\ll 1 \ \text{ i.e. slow-roll:   } \  \frac{1}{2}\dot{\phi}^2\ll V(\phi )= 3M_P^2H^2  \ \text{  or   } \   \varepsilon =\dfrac{ M_P^2}{2} \left( \dfrac{V'}{V}\right)^2 \ll 1   
\end{equation}  
 Of course, if one wants inflation to last the slowly-rolling field cannot accellerate much, i.e.   \begin{equation}
 \ddot{\phi}\ll H\dot{\phi} \ \ \text{, \ \  \ which can be translated to   \  } \  \ \eta =M_P^2  \dfrac{|V''|}{V} \ll 1 \ .
\end{equation}  

The typical inflationary potential is shown in Fig. \ref{fig:inflation-typical}.
%%%%%%%%%%%
\begin{figure}[htb]
\begin{center}
\includegraphics[scale=.4]{Figures/new-eternal-inflation}
\caption{\small Schematic picture of a potential leading to  slow-roll inflation. The field first slowly rolls down the most flat region and eventually falls on the minimum, giving rise to the end of inflation.}\label{fig:inflation-typical}
\end{center}
\end{figure}
%%%%%%%%%%%


During inflation, the inflaton suffers quantum fluctuations that are space and time depedent   $\delta\phi (t,\vec{x})$. These fluctuations fulfil the properties \begin{equation}
\langle\delta\phi\rangle =0\quad\quad , \quad \quad   \langle\delta\phi^2\rangle =\dfrac{H^2}{4\pi^2} \ .  
\end{equation}
Let us now study some  of the effects of these quantum fluctuations. Consider  an inflaton   initially at $\phi_0$.  After a time interval $H^{-1}$  it will move due to its classical motion $\Delta \phi_{cl}$ and due to quantum fluctuations
\begin{equation}
\phi_0 \rightarrow \phi_0 + \Delta \phi_{cl}+ \delta \phi \quad \quad \text{ with } \quad \quad \Delta \phi_{cl}= \dfrac{3}{2}M_p\sqrt{\varepsilon }
\end{equation}
It can be that $|\delta\phi|\geq |\Delta\phi_{cl} | $. The condition for this to happen can be seen to be \begin{equation}
V=3M_P^2 H^2 \geq 27\pi^2 M_p^4 \varepsilon   \ . \label{eq:condition}
\end{equation}
Then, for a Gaussian distribution of $\delta\phi$ and saturation of the above inequality \begin{equation}
% P(|\delta\phi|>|\Delta\phi | ) = 16 \% \ .
P(|\delta\phi|>|\Delta\phi_{cl} | ) = 16 \% \ .
\end{equation}
So, for a region with initial volume $\mathcal{V}_0$ at $\phi_0$, the volume after $\Delta t=H^{-1}$ grows by $e^3\sim 20$ and  at least (?) $16 \%$  of it ($\sim 3\mathcal{V}_0$) will be in a field configuration $\phi \geq \phi_0$. As  in the old inflationary scenario, the volume   where $V(\phi) \geq V(\phi_0 )$ grows. 
If the initial state is on the highland of the scalar potential, this once again corresponds to a eternally inflating scenario.


The condition (\ref{eq:condition}) seems to be quite generic, but it may also be   troublesome. Let us comment what happens for a few choices of $V(\phi )$:\begin{itemize}
\item If $V=\frac{1}{2}m^2\phi^2$ ,  (\ref{eq:condition})  implies $\phi > M_P$ \ (but $V<M_P^4$ )
\item If $V=\lambda(\phi^2-v^2)^2$, some region of space will always be at the hill-top.
\item If $V=\lambda \tanh^2 (\phi / v )$, some region of space will always be at the plateau. 
\end{itemize}


From here we find that if certain conditions are met quantum fluctuations give rise to a random walk on the scalar potential for each spacetime region.  There are always inflating regions and also non-inflating ones. We have a  multiverse with many \textit{universes}: regions of space where $\phi$ looks homogeneous within the horizon $H^{-1}(t,\vec{x}) $.


In this stochastic approach   to  inflation the inflaton does a Brownian motion in field space \cite{Linde-old}. The probability to find a region 
 where the scalar field takes the specific value $\phi$  is given by a diffusion equation, that has a stationary solution \linebreak 
$P \sim \exp (-S(\phi ))$. To 'normalize' it one   has to match it with the Hartle-Hawking \cite{Hartle:1983ai} wave-function describing the ground state of the Universe. Then, if there is a de Sitter vacuum  $V_{dS}$   on  the scalar potential,
the probability distribution to find a point   in  the configuration $\phi$ is
\begin{equation}
P(\phi ) \sim   \exp \left( - \dfrac{ 24\pi^2 M_P^4}{ V_{dS}} + \dfrac{ 24 \pi^2 M_P^4}{ V ( \phi )} \right)
\end{equation}

Two remarks about this expression:
\begin{itemize}
\item   It  explains the probability of Hawking-Moss solution for $V(\phi )=V_{top}$ . 
\item This probability   is  defined as a fraction of \textit{comoving} volume.  
\end{itemize}
 
 



\section{The measure problem}

The following analysis has been extracted mainly from  \cite{Linde-new,Linde-old}

In an infinite multiverse, everything that can happen will happen an infinite amount of times. This leads to trouble when trying to compute probabilities. In fact, if we want to find a probability we need to compare quantities, but we always find expressions of the type
\begin{equation}
\dfrac{P_a}{P_b}=\dfrac{\infty}{\infty}
\end{equation}

This is of course problematic. In fact, the result is regularization (foliation) dependent. One could try to compare at 'equal time', but no global time coordinate exists. The problem is that, as we will now see,  different approaches lead to different results.
 This is the measure problem, that has been an open issue for $\sim 30$ years! 
 
 In order to understand the problem better, we  now compute some probabilities   in different approaches and see how the results differ from one another. To keep the analysis as simple as possible,  in what follows we will only consider old inflation type scenarios.

\subsection{Comoving probability measure}

In this section the probability  $P$ is defined as a  fraction of comoving volume, ignoring expansion of the Universe. We will illustrate the problem using the following rather simple potential

%%%%%%%%%%%
\begin{figure}[htb]
\begin{center}
\includegraphics[scale=.3]{Figures/potential-sink}
\label{fig:hwd6d8}
%\caption{\small Brane creation effect showing that in the presence of a type IIA mass parameter $p$, an NS5-brane can exist only with $p$ D6-branes ending on it.}
\end{center}
\end{figure}
%%%%%%%%%%%

The probability $P_i$ of being in a vacuum $i$ changes with time due to the different tunneling processes as described by the vacuum dynamics equations\begin{eqnarray}
\dot{P}_1=-J_{1s}-J_{12}+J_{21} \\ 
\dot{P}_2=-J_{2s}-J_{21}+J_{12}
\end{eqnarray}
where $J_{ij}=P_i \Gamma_{ij}$ are the probability currents. 

Let us also define,  for anthropic purposes, the  probability of being born in the vacuum dS$_i$, which  is proportional to production rate of dS$_i$ (but unlike in the previous case doesn't take into account the decay rate from the corresponding vacuum): \begin{equation}
\dot{Q}_i=\sum_j J_{ij } \ .
\end{equation}

One can solve the above equations using $\mathcal{P}_i\equiv \int_0^{\infty }P_i dt$ :\begin{eqnarray}
P_1(\infty ) - P_1(0)=-\mathcal{P}_1 ( \Gamma_{1s}+\Gamma_{12})+\mathcal{P}_2\Gamma_{21} \\ 
P_2(\infty ) - P_2(0)=-\mathcal{P}_2 ( \Gamma_{2s}+\Gamma_{21})+\mathcal{P}_1\Gamma_{12} \\ 
Q_1(\infty ) =\mathcal{P}_2\Gamma_{21} \\ 
Q_2(\infty ) =\mathcal{P}_1\Gamma_{12} 
\end{eqnarray}
and calculate e.g. the relative probability of being born at different dS vacua.  First, because of   the presence of  the sink, 
$P_1(\infty )=0$.   Second,  we neglect the tunneling rate between dS$_2$ and the sink. We can now compute, for example, the relative probability of being born in different vacua. Consider first the case 
  when  the whole Universe starts at dS$_2$ ($P_1(0)=0$ , $P_2(0)=1$).   Here one finds:  \begin{equation}
q_{21}=\dfrac{Q_2(\infty )}{Q_1(\infty )}= \dfrac{\mathcal{P}_1\Gamma_{12} }{\mathcal{P}_2\Gamma_{21}}= \dfrac{ \Gamma_{12} }{ \Gamma_{21}}\dfrac{\Gamma_{21}}{\Gamma_{1s}+\Gamma_{12}}= \dfrac{\Gamma_{12}}{\Gamma_{1s}+\Gamma_{12}}\ . 
\end{equation}
Instead, if the whole Universe starts at dS$_1$, an analogous calculation leads to
\begin{equation}
% q_{21}=... =  \dfrac{\Gamma_{12}}{\Gamma_{1s}+\Gamma_{12}} \left(1-\dfrac{1}{Q_1} \right) \ .
q_{21} =  \dfrac{\Gamma_{12}}{\Gamma_{1s}+\Gamma_{12}} \left(1-\dfrac{1}{Q_1} \right) \ .
\end{equation}
 So we see that the calculation based on the comoving probability measure highy depends on the initial conditions. 
% So we see that using the comoving probability measure, the result depends on initial conditions.

\subsection{Pseudo comoving volume-weighted measure}

The next step is to  take into account the Hubble expansion of the Universe, since the previous approach misses that observers are always being created due to inflation being eternal. We can start adjusting our time parameter to the local Hubble expansion  $H^{-1}$ as a time unit (i.e. we are measuring time in units proportional to the log of the distance between galaxies).

This time the vacuum dynamics equations are slightly modified\begin{eqnarray}
\dot{P}_1=-J_{1s}-J_{12}+J_{21}+3P_1 \\ 
\dot{P}_2=-J_{2s}-J_{21}+J_{12}+3P_2 \ .
\end{eqnarray}
Once again, taking $\Gamma_{2s}=0$ for simplicity, one can easily observe that  $p_{21}=P_2/P_1$ approaches a stationary regime such that \begin{equation}
P_1=\dfrac{P_2}{p_{21}}=\tilde{P}_1e^{3t}\exp \left(-\dfrac{\Gamma_{1s}}{1+p_{21}} t \right)	 
\end{equation}
where
\begin{equation}
p_{21}=
\begin{cases}
\Gamma_{12}/\Gamma_{21} \quad \text{ if } \quad \Gamma_{1s}/\Gamma_{12}\ll 1 \\
\Gamma_{1s}/\Gamma_{21} \quad \text{ if } \quad \Gamma_{1s}/\Gamma_{12}\gg 1 
\end{cases}
\end{equation}
At this point we can again compute the relative probability of being born in different vacua, as we did with the previous measure: \begin{equation}
q_{21}=\dfrac{Q_2}{Q_1}=\dfrac{\dot{Q}_2}{\dot{Q}_1} = \dfrac{J_{12}}{J_{21}}=\dfrac{P_1\Gamma_{12}}{P_2\Gamma_{21}}=\dfrac{\Gamma_{12}}{p_{21}\Gamma_{21}} \  .
\end{equation}
Unlike in the previous case, this expression indicates that in this measure the relative probability is independent  of initial conditions. The reason is of course that we took into account the Hubble expansion. Let us also remark that the outcome depends on the different tunneling rates, so one needs quite a lot of information of the potential to be able to compute this result.

\subsection{Standard volume-weighted measure}

The last measure we will consider is probably the most natural one. This time, we will consider the expansion of the Universe, but we will use more   standard time units (e.g. the Planck time $t_P$), which is more natural than the one in the previous case.


This time the vacuum dynamics equations are\begin{eqnarray}
\dot{P}_1=-J_{1s}-J_{12}+J_{21}+3H_1P_1 \\ 
\dot{P}_2=-J_{2s}-J_{21}+J_{12}+3H_2P_2 \ ,
\end{eqnarray}
whose solution is \begin{equation}
P_1=\dfrac{P_2}{p_{21}}=\tilde{P}_1\exp \left(\dfrac{3H_1+3p_{21}H_2-\Gamma_{1s}-p_{21}\Gamma_{2s}}{1+p_{21}} t\right) \ ,
\end{equation}
with  \ $p_{21}=\dfrac{3(H_2-H_1)}{\Gamma_{21}}\gg 1$ . 

Typically $H_i,\Delta H\gg \Gamma_{ij} $ , so we find that in this case   
\begin{equation}
P_1=\tilde{P}_1e^{3H_2 t}  \ .
\end{equation}
 The interpretation of this result is that most of the Universe grows quickly in dS$_2$ and then decays to dS$_1$ . 


To compare with the previous measures let us again compute \begin{equation}
q_{21}=\dfrac{\dot{Q}_2}{\dot{Q}_1}= \dfrac{P_1\Gamma_{12}}{P_2\Gamma_{21}}=\dfrac{\Gamma_{12}}{3(H_2-H_2)}\ll 1 \ ,
\end{equation}
which as expected is independent of the initial conditions due to Hubble growth, and moreover, requires way less information about the scalar potential as compared to the previous case (e.g. it is   independent of sinks due to $H_i,\Delta H\gg \Gamma_{ij} $). 

The above results show that different measures lead to different results. The last measure we considered seems to be the most natural one, but it is not better motivated than the others and is not without problems. For example, as explained in \cite{Linde-new}, this measure happens not to be very helpful for solving the cosmological constant problem. To explain this, let us first note that the most (and almost only) relevant scale when considering this measure is the energy density scale 
 of the highland of the potential, which is responsible for most of the growth of space on the multiverse. The possibility to obtain a de Sitter geometry with a concrete  value of the cosmological constant is then given by the tunneling rate from the highland to a particular vacuum with the corresponding value of vacuum energy. Unless there exists some mechanism enhancing such a tunneling rate, there is a flat probability distribution to tunnel to all vacua out of the highlands.
  It's been observed  (see e.g. \cite{SchwartzPerlov:2006hi}) that this flat distribution may be rather likely,   especially  in potentials with very large amount of vacua,
such as the string theory landscape.

To give an end to this section, let us just recall that   the measure problem is still an open issue in physics.


\section{Other problems}

The measure problem is only one of the many 'paradoxes/questions/problems' that arise when considering the eternally inflating multiverse. We now briefly go through some other of these problems that  are rather different to the one above. We would like to point out that in some cases  these paradoxes arise when trying to argue that we are typical in the multiverse, which may not be true.  We don't intend to go on this debate in these notes, so in what follows we just describe some of the most popular problems related to eternal inflation.

\subsection{Youngness paradox}

For more details, see e.g. \cite{Guth}.   The youngness paradox starts with the observation that due to exponential growth of the false vacuum, the pocket-universe production rate is\begin{equation}
\Gamma \mathcal{V}(t)= ( \Gamma \mathcal{V}_0) e^{3Ht} \ .
\end{equation}
So it grows exponentially with time. Therefore, most of the pocket universes are born at late times, they are very young! Our \textit{old} pocket universe seems to be old, and thus there are many more young pocket universes than universes as old as ours, so we would be living in an extremely  unlikely pocket universe! As mentioned above, this is not necessarily problematic, but just indicates we are not typical in the multiverse.




\subsection{Boltzmann brains \cite{Kleban,Linde-new} }

Since de Sitter is a thermal system,   it is possible to create galaxies, planets, observers... (not necessarily inteligent ones like us, it would be enough to have some sort of conscious being known as Boltzmann Brains or BB) from a thermal fluctuation, rather than going through the whole process of   inflation and subsequent evolution of the Universe to the way it is at the moment. In other words, if a vacuum is long-enough lived, spontaneous structure formation will occur.

It is possible to compute the BB creation rate based on the arguments related to the entropy of the Universe. It turns out that the 
 BB creation rate happens to be larger than the nucleation rate of inflationary universes. Again, it seems    that BB-s may be more typical than us. Nonetheless, this result is different when considering the measures above. Here we will not study each case, since the goal is just to explain what the problem is about. The interested reader can check \cite{Linde-new} and references thereof.


\section{A proposed solution: horizon complementarity}

A proposal to solve many of the above problems is based on the so-called horizon complementarity idea, that generalizes   black hole (BH) complementarity (see e.g. \cite{Bousso,Nomura}). 

The idea behind  BH complementarity is that one cannot patch together different pictures in the presence of a horizon separating both patches. In order to understand it better, let us consider two observers  $A$ and $B$ that are out of a BH, such that $A$  falls in the  BH with e.g. \textit{books}, while $B$ stays out. Once $A$ crosses the horizon, from $A$'s perspective the books just crossed the horizon, and nothing else happens. From $B$'s perspective, instead, the books crossed the horizon and eventually will become Hawking radiation (for this discussion we  leave aside possible 
information loss in this process). Both observers see different things. Who is right? According to BH complementarity both are. The problem is that we cannot patch both pictures together due to the horizon.


Extending this idea to cosmology is not complicated. One could simply state that   it makes no sense to try to define things out of our causal patch. This may provide a solution to many of the previous ambiguities. In fact, it seems to be a reasonable explanation for several paradoxes and problems, but for the time being this is nothing else but a proposal.


\section{Outlook}

In this chapter we  addressed some of the consequences of eternal inflation and the possibility of the existance of a multiverse, so this chapter is more expeculative than many of the previous ones. We started studying how eternal inflation gives rise to the multiverse both in the old and new inflation scenarios and afterwards provided a superficial description of some of the problems arising from the existence of the multiverse. The first of these problems is the measure problem, which in a few words is the trouble one finds to define probability distributions in an infinite Universe. There are multiple porposals to regularize the infinities one finds when trying to compute relative probabilities in the multiverse, each of them suffering from some limitations. We also addressed more briefly the issue of why our pocket unirse is `so old' if most of the pocket universes are created at very late times, and why our universe went through the process of inflation and subsequent structure formation instead of being the product of a thermal fluctuation, which seems to be more likely. These two paradoxes, known as the youngness and the Boltzmann brain paradox, are the result of trying to argue that our pocket universe is a typical one in the multiverse, which might not be the case. We finally mentioned a proposal to solve these problems, which extends the black hole complementarity idea to cosmology: according to horizon complementarity, it makes no sense to try to argue about the physics out of our causal patch. 


% \bibliography{biblio}
% \bibliographystyle{JHEP}


% \end{document}

% %%%%%%%%%%%
% \begin{figure}[htb]
% \begin{center}
% \includegraphics[scale=1]{figure.pdf}
% \caption{\small Possible configuration of D6-branes with a mirror dual description in terms of magnetized D7-branes. Mirror symmetry is equivalent to three T-dualities all performed along
% the vertical directions of the three tori.}\label{fig:1}
% \end{center}
% \end{figure}
% %%%%%%%%%%%
 



% \begin{thebibliography}{99}


   
% \end{thebibliography}      



% \subsection{Template for figure}

