\BOOKMARK [0][-]{toc.0}{Contents}{}% 1
\BOOKMARK [0][-]{chapter.1}{Cosmological relaxation \340 la Abbott and Brown-Teitelboim}{}% 2
\BOOKMARK [1][-]{section.1.1}{The idea: a stepwise adjustment of the Cosmological Constant}{chapter.1}% 3
\BOOKMARK [1][-]{section.1.2}{The Abbott mechanism}{chapter.1}% 4
\BOOKMARK [2][-]{subsection.1.2.1}{Framework}{section.1.2}% 5
\BOOKMARK [2][-]{subsection.1.2.2}{Condition on the step-size}{section.1.2}% 6
\BOOKMARK [2][-]{subsection.1.2.3}{Dynamic: The descent of the potential}{section.1.2}% 7
\BOOKMARK [1][-]{section.1.3}{The Brown and Teitelboim mechanism}{chapter.1}% 8
\BOOKMARK [2][-]{subsection.1.3.1}{The Schwinger process in \(1+1\)-D}{section.1.3}% 9
\BOOKMARK [2][-]{subsection.1.3.2}{From the \(1+1\)-D Schwinger process to the BT mechanism}{section.1.3}% 10
\BOOKMARK [2][-]{subsection.1.3.3}{Ability to solve the CC problem}{section.1.3}% 11
\BOOKMARK [1][-]{section.1.4}{Bousso-Polchinski approach}{chapter.1}% 12
\BOOKMARK [2][-]{subsection.1.4.1}{Multiple 4-form fields}{section.1.4}% 13
\BOOKMARK [2][-]{subsection.1.4.2}{Step-Size Problem treatment}{section.1.4}% 14
\BOOKMARK [2][-]{subsection.1.4.3}{Membrane nucleations naturally lead to a multiverse}{section.1.4}% 15
\BOOKMARK [2][-]{subsection.1.4.4}{Empty Universe Problem treatment}{section.1.4}% 16
\BOOKMARK [1][-]{section.1.5}{Conclusion}{chapter.1}% 17
\BOOKMARK [0][-]{chapter.2}{Relaxation of Cosmological Constant via Null Energy Condition Violation}{}% 18
\BOOKMARK [1][-]{section.2.1}{Motivation}{chapter.2}% 19
\BOOKMARK [1][-]{section.2.2}{Preliminaries}{chapter.2}% 20
\BOOKMARK [2][-]{subsection.2.2.1}{Ghost condensation}{section.2.2}% 21
\BOOKMARK [2][-]{subsection.2.2.2}{Horndeski's theory}{section.2.2}% 22
\BOOKMARK [1][-]{section.2.3}{Relaxing the CC}{chapter.2}% 23
\BOOKMARK [1][-]{section.2.4}{Relaxation}{chapter.2}% 24
\BOOKMARK [2][-]{subsection.2.4.1}{Phase transition}{section.2.4}% 25
\BOOKMARK [2][-]{subsection.2.4.2}{NEC violation}{section.2.4}% 26
\BOOKMARK [1][-]{section.2.5}{Conclusion}{chapter.2}% 27
\BOOKMARK [0][-]{chapter.3}{Bouncing cosmologies and vorticity in compact extra dimensions}{}% 28
\BOOKMARK [1][-]{section.3.1}{Motivation}{chapter.3}% 29
\BOOKMARK [1][-]{section.3.2}{Bounce}{chapter.3}% 30
\BOOKMARK [1][-]{section.3.3}{A class of solutions}{chapter.3}% 31
\BOOKMARK [1][-]{section.3.4}{Effective description}{chapter.3}% 32
\BOOKMARK [1][-]{section.3.5}{Conclusion}{chapter.3}% 33
\BOOKMARK [0][-]{chapter*.13}{Bibliography}{}% 34
