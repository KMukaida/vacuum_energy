\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Cosmological relaxation \`a la Abbott and Brown-Teitelboim}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}The idea: a stepwise adjustment of the Cosmological Constant}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}The Abbott mechanism}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Framework}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Condition on the step-size}{3}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Dynamic: The descent of the potential}{4}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}The Brown and Teitelboim mechanism}{5}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}The Schwinger process in (1+1)-D}{5}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}From the (1+1)-D Schwinger process to the BT mechanism}{7}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Ability to solve the CC problem}{8}{subsection.1.3.3}
\contentsline {section}{\numberline {1.4}Bousso-Polchinski approach}{9}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Multiple 4-form fields}{9}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Step-Size Problem treatment}{10}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Membrane nucleations naturally lead to a multiverse}{13}{subsection.1.4.3}
\contentsline {subsection}{\numberline {1.4.4}Empty Universe Problem treatment}{14}{subsection.1.4.4}
\contentsline {section}{\numberline {1.5}Conclusion}{16}{section.1.5}
\contentsline {chapter}{\numberline {2}Relaxation of Cosmological Constant via Null Energy Condition Violation}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Motivation}{17}{section.2.1}
\contentsline {section}{\numberline {2.2}Preliminaries}{17}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Ghost condensation}{17}{subsection.2.2.1}
\contentsline {paragraph}{\nonumberline \bm {$P(X)$} theory.}{17}{section*.2}
\contentsline {paragraph}{\nonumberline Ghost condensation.}{19}{section*.3}
\contentsline {paragraph}{\nonumberline Ghost inflation.}{21}{section*.4}
\contentsline {subsection}{\numberline {2.2.2}Horndeski's theory}{23}{subsection.2.2.2}
\contentsline {paragraph}{\nonumberline Horndeski's theory.}{23}{section*.5}
\contentsline {paragraph}{\nonumberline Fluctuations.}{24}{section*.6}
\contentsline {section}{\numberline {2.3}Relaxing the CC}{25}{section.2.3}
\contentsline {section}{\numberline {2.4}Relaxation}{25}{section.2.4}
\contentsline {paragraph}{\nonumberline Ordinary scalar field.}{26}{section*.7}
\contentsline {paragraph}{\nonumberline Ghost condensation with shift symmetry breaking.}{29}{section*.8}
\contentsline {subsection}{\numberline {2.4.1}Phase transition}{32}{subsection.2.4.1}
\contentsline {paragraph}{\nonumberline Case 1.}{33}{section*.9}
\contentsline {paragraph}{\nonumberline Case 2.}{33}{section*.10}
\contentsline {subsection}{\numberline {2.4.2}NEC violation}{34}{subsection.2.4.2}
\contentsline {paragraph}{\nonumberline Slow NEC violation by ghost condensation.}{35}{section*.11}
\contentsline {paragraph}{\nonumberline Fast NEC violation by Hordeski's theory.}{40}{section*.12}
\contentsline {section}{\numberline {2.5}Conclusion}{42}{section.2.5}
\contentsline {chapter}{\numberline {3}Bouncing cosmologies and vorticity in compact extra dimensions}{45}{chapter.3}
\contentsline {section}{\numberline {3.1}Motivation}{45}{section.3.1}
\contentsline {section}{\numberline {3.2}Bounce}{46}{section.3.2}
\contentsline {section}{\numberline {3.3}A class of solutions}{47}{section.3.3}
\contentsline {section}{\numberline {3.4}Effective description}{49}{section.3.4}
\contentsline {section}{\numberline {3.5}Conclusion}{51}{section.3.5}
\contentsline {chapter}{\nonumberline Bibliography}{53}{chapter*.13}
